package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.appengine.repackaged.com.google.gson.Gson;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.googlecode.objectify.cmd.Saver;
import com.smartbeat.vigisense.server.model.MioFuseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * WARNING: This generated code is intended as a sample or starting point for using a
 * Google Cloud Endpoints RESTful API with an Objectify entity. It provides no data access
 * restrictions and no data validation.
 * <p>
 * DO NOT deploy this code unchanged as part of a real application to real users.
 */
@Api(
        name = "mioFuseActivityApi",
        version = "v1",
        resource = "mioFuseActivity",
        namespace = @ApiNamespace(
                ownerDomain = "model.server.vigisense.smartbeat.com",
                ownerName = "model.server.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class MioFuseActivityEndpoint {

    private static final Logger logger = Logger.getLogger(MioFuseActivityEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(MioFuseActivity.class);
    }

    /**
     * Returns the {@link MioFuseActivity} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code MioFuseActivity} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "mioFuseActivity/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public MioFuseActivity get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting MioFuseActivity with ID: " + _id);
        MioFuseActivity mioFuseActivity = ofy().load().type(MioFuseActivity.class).id(_id).now();
        if (mioFuseActivity == null) {
            throw new NotFoundException("Could not find MioFuseActivity with ID: " + _id);
        }
        return mioFuseActivity;
    }

    /**
     * Inserts a new {@code MioFuseActivity}.
     */
    @ApiMethod(
            name = "insert",
            path = "mioFuseActivity",
            httpMethod = ApiMethod.HttpMethod.POST)
    public MioFuseActivity insert(MioFuseActivity mioFuseActivity) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that mioFuseActivity._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(mioFuseActivity).now();
        logger.info("Created MioFuseActivity with ID: " + mioFuseActivity.get_id());

        return ofy().load().entity(mioFuseActivity).now();
    }

    /**
     * Updates an existing {@code MioFuseActivity}.
     *
     * @param _id             the ID of the entity to be updated
     * @param mioFuseActivity the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseActivity}
     */
    @ApiMethod(
            name = "update",
            path = "mioFuseActivity/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public MioFuseActivity update(@Named("_id") Long _id, MioFuseActivity mioFuseActivity) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(mioFuseActivity).now();
        logger.info("Updated MioFuseActivity: " + mioFuseActivity);
        return ofy().load().entity(mioFuseActivity).now();
    }

    /**
     * Deletes the specified {@code MioFuseActivity}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseActivity}
     */
    @ApiMethod(
            name = "remove",
            path = "mioFuseActivity/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(MioFuseActivity.class).id(_id).now();
        logger.info("Deleted MioFuseActivity with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "mioFuseActivity",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<MioFuseActivity> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<MioFuseActivity> query = ofy().load().type(MioFuseActivity.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<MioFuseActivity> queryIterator = query.iterator();
        List<MioFuseActivity> mioFuseActivityList = new ArrayList<MioFuseActivity>(limit);
        while (queryIterator.hasNext()) {
            mioFuseActivityList.add(queryIterator.next());
        }
        return CollectionResponse.<MioFuseActivity>builder().setItems(mioFuseActivityList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(MioFuseActivity.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find MioFuseActivity with ID: " + _id);
        }
    }

    /**
     * InsertAll {@code MioFuseHeartRate}.
     */
    @ApiMethod(
            name = "insertAll",
            path = "mioFuseHeartRates",
            httpMethod = ApiMethod.HttpMethod.POST)
    public MioFuseActivity insertAll(@Named("userId") String userId, @Named("timesMioFuseActivityJsonString") String timesMioFuseActivityJsonString) {
        logger.info("insertAll userId, timesMioFuseActivityJsonString : " +timesMioFuseActivityJsonString);
        Saver save = ofy().save();
        List<MioFuseActivity> steps = new ArrayList<>();
        long[][] batch = new Gson().fromJson(timesMioFuseActivityJsonString, long[][].class);
        for (int i = 0; i < batch.length; i++) {
            long[] row = batch[i];
            MioFuseActivity step = new MioFuseActivity();
            step.setUserId(userId);
            step.setTimestampMs(row[0]);
            step.setStepsInt((int) row[1]);
            steps.add(step);
        }
        save.entities(steps).now();
        logger.info("insertAll done, size : " + steps.size());
        return null;
    }
}