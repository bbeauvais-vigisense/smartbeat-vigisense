package com.smartbeat.vigisense.server.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class BpMonitor {
    @Id
    Long _id;
    @Index
    String userId;
    @Index
    Long timestampMs;
    Integer systoleInt;
    Integer diastoleInt;
    Integer heartRateInt;

    public BpMonitor() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getSystoleInt() {
        return systoleInt;
    }

    public void setSystoleInt(Integer systoleInt) {
        this.systoleInt = systoleInt;
    }

    public Integer getDiastoleInt() {
        return diastoleInt;
    }

    public void setDiastoleInt(Integer diastoleInt) {
        this.diastoleInt = diastoleInt;
    }

    public Integer getHeartRateInt() {
        return heartRateInt;
    }

    public void setHeartRateInt(Integer heartRateInt) {
        this.heartRateInt = heartRateInt;
    }

    @Override
    public String toString() {
        return "BpMonitor{" +
                "id=" + _id +
                ", userId='" + userId + '\'' +
                ", timestampMs=" + timestampMs +
                ", systoleInt=" + systoleInt +
                ", diastoleInt=" + diastoleInt +
                ", heartRateInt=" + heartRateInt +
                '}';
    }
}
