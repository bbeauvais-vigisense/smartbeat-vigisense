package com.smartbeat.vigisense.server.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
@Entity
public class Oximeter  {

    @Id
    Long _id;
    @Index
    String userId;
    @Index
    Long timestampMs;

    Integer spo2;

    public Oximeter() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }


    public Integer getSpo2() {
        return spo2;
    }

    public void setSpo2(Integer spo2) {
        this.spo2 = spo2;
    }

    @Override
    public String toString() {
        return "Oximeter{" +
                "_id=" + _id +
                ", userId='" + userId + '\'' +
                ", timestampMs=" + timestampMs +
                ", spo2=" + spo2 +
                '}';
    }
}