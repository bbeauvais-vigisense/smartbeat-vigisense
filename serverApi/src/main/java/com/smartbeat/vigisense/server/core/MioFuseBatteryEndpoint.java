package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.MioFuseBattery;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Api(
        name = "mioFuseBatteryApi",
        version = "v1",
        resource = "mioFuseBattery",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.smartbeat.com",
                ownerName = "api.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class MioFuseBatteryEndpoint {

    private static final Logger logger = Logger.getLogger(MioFuseBatteryEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(MioFuseBattery.class);
    }

    /**
     * Returns the {@link MioFuseBattery} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code MioFuseBattery} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "mioFuseBattery/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public MioFuseBattery get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting MioFuseBattery with ID: " + _id);
        MioFuseBattery mioFuseBattery = ofy().load().type(MioFuseBattery.class).id(_id).now();
        if (mioFuseBattery == null) {
            throw new NotFoundException("Could not find MioFuseBattery with ID: " + _id);
        }
        return mioFuseBattery;
    }

    /**
     * Inserts a new {@code MioFuseBattery}.
     */
    @ApiMethod(
            name = "insert",
            path = "mioFuseBattery",
            httpMethod = ApiMethod.HttpMethod.POST)
    public MioFuseBattery insert(MioFuseBattery mioFuseBattery) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that mioFuseBattery._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(mioFuseBattery).now();
        logger.info("Created MioFuseBattery with ID: " + mioFuseBattery.get_id());

        return ofy().load().entity(mioFuseBattery).now();
    }

    /**
     * Updates an existing {@code MioFuseBattery}.
     *
     * @param _id       the ID of the entity to be updated
     * @param mioFuseBattery the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseBattery}
     */
    @ApiMethod(
            name = "update",
            path = "mioFuseBattery/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public MioFuseBattery update(@Named("_id") Long _id, MioFuseBattery mioFuseBattery) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(mioFuseBattery).now();
        logger.info("Updated MioFuseBattery: " + mioFuseBattery);
        return ofy().load().entity(mioFuseBattery).now();
    }

    /**
     * Deletes the specified {@code MioFuseBattery}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseBattery}
     */
    @ApiMethod(
            name = "remove",
            path = "mioFuseBattery/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(MioFuseBattery.class).id(_id).now();
        logger.info("Deleted MioFuseBattery with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "mioFuseBattery",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<MioFuseBattery> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<MioFuseBattery> query = ofy().load().type(MioFuseBattery.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<MioFuseBattery> queryIterator = query.iterator();
        List<MioFuseBattery> mioFuseBatteryList = new ArrayList<MioFuseBattery>(limit);
        while (queryIterator.hasNext()) {
            mioFuseBatteryList.add(queryIterator.next());
        }
        return CollectionResponse.<MioFuseBattery>builder().setItems(mioFuseBatteryList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(MioFuseBattery.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find MioFuseBattery with ID: " + _id);
        }
    }
}