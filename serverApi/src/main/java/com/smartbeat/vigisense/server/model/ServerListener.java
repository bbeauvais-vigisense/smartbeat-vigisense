package com.smartbeat.vigisense.server.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
@Entity
public class ServerListener {
    @Id
    Long _id;
    @Index
    String url;

    public ServerListener() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ServerListener{" +
                "_id=" + _id +
                ", url='" + url + '\'' +
                '}';
    }
}
