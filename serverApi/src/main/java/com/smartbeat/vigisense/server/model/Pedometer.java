package com.smartbeat.vigisense.server.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Pedometer {

    @Id
    Long _id;
    @Index
    String userId;
    @Index
    Long timestampMs;
    Integer stepsInt;
    Float distanceMeterFloat;
    Float calorieFloat;

    public Pedometer() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getStepsInt() {
        return stepsInt;
    }

    public void setStepsInt(Integer stepsInt) {
        this.stepsInt = stepsInt;
    }

    public Float getDistanceMeterFloat() {
        return distanceMeterFloat;
    }

    public void setDistanceMeterFloat(Float distanceMeterFloat) {
        this.distanceMeterFloat = distanceMeterFloat;
    }

    public Float getCalorieFloat() {
        return calorieFloat;
    }

    public void setCalorieFloat(Float calorieFloat) {
        this.calorieFloat = calorieFloat;
    }

    @Override
    public String toString() {
        return "Pedometer{" +
                "_id=" + _id +
                ", userId='" + userId + '\'' +
                ", timestampMs=" + timestampMs +
                ", stepsInt=" + stepsInt +
                ", distanceMeterFloat=" + distanceMeterFloat +
                ", calorieFloat=" + calorieFloat +
                '}';
    }
}