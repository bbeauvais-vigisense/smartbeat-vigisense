package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.BodyScale;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Api(
        name = "bodyScaleApi",

        version = "v1",
        resource = "bodyScale",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.smartbeat.com",
                ownerName = "api.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class BodyScaleEndpoint {

    private static final Logger logger = Logger.getLogger(BodyScaleEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(BodyScale.class);
    }

    /**
     * Returns the {@link BodyScale} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code BodyScale} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "bodyScale/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public BodyScale get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting BodyScale with ID: " + _id);
        BodyScale bodyScale = ofy().load().type(BodyScale.class).id(_id).now();
        if (bodyScale == null) {
            throw new NotFoundException("Could not find BodyScale with ID: " + _id);
        }
        return bodyScale;
    }

    /**
     * Inserts a new {@code BodyScale}.
     */
    @ApiMethod(
            name = "insert",
            path = "bodyScale",
            httpMethod = ApiMethod.HttpMethod.POST)
    public BodyScale insert(BodyScale bodyScale) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that bodyScale._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(bodyScale).now();
        logger.info("Created BodyScale with ID: " + bodyScale.get_id());

        return ofy().load().entity(bodyScale).now();
    }

    /**
     * Updates an existing {@code BodyScale}.
     *
     * @param _id       the ID of the entity to be updated
     * @param bodyScale the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code BodyScale}
     */
    @ApiMethod(
            name = "update",
            path = "bodyScale/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public BodyScale update(@Named("_id") Long _id, BodyScale bodyScale) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(bodyScale).now();
        logger.info("Updated BodyScale: " + bodyScale);
        return ofy().load().entity(bodyScale).now();
    }

    /**
     * Deletes the specified {@code BodyScale}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code BodyScale}
     */
    @ApiMethod(
            name = "remove",
            path = "bodyScale/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(BodyScale.class).id(_id).now();
        logger.info("Deleted BodyScale with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "bodyScale",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<BodyScale> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<BodyScale> query = ofy().load().type(BodyScale.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<BodyScale> queryIterator = query.iterator();
        List<BodyScale> bodyScaleList = new ArrayList<BodyScale>(limit);
        while (queryIterator.hasNext()) {
            bodyScaleList.add(queryIterator.next());
        }
        return CollectionResponse.<BodyScale>builder().setItems(bodyScaleList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(BodyScale.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find BodyScale with ID: " + _id);
        }
    }
}