package com.smartbeat.vigisense.server.example;

import com.smartbeat.vigisense.server.api.Message;
import com.smartbeat.vigisense.server.core.PartnerEndpoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegisterListener extends HttpServlet {

    private static final Logger log;

    static {
        log = Logger.getLogger(PartnerEndpoint.class.getName());
        log.setLevel(Level.ALL);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String out = "";
//            out += registerListener(URLEncoder.encode(Message.SERVER_URL+"/"+HandlePush.class.getSimpleName(), "UTF-8"))+"\n";
            out += registerListener(URLEncoder.encode("http://your-domain.com/pushListener", "UTF-8"))+"\n";
            out += unregisterListener(URLEncoder.encode("http://your-domain.com/pushListener", "UTF-8"))+"\n";
            responseToClient(response, out);
        } catch (Exception e) {
            e.printStackTrace();
            responseToClient(response, e.getMessage());
        }
    }

    private void responseToClient(HttpServletResponse response, String out) throws IOException {
        log.fine(out);
        response.setHeader("Content-Type", "text/plain");
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter writer = response.getWriter();
        writer.write(out);
        writer.close();
    }

    /**
     * To build your request visit https://apis-explorer.appspot.com/apis-explorer/?base=https://vigisense-api.appspot.com/_ah/api#p/partnerApi/v1/
     */

    private String registerListener(String listenerServerUrl) throws Exception {
        //POST https://vigisense-api.appspot.com/_ah/api/partnerApi/v1/registerListener/http%3A%2F%2Fyour-domain.appspot.com%2FpushListener
        return postToPartnerApi("registerListener", listenerServerUrl);
    }

    private String unregisterListener(String listenerServerUrl) throws Exception {
        //POST https://vigisense-api.appspot.com/_ah/api/partnerApi/v1/unregisterListener/http%3A%2F%2your-domain.appspot.com%2FpushListener
        return postToPartnerApi("unregisterListener",listenerServerUrl);
    }

    private String postToPartnerApi(String urlApi, String values) throws Exception {
        URL url = new URL(Message.SERVER_API_URL + "/" + urlApi+"/"+values);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        int respCode = conn.getResponseCode();
        if (respCode == HttpURLConnection.HTTP_OK || respCode == HttpURLConnection.HTTP_NO_CONTENT) {
            StringBuffer response = new StringBuffer();
            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
            return "postToPartnerApi, url: " +url+", response: "+ response.toString();
        } else {
            return "error, code: " + conn.getResponseCode() + ", msg: " + conn.getResponseMessage()+", url: "+url;
        }
    }
}