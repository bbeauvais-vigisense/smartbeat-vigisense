package com.smartbeat.vigisense.server.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class BodyScale {

    @Id
    Long _id;
    @Index
    String userId;
    @Index
    Long timestampMs;
    Integer batteryPercentInt;
    Float weightKgFloat;
    Float fatPercentFloat;

    public BodyScale() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getBatteryPercentInt() {
        return batteryPercentInt;
    }

    public void setBatteryPercentInt(Integer batteryPercentInt) {
        this.batteryPercentInt = batteryPercentInt;
    }

    public Float getWeightKgFloat() {
        return weightKgFloat;
    }

    public void setWeightKgFloat(Float weightKgFloat) {
        this.weightKgFloat = weightKgFloat;
    }

    public Float getFatPercentFloat() {
        return fatPercentFloat;
    }

    public void setFatPercentFloat(Float fatPercentFloat) {
        this.fatPercentFloat = fatPercentFloat;
    }

    @Override
    public String toString() {
        return "BodyScale{" +
                "id=" + _id +
                ", userId='" + userId + '\'' +
                ", timestampMs=" + timestampMs +
                ", batteryPercentInt=" + batteryPercentInt +
                ", weightKgFloat=" + weightKgFloat +
                ", fatPercentFloat=" + fatPercentFloat +
                '}';
    }
}