package com.smartbeat.vigisense.server.model;


import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class MioFuseHeartRateDeltaDay {

    @Id
    Long _id;
    @Index
    String userId;
    @Index
    Long timestampMs;

    Integer median;

    Integer mean;

    Integer standardDeviation;

    Integer min;

    Integer firstDecile;
    Integer lastDecile;

    Integer max;

    Integer firstQuartile;
    Integer lastQuartile;

    Long sampleSize;

    public Integer getFirstQuartile() {
        return firstQuartile;
    }

    public void setFirstQuartile(Integer firstQuartile) {
        this.firstQuartile = firstQuartile;
    }

    public Integer getLastQuartile() {
        return lastQuartile;
    }

    public void setLastQuartile(Integer lastQuartile) {
        this.lastQuartile = lastQuartile;
    }

    public Long getSampleSize() {
        return sampleSize;
    }

    public void setSampleSize(Long sampleSize) {
        this.sampleSize = sampleSize;
    }

    public MioFuseHeartRateDeltaDay() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getMedian() {
        return median;
    }

    public void setMedian(Integer median) {
        this.median = median;
    }

    public Integer getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(Integer standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getFirstDecile() {
        return firstDecile;
    }

    public void setFirstDecile(Integer firstDecile) {
        this.firstDecile = firstDecile;
    }

    public Integer getLastDecile() {
        return lastDecile;
    }

    public void setLastDecile(Integer lastDecile) {
        this.lastDecile = lastDecile;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getMean() {
        return mean;
    }

    public void setMean(Integer mean) {
        this.mean = mean;
    }
}