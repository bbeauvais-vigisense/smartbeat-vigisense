package com.smartbeat.vigisense.server.example;

import com.google.appengine.repackaged.com.google.gson.Gson;
import com.google.appengine.repackaged.com.google.gson.JsonObject;
import com.google.appengine.repackaged.com.google.gson.JsonParser;
import com.smartbeat.vigisense.server.api.Message;
import com.smartbeat.vigisense.server.model.BodyScale;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HandlePush extends HttpServlet {

    private static final Logger log;

    static {
        log = Logger.getLogger(HandlePush.class.getName());
        log.setLevel(Level.ALL);
    }

//        {"userId":"eyJ0eXAiOiJKV1QiLCJhbGciO",
//            "timestampMs":1477760428381,
//            "heartRateInt":74,
//            "tableName":"MioFuseHeartRate"}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String out = handlePushRequest(request);
            responseToClient(response, out);
        } catch (Exception e) {
            e.printStackTrace();
            log.log(Level.SEVERE, e.toString(), e);
            responseToClient(response, e.getMessage());
        }
    }

    private void responseToClient(HttpServletResponse response, String out) throws IOException {
        log.fine(out);
        response.setHeader("Content-Type", "text/plain");
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter writer = response.getWriter();
        writer.write(out);
        writer.close();
    }
    private String handlePushRequest(HttpServletRequest request) throws IOException {
        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null)
            buffer.append(line);
        reader.close();
        String jsonString = URLDecoder.decode(buffer.toString(), "UTF-8");
        String out = "handlePushRequest, jsonString: "+ jsonString +"\n";
        JsonObject json = new JsonParser().parse(jsonString).getAsJsonObject();
        String tableName = json.get(Message.TABLE_NAME).getAsString();
        out += "handlePushRequest, tableName: "+ tableName +"\n";
        if (tableName.equals(Message.BODY_SCALE_TABLE)){
            out += "bodyScale, weight: "+json.get(Message.BODY_SCALE_WEIGHT_KG_FLOAT).getAsFloat()+"\n";
            BodyScale bodyScale = new Gson().fromJson(jsonString, BodyScale.class);
            out += "bodyScale: "+bodyScale+"\n";
        }
        return out;
    }
}