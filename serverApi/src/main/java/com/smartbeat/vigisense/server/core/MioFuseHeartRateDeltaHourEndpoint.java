package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.MioFuseHeartRateDeltaHour;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * WARNING: This generated code is intended as a sample or starting point for using a
 * Google Cloud Endpoints RESTful API with an Objectify entity. It provides no data access
 * restrictions and no data validation.
 * <p>
 * DO NOT deploy this code unchanged as part of a real application to real users.
 */
@Api(
        name = "mioFuseHeartRateDeltaHourApi",
        version = "v1",
        resource = "mioFuseHeartRateDeltaHour",
        namespace = @ApiNamespace(
                ownerDomain = "model.server.vigisense.smartbeat.com",
                ownerName = "model.server.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class MioFuseHeartRateDeltaHourEndpoint {

    private static final Logger logger = Logger.getLogger(MioFuseHeartRateDeltaHourEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(MioFuseHeartRateDeltaHour.class);
    }

    /**
     * Returns the {@link MioFuseHeartRateDeltaHour} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code MioFuseHeartRateDeltaHour} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "mioFuseHeartRateDeltaHour/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public MioFuseHeartRateDeltaHour get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting MioFuseHeartRateDeltaHour with ID: " + _id);
        MioFuseHeartRateDeltaHour mioFuseHeartRateDeltaHour = ofy().load().type(MioFuseHeartRateDeltaHour.class).id(_id).now();
        if (mioFuseHeartRateDeltaHour == null) {
            throw new NotFoundException("Could not find MioFuseHeartRateDeltaHour with ID: " + _id);
        }
        return mioFuseHeartRateDeltaHour;
    }

    /**
     * Inserts a new {@code MioFuseHeartRateDeltaHour}.
     */
    @ApiMethod(
            name = "insert",
            path = "mioFuseHeartRateDeltaHour",
            httpMethod = ApiMethod.HttpMethod.POST)
    public MioFuseHeartRateDeltaHour insert(MioFuseHeartRateDeltaHour mioFuseHeartRateDeltaHour) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that mioFuseHeartRateDeltaHour._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(mioFuseHeartRateDeltaHour).now();
        logger.info("Created MioFuseHeartRateDeltaHour with ID: " + mioFuseHeartRateDeltaHour.get_id());

        return ofy().load().entity(mioFuseHeartRateDeltaHour).now();
    }

    /**
     * Updates an existing {@code MioFuseHeartRateDeltaHour}.
     *
     * @param _id                       the ID of the entity to be updated
     * @param mioFuseHeartRateDeltaHour the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseHeartRateDeltaHour}
     */
    @ApiMethod(
            name = "update",
            path = "mioFuseHeartRateDeltaHour/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public MioFuseHeartRateDeltaHour update(@Named("_id") Long _id, MioFuseHeartRateDeltaHour mioFuseHeartRateDeltaHour) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(mioFuseHeartRateDeltaHour).now();
        logger.info("Updated MioFuseHeartRateDeltaHour: " + mioFuseHeartRateDeltaHour);
        return ofy().load().entity(mioFuseHeartRateDeltaHour).now();
    }

    /**
     * Deletes the specified {@code MioFuseHeartRateDeltaHour}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseHeartRateDeltaHour}
     */
    @ApiMethod(
            name = "remove",
            path = "mioFuseHeartRateDeltaHour/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(MioFuseHeartRateDeltaHour.class).id(_id).now();
        logger.info("Deleted MioFuseHeartRateDeltaHour with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "mioFuseHeartRateDeltaHour",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<MioFuseHeartRateDeltaHour> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<MioFuseHeartRateDeltaHour> query = ofy().load().type(MioFuseHeartRateDeltaHour.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<MioFuseHeartRateDeltaHour> queryIterator = query.iterator();
        List<MioFuseHeartRateDeltaHour> mioFuseHeartRateDeltaHourList = new ArrayList<MioFuseHeartRateDeltaHour>(limit);
        while (queryIterator.hasNext()) {
            mioFuseHeartRateDeltaHourList.add(queryIterator.next());
        }
        return CollectionResponse.<MioFuseHeartRateDeltaHour>builder().setItems(mioFuseHeartRateDeltaHourList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(MioFuseHeartRateDeltaHour.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find MioFuseHeartRateDeltaHour with ID: " + _id);
        }
    }
}