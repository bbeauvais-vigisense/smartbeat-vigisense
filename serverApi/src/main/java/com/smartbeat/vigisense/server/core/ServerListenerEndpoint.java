package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.ServerListener;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * WARNING: This generated code is intended as a sample or starting point for using a
 * Google Cloud Endpoints RESTful API with an Objectify entity. It provides no data access
 * restrictions and no data validation.
 * <p/>
 * DO NOT deploy this code unchanged as part of a real application to real users.
 */
@Api(
        name = "serverListenerApi",
        version = "v1",
        resource = "serverListener",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.smartbeat.com",
                ownerName = "api.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class ServerListenerEndpoint {

    private static final Logger logger = Logger.getLogger(ServerListenerEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(ServerListener.class);
    }

    /**
     * Returns the {@link ServerListener} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code ServerListener} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "serverListener/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public ServerListener get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting ServerListener with ID: " + _id);
        ServerListener serverListener = ofy().load().type(ServerListener.class).id(_id).now();
        if (serverListener == null) {
            throw new NotFoundException("Could not find ServerListener with ID: " + _id);
        }
        return serverListener;
    }

    /**
     * Inserts a new {@code ServerListener}.
     */
    @ApiMethod(
            name = "insert",
            path = "serverListener",
            httpMethod = ApiMethod.HttpMethod.POST)
    public ServerListener insert(ServerListener serverListener) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that serverListener._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(serverListener).now();
        logger.info("Created ServerListener with ID: " + serverListener.get_id());

        return ofy().load().entity(serverListener).now();
    }

    /**
     * Updates an existing {@code ServerListener}.
     *
     * @param _id            the ID of the entity to be updated
     * @param serverListener the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code ServerListener}
     */
    @ApiMethod(
            name = "update",
            path = "serverListener/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public ServerListener update(@Named("_id") Long _id, ServerListener serverListener) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(serverListener).now();
        logger.info("Updated ServerListener: " + serverListener);
        return ofy().load().entity(serverListener).now();
    }

    /**
     * Deletes the specified {@code ServerListener}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code ServerListener}
     */
    @ApiMethod(
            name = "remove",
            path = "serverListener/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(ServerListener.class).id(_id).now();
        logger.info("Deleted ServerListener with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "serverListener",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<ServerListener> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<ServerListener> query = ofy().load().type(ServerListener.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<ServerListener> queryIterator = query.iterator();
        List<ServerListener> serverListenerList = new ArrayList<ServerListener>(limit);
        while (queryIterator.hasNext()) {
            serverListenerList.add(queryIterator.next());
        }
        return CollectionResponse.<ServerListener>builder().setItems(serverListenerList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(ServerListener.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find ServerListener with ID: " + _id);
        }
    }
}