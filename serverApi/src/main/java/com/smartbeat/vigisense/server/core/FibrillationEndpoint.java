package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.Fibrillation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Api(
        name = "fibrillationApi",
        version = "v1",
        resource = "fibrillation",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.smartbeat.com",
                ownerName = "api.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class FibrillationEndpoint {

    private static final Logger logger = Logger.getLogger(FibrillationEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(Fibrillation.class);
    }

    /**
     * Returns the {@link Fibrillation} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code Fibrillation} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "fibrillation/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public Fibrillation get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting Fibrillation with ID: " + _id);
        Fibrillation fibrillation = ofy().load().type(Fibrillation.class).id(_id).now();
        if (fibrillation == null) {
            throw new NotFoundException("Could not find Fibrillation with ID: " + _id);
        }
        return fibrillation;
    }

    /**
     * Inserts a new {@code Fibrillation}.
     */
    @ApiMethod(
            name = "insert",
            path = "fibrillation",
            httpMethod = ApiMethod.HttpMethod.POST)
    public Fibrillation insert(Fibrillation fibrillation) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that fibrillation._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(fibrillation).now();
        logger.info("Created Fibrillation with ID: " + fibrillation.get_id());

        return ofy().load().entity(fibrillation).now();
    }

    /**
     * Updates an existing {@code Fibrillation}.
     *
     * @param _id          the ID of the entity to be updated
     * @param fibrillation the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code Fibrillation}
     */
    @ApiMethod(
            name = "update",
            path = "fibrillation/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public Fibrillation update(@Named("_id") Long _id, Fibrillation fibrillation) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(fibrillation).now();
        logger.info("Updated Fibrillation: " + fibrillation);
        return ofy().load().entity(fibrillation).now();
    }

    /**
     * Deletes the specified {@code Fibrillation}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code Fibrillation}
     */
    @ApiMethod(
            name = "remove",
            path = "fibrillation/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(Fibrillation.class).id(_id).now();
        logger.info("Deleted Fibrillation with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "fibrillation",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<Fibrillation> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<Fibrillation> query = ofy().load().type(Fibrillation.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<Fibrillation> queryIterator = query.iterator();
        List<Fibrillation> fibrillationList = new ArrayList<Fibrillation>(limit);
        while (queryIterator.hasNext()) {
            fibrillationList.add(queryIterator.next());
        }
        return CollectionResponse.<Fibrillation>builder().setItems(fibrillationList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(Fibrillation.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find Fibrillation with ID: " + _id);
        }
    }
}