package com.smartbeat.vigisense.server.model;


import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
@Entity
public class MioFuseDailyActivity  {

    @Id
    Long _id;
    @Index
    String userId;
    @Index
    Long timestampMs;
    Integer stepsInt;
    String dateStr;

    public MioFuseDailyActivity() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getStepsInt() {
        return stepsInt;
    }

    public void setStepsInt(Integer stepsInt) {
        this.stepsInt = stepsInt;
    }


    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    @Override
    public String toString() {
        return "MioFuseDailyActivity{" +
                "id=" + _id +
                ", userId='" + userId + '\'' +
                ", timestampMs='" + timestampMs + '\'' +
                ", stepsInt=" + stepsInt +
                ", dateStr=" + dateStr +
                '}';
    }


}