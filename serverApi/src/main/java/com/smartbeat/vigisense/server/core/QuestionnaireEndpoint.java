package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.Questionnaire;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Api(
        name = "questionnaireApi",
        version = "v1",
        resource = "questionnaire",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.smartbeat.com",
                ownerName = "api.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class QuestionnaireEndpoint {

    private static final Logger logger = Logger.getLogger(QuestionnaireEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(Questionnaire.class);
    }

    /**
     * Returns the {@link Questionnaire} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code Questionnaire} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "questionnaire/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public Questionnaire get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting Questionnaire with ID: " + _id);
        Questionnaire questionnaire = ofy().load().type(Questionnaire.class).id(_id).now();
        if (questionnaire == null) {
            throw new NotFoundException("Could not find Questionnaire with ID: " + _id);
        }
        return questionnaire;
    }

    /**
     * Inserts a new {@code Questionnaire}.
     */
    @ApiMethod(
            name = "insert",
            path = "questionnaire",
            httpMethod = ApiMethod.HttpMethod.POST)
    public Questionnaire insert(Questionnaire questionnaire) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that questionnaire._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(questionnaire).now();
        logger.info("Created Questionnaire with ID: " + questionnaire.get_id());

        return ofy().load().entity(questionnaire).now();
    }

    /**
     * Updates an existing {@code Questionnaire}.
     *
     * @param _id       the ID of the entity to be updated
     * @param questionnaire the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code Questionnaire}
     */
    @ApiMethod(
            name = "update",
            path = "questionnaire/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public Questionnaire update(@Named("_id") Long _id, Questionnaire questionnaire) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(questionnaire).now();
        logger.info("Updated Questionnaire: " + questionnaire);
        return ofy().load().entity(questionnaire).now();
    }

    /**
     * Deletes the specified {@code Questionnaire}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code Questionnaire}
     */
    @ApiMethod(
            name = "remove",
            path = "questionnaire/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(Questionnaire.class).id(_id).now();
        logger.info("Deleted Questionnaire with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "questionnaire",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<Questionnaire> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<Questionnaire> query = ofy().load().type(Questionnaire.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<Questionnaire> queryIterator = query.iterator();
        List<Questionnaire> questionnaireList = new ArrayList<Questionnaire>(limit);
        while (queryIterator.hasNext()) {
            questionnaireList.add(queryIterator.next());
        }
        return CollectionResponse.<Questionnaire>builder().setItems(questionnaireList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(Questionnaire.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find Questionnaire with ID: " + _id);
        }
    }
}