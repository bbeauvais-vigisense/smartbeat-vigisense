package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.MioFuseHeartRateDay;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * WARNING: This generated code is intended as a sample or starting point for using a
 * Google Cloud Endpoints RESTful API with an Objectify entity. It provides no data access
 * restrictions and no data validation.
 * <p>
 * DO NOT deploy this code unchanged as part of a real application to real users.
 */
@Api(
        name = "mioFuseHeartRateDayApi",
        version = "v1",
        resource = "mioFuseHeartRateDay",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.smartbeat.com",
                ownerName = "api.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class MioFuseHeartRateDayEndpoint {

    private static final Logger logger = Logger.getLogger(MioFuseHeartRateDayEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(MioFuseHeartRateDay.class);
    }

    /**
     * Returns the {@link MioFuseHeartRateDay} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code MioFuseHeartRateDay} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "mioFuseHeartRateDay/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public MioFuseHeartRateDay get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting MioFuseHeartRateDay with ID: " + _id);
        MioFuseHeartRateDay mioFuseHeartRateDay = ofy().load().type(MioFuseHeartRateDay.class).id(_id).now();
        if (mioFuseHeartRateDay == null) {
            throw new NotFoundException("Could not find MioFuseHeartRateDay with ID: " + _id);
        }
        return mioFuseHeartRateDay;
    }

    /**
     * Inserts a new {@code MioFuseHeartRateDay}.
     */
    @ApiMethod(
            name = "insert",
            path = "mioFuseHeartRateDay",
            httpMethod = ApiMethod.HttpMethod.POST)
    public MioFuseHeartRateDay insert(MioFuseHeartRateDay mioFuseHeartRateDay) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that mioFuseHeartRateDay._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(mioFuseHeartRateDay).now();
        logger.info("Created MioFuseHeartRateDay with ID: " + mioFuseHeartRateDay.get_id());

        return ofy().load().entity(mioFuseHeartRateDay).now();
    }

    /**
     * Updates an existing {@code MioFuseHeartRateDay}.
     *
     * @param _id                 the ID of the entity to be updated
     * @param mioFuseHeartRateDay the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseHeartRateDay}
     */
    @ApiMethod(
            name = "update",
            path = "mioFuseHeartRateDay/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public MioFuseHeartRateDay update(@Named("_id") Long _id, MioFuseHeartRateDay mioFuseHeartRateDay) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(mioFuseHeartRateDay).now();
        logger.info("Updated MioFuseHeartRateDay: " + mioFuseHeartRateDay);
        return ofy().load().entity(mioFuseHeartRateDay).now();
    }

    /**
     * Deletes the specified {@code MioFuseHeartRateDay}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseHeartRateDay}
     */
    @ApiMethod(
            name = "remove",
            path = "mioFuseHeartRateDay/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(MioFuseHeartRateDay.class).id(_id).now();
        logger.info("Deleted MioFuseHeartRateDay with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "mioFuseHeartRateDay",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<MioFuseHeartRateDay> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<MioFuseHeartRateDay> query = ofy().load().type(MioFuseHeartRateDay.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<MioFuseHeartRateDay> queryIterator = query.iterator();
        List<MioFuseHeartRateDay> mioFuseHeartRateDayList = new ArrayList<MioFuseHeartRateDay>(limit);
        while (queryIterator.hasNext()) {
            mioFuseHeartRateDayList.add(queryIterator.next());
        }
        return CollectionResponse.<MioFuseHeartRateDay>builder().setItems(mioFuseHeartRateDayList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(MioFuseHeartRateDay.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find MioFuseHeartRateDay with ID: " + _id);
        }
    }
}