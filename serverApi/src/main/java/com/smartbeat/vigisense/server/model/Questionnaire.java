package com.smartbeat.vigisense.server.model;



import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
@Entity
public class Questionnaire{

    @Id
    Long _id;
    @Index
    String userId;
    @Index
    Long timestampMs;
    String json;

    public Questionnaire() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    @Override
    public String toString() {
        return "Questionnaire{" +
                "id=" + _id +
                ", userId='" + userId + '\'' +
                ", timestampMs=" + timestampMs +
                ", json=" + json +
                '}';
    }
}