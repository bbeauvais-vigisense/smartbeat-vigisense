package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.BpMonitor;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Api(
        name = "bpMonitorApi",
        version = "v1",
        resource = "bpMonitor",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.smartbeat.com",
                ownerName = "api.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class BpMonitorEndpoint {

    private static final Logger logger = Logger.getLogger(BpMonitorEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(BpMonitor.class);
    }

    /**
     * Returns the {@link BpMonitor} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code BpMonitor} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "bpMonitor/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public BpMonitor get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting BpMonitor with ID: " + _id);
        BpMonitor bpMonitor = ofy().load().type(BpMonitor.class).id(_id).now();
        if (bpMonitor == null) {
            throw new NotFoundException("Could not find BpMonitor with ID: " + _id);
        }
        return bpMonitor;
    }

    /**
     * Inserts a new {@code BpMonitor}.
     */
    @ApiMethod(
            name = "insert",
            path = "bpMonitor",
            httpMethod = ApiMethod.HttpMethod.POST)
    public BpMonitor insert(BpMonitor bpMonitor) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that bpMonitor._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(bpMonitor).now();
        logger.info("Created BpMonitor with ID: " + bpMonitor.get_id());

        return ofy().load().entity(bpMonitor).now();
    }

    /**
     * Updates an existing {@code BpMonitor}.
     *
     * @param _id       the ID of the entity to be updated
     * @param bpMonitor the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code BpMonitor}
     */
    @ApiMethod(
            name = "update",
            path = "bpMonitor/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public BpMonitor update(@Named("_id") Long _id, BpMonitor bpMonitor) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(bpMonitor).now();
        logger.info("Updated BpMonitor: " + bpMonitor);
        return ofy().load().entity(bpMonitor).now();
    }

    /**
     * Deletes the specified {@code BpMonitor}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code BpMonitor}
     */
    @ApiMethod(
            name = "remove",
            path = "bpMonitor/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(BpMonitor.class).id(_id).now();
        logger.info("Deleted BpMonitor with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "bpMonitor",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<BpMonitor> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<BpMonitor> query = ofy().load().type(BpMonitor.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<BpMonitor> queryIterator = query.iterator();
        List<BpMonitor> bpMonitorList = new ArrayList<BpMonitor>(limit);
        while (queryIterator.hasNext()) {
            bpMonitorList.add(queryIterator.next());
        }
        return CollectionResponse.<BpMonitor>builder().setItems(bpMonitorList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(BpMonitor.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find BpMonitor with ID: " + _id);
        }
    }
}