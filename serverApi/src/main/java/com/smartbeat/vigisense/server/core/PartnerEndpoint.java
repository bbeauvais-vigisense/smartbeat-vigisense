
package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.api.Message;
import com.smartbeat.vigisense.server.model.BodyScale;
import com.smartbeat.vigisense.server.model.BpMonitor;
import com.smartbeat.vigisense.server.model.Fibrillation;
import com.smartbeat.vigisense.server.model.Medication;
import com.smartbeat.vigisense.server.model.MioFuseActivity;
import com.smartbeat.vigisense.server.model.MioFuseBattery;
import com.smartbeat.vigisense.server.model.MioFuseDailyActivity;
import com.smartbeat.vigisense.server.model.MioFuseHeartRate;
import com.smartbeat.vigisense.server.model.MioFuseHeartRateDay;
import com.smartbeat.vigisense.server.model.MioFuseHeartRateDeltaDay;
import com.smartbeat.vigisense.server.model.MioFuseHeartRateDeltaHour;
import com.smartbeat.vigisense.server.model.MioFuseHeartRateHour;
import com.smartbeat.vigisense.server.model.Oximeter;
import com.smartbeat.vigisense.server.model.Pedometer;
import com.smartbeat.vigisense.server.model.Questionnaire;
import com.smartbeat.vigisense.server.model.ServerListener;

import org.apache.commons.validator.routines.UrlValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Api(
        name = "partnerApi",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.smartbeat.com",
                ownerName = "api.vigisense.smartbeat.com",
                packagePath = ""
        )
)

public class PartnerEndpoint {

    private static final Logger log;
    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        log = Logger.getLogger(PartnerEndpoint.class.getName());
        log.setLevel(Level.ALL);
        ObjectifyService.register(Pedometer.class);
        ObjectifyService.register(BpMonitor.class);
        ObjectifyService.register(BodyScale.class);
        ObjectifyService.register(MioFuseActivity.class);
        ObjectifyService.register(MioFuseDailyActivity.class);
        ObjectifyService.register(MioFuseBattery.class);
        ObjectifyService.register(MioFuseHeartRate.class);
        ObjectifyService.register(MioFuseHeartRateDay.class);
        ObjectifyService.register(MioFuseHeartRateHour.class);
        ObjectifyService.register(MioFuseHeartRateDeltaHour.class);
        ObjectifyService.register(MioFuseHeartRateDeltaDay.class);
        ObjectifyService.register(Oximeter.class);
        ObjectifyService.register(Fibrillation.class);
        ObjectifyService.register(Medication.class);
        ObjectifyService.register(ServerListener.class);
    }

    public CollectionResponse<Oximeter> getLastOximeterMeasures(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastOximeter, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, Oximeter.class);
    }

    public CollectionResponse<MioFuseHeartRateHour> getLastMioFuseHeartRateHour(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("MioFuseHeartRateHour, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, MioFuseHeartRateHour.class);
    }

    public CollectionResponse<MioFuseHeartRateDay> getLastMioFuseHeartRateDay(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastMioFuseHeartRateDay, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, MioFuseHeartRateDay.class);
    }

    public CollectionResponse<MioFuseHeartRateDeltaHour> getLastMioFuseHeartRateDeltaHour(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastMioFuseHeartRateDeltaHour, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, MioFuseHeartRateDeltaHour.class);
    }

    public CollectionResponse<MioFuseHeartRateDeltaDay> getLastMioFuseHeartRateDeltaDay(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastMioFuseHeartRateDeltaDay, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, MioFuseHeartRateDeltaDay.class);
    }


    public CollectionResponse<BodyScale> getLastBodyScaleMeasures(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastBodyScaleMeasures, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, BodyScale.class);
    }

//    public CollectionResponse<Pedometer> getLastPedometerMeasures(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
//        log.info("getLastPedometerMeasures, limit: " + limit + ", userId: " + userId);
//        return queryLast(userId, limit, cursor, Pedometer.class);
//    }

    public CollectionResponse<BpMonitor> getLastBpMonitorMeasures(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastBpMonitorMeasures, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, BpMonitor.class);
    }

    public CollectionResponse<MioFuseDailyActivity> getLastMioFuseDailyActivity(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastMioFuseDailyActivity, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, MioFuseDailyActivity.class);
    }
    public CollectionResponse<MioFuseActivity> getLastMioFuseActivity(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastMioFuseActivity , limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, MioFuseActivity.class);
    }

    public CollectionResponse<MioFuseHeartRate> getLastMioFuseHeartRate(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastMioFuseHeartRate, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, MioFuseHeartRate.class);
    }

    public CollectionResponse<MioFuseBattery> getLastMioFuseBattery(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastMioFuseBattery, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, MioFuseBattery.class);
    }

    public CollectionResponse<Questionnaire> getLastQuestionnaire(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastQuestionnaire, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, Questionnaire.class);
    }

    public CollectionResponse<Fibrillation> getLastFibrillation(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastFibrillation, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, Fibrillation.class);
    }
    public CollectionResponse<Medication> getLastMedication(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor) {
        log.info("getLastFibrillation, limit: " + limit + ", userId: " + userId);
        return queryLast(userId, limit, cursor, Medication.class);
    }

    private <T> CollectionResponse<T> queryLast(@Named(Message.USER_ID) String userId, @Nullable @Named("limit") Integer limit, @Nullable @Named("cursor") String cursor, Class<T> type) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<T> query = ofy().load().type(type).limit(limit).order("-" + Message.TIMESTAMP_MS).filter(Message.USER_ID, userId);
        if (cursor != null)
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        QueryResultIterator<T> queryIterator = query.iterator();
        List<T> bodyScaleList = new ArrayList<>(limit);
        while (queryIterator.hasNext()) {
            bodyScaleList.add(queryIterator.next());
        }
        return CollectionResponse.<T>builder().setItems(bodyScaleList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    public void registerListener(@Named("url") String url) throws Exception {
        String[] schemes = {"http", "https"};
        UrlValidator urlValidator = new UrlValidator(schemes);
        if (!urlValidator.isValid(url))
            throw new Exception("not valid url: " + url);
        ServerListener listener = ofy().load().type(ServerListener.class).filter("url", url).first().now();
        if (null != listener)
            throw new Exception("url already registered: " + listener);
        ServerListener listenerServer = new ServerListener();
        listenerServer.setUrl(url);
        new ServerListenerEndpoint().insert(listenerServer);
    }

    public void unregisterListener(@Named("url") String url) throws Exception {
        ServerListener listener = ofy().load().type(ServerListener.class).filter("url", url).first().now();
        if (null == listener)
            throw new Exception("url not registered: " + url);
        new ServerListenerEndpoint().remove(listener.get_id());
    }

}
