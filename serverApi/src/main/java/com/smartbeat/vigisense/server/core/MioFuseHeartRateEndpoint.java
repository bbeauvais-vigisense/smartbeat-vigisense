package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.appengine.repackaged.com.google.gson.Gson;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.googlecode.objectify.cmd.Saver;
import com.smartbeat.vigisense.server.model.MioFuseHeartRate;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Api(
        name = "mioFuseHeartRateApi",
        version = "v1",
        resource = "mioFuseHeartRate",
        namespace = @ApiNamespace(
                ownerDomain = "api.vigisense.smartbeat.com",
                ownerName = "api.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class MioFuseHeartRateEndpoint {

    private static final Logger logger = Logger.getLogger(MioFuseHeartRateEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(MioFuseHeartRate.class);
    }

    /**
     * Returns the {@link MioFuseHeartRate} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code MioFuseHeartRate} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "mioFuseHeartRate/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public MioFuseHeartRate get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting MioFuseHeartRate with ID: " + _id);
        MioFuseHeartRate mioFuseHeartRate = ofy().load().type(MioFuseHeartRate.class).id(_id).now();
        if (mioFuseHeartRate == null) {
            throw new NotFoundException("Could not find MioFuseHeartRate with ID: " + _id);
        }
        return mioFuseHeartRate;
    }

    /**
     * Inserts a new {@code MioFuseHeartRate}.
     */
    @ApiMethod(
            name = "insert",
            path = "mioFuseHeartRate",
            httpMethod = ApiMethod.HttpMethod.POST)
    public MioFuseHeartRate insert(MioFuseHeartRate mioFuseHeartRate) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that mioFuseHeartRate._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(mioFuseHeartRate).now();
        logger.info("Created MioFuseHeartRate with ID: " + mioFuseHeartRate.get_id());

        return ofy().load().entity(mioFuseHeartRate).now();
    }

    /**
     * InsertAll {@code MioFuseHeartRate}.
     */
    @ApiMethod(
            name = "insertAll",
            path = "mioFuseHeartRates",
            httpMethod = ApiMethod.HttpMethod.POST)
    public MioFuseHeartRate insertAll(@Named("userId") String userId, @Named("timesHeartRatesJsonString") String timesHeartRatesJsonString) {
        logger.info("insertAll userId, timesHeartRatesJsonString : " +timesHeartRatesJsonString);
        Saver save = ofy().save();
        List<MioFuseHeartRate> hrs = new ArrayList<>();
        long[][] batch = new Gson().fromJson(timesHeartRatesJsonString, long[][].class);
        for (int i = 0; i < batch.length; i++) {
            long[] row = batch[i];
            MioFuseHeartRate hr = new MioFuseHeartRate();
            hr.setUserId(userId);
            hr.setTimestampMs(row[0]);
            hr.setHeartRateInt((int) row[1]);
            hrs.add(hr);
        }
        save.entities(hrs).now();
        logger.info("insertAll done, size : " + hrs.size());
        return null;
    }

    /**
     * Updates an existing {@code MioFuseHeartRate}.
     *
     * @param _id              the ID of the entity to be updated
     * @param mioFuseHeartRate the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseHeartRate}
     */
    @ApiMethod(
            name = "update",
            path = "mioFuseHeartRate/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public MioFuseHeartRate update(@Named("_id") Long _id, MioFuseHeartRate mioFuseHeartRate) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(mioFuseHeartRate).now();
        logger.info("Updated MioFuseHeartRate: " + mioFuseHeartRate);
        return ofy().load().entity(mioFuseHeartRate).now();
    }

    /**
     * Deletes the specified {@code MioFuseHeartRate}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseHeartRate}
     */
    @ApiMethod(
            name = "remove",
            path = "mioFuseHeartRate/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(MioFuseHeartRate.class).id(_id).now();
        logger.info("Deleted MioFuseHeartRate with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "mioFuseHeartRate",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<MioFuseHeartRate> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<MioFuseHeartRate> query = ofy().load().type(MioFuseHeartRate.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<MioFuseHeartRate> queryIterator = query.iterator();
        List<MioFuseHeartRate> mioFuseHeartRateList = new ArrayList<MioFuseHeartRate>(limit);
        while (queryIterator.hasNext()) {
            mioFuseHeartRateList.add(queryIterator.next());
        }
        return CollectionResponse.<MioFuseHeartRate>builder().setItems(mioFuseHeartRateList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(MioFuseHeartRate.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find MioFuseHeartRate  with ID: " + _id);
        }
    }
}