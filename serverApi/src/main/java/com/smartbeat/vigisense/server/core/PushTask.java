package com.smartbeat.vigisense.server.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PushTask extends HttpServlet {

    public static final String URL = "url";
    public static final String ROW = "row";
    private static final Logger log;

    static {
        log = Logger.getLogger(PartnerEndpoint.class.getName());
        log.setLevel(Level.ALL);
    }

    @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String urlStr = req.getParameter(URL);
            String jsonObjStr = req.getParameter(ROW);
            push(urlStr, jsonObjStr);
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getOutputStream().close();
        }catch(Exception e){
            log.severe(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void push(String urlStr, String jsonObjStr) throws IOException {
        log.info("doPost, url: " + urlStr + ", jsonObjStr: " + jsonObjStr);
        URL url = new URL(urlStr);
        HttpURLConnection conn = null;
        conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        writer.write(URLEncoder.encode(jsonObjStr, "UTF-8"));
        writer.close();

        int respCode = conn.getResponseCode();  // New items get NOT_FOUND on PUT
        if (respCode == HttpURLConnection.HTTP_OK) {
            StringBuffer response = new StringBuffer();
            String line;

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
            log.fine("response: " + response.toString());
        } else {
            throw new RuntimeException("error, code: " + conn.getResponseCode() + ", msg: " + conn.getResponseMessage());
        }
    }
}