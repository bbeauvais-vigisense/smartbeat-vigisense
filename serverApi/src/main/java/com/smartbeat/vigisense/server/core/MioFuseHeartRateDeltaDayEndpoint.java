package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.MioFuseHeartRateDeltaDay;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * WARNING: This generated code is intended as a sample or starting point for using a
 * Google Cloud Endpoints RESTful API with an Objectify entity. It provides no data access
 * restrictions and no data validation.
 * <p>
 * DO NOT deploy this code unchanged as part of a real application to real users.
 */
@Api(
        name = "mioFuseHeartRateDeltaDayApi",
        version = "v1",
        resource = "mioFuseHeartRateDeltaDay",
        namespace = @ApiNamespace(
                ownerDomain = "model.server.vigisense.smartbeat.com",
                ownerName = "model.server.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class MioFuseHeartRateDeltaDayEndpoint {

    private static final Logger logger = Logger.getLogger(MioFuseHeartRateDeltaDayEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(MioFuseHeartRateDeltaDay.class);
    }

    /**
     * Returns the {@link MioFuseHeartRateDeltaDay} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code MioFuseHeartRateDeltaDay} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "mioFuseHeartRateDeltaDay/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public MioFuseHeartRateDeltaDay get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting MioFuseHeartRateDeltaDay with ID: " + _id);
        MioFuseHeartRateDeltaDay mioFuseHeartRateDeltaDay = ofy().load().type(MioFuseHeartRateDeltaDay.class).id(_id).now();
        if (mioFuseHeartRateDeltaDay == null) {
            throw new NotFoundException("Could not find MioFuseHeartRateDeltaDay with ID: " + _id);
        }
        return mioFuseHeartRateDeltaDay;
    }

    /**
     * Inserts a new {@code MioFuseHeartRateDeltaDay}.
     */
    @ApiMethod(
            name = "insert",
            path = "mioFuseHeartRateDeltaDay",
            httpMethod = ApiMethod.HttpMethod.POST)
    public MioFuseHeartRateDeltaDay insert(MioFuseHeartRateDeltaDay mioFuseHeartRateDeltaDay) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that mioFuseHeartRateDeltaDay._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(mioFuseHeartRateDeltaDay).now();
        logger.info("Created MioFuseHeartRateDeltaDay with ID: " + mioFuseHeartRateDeltaDay.get_id());

        return ofy().load().entity(mioFuseHeartRateDeltaDay).now();
    }

    /**
     * Updates an existing {@code MioFuseHeartRateDeltaDay}.
     *
     * @param _id                      the ID of the entity to be updated
     * @param mioFuseHeartRateDeltaDay the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseHeartRateDeltaDay}
     */
    @ApiMethod(
            name = "update",
            path = "mioFuseHeartRateDeltaDay/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public MioFuseHeartRateDeltaDay update(@Named("_id") Long _id, MioFuseHeartRateDeltaDay mioFuseHeartRateDeltaDay) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(mioFuseHeartRateDeltaDay).now();
        logger.info("Updated MioFuseHeartRateDeltaDay: " + mioFuseHeartRateDeltaDay);
        return ofy().load().entity(mioFuseHeartRateDeltaDay).now();
    }

    /**
     * Deletes the specified {@code MioFuseHeartRateDeltaDay}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code MioFuseHeartRateDeltaDay}
     */
    @ApiMethod(
            name = "remove",
            path = "mioFuseHeartRateDeltaDay/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(MioFuseHeartRateDeltaDay.class).id(_id).now();
        logger.info("Deleted MioFuseHeartRateDeltaDay with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "mioFuseHeartRateDeltaDay",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<MioFuseHeartRateDeltaDay> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<MioFuseHeartRateDeltaDay> query = ofy().load().type(MioFuseHeartRateDeltaDay.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<MioFuseHeartRateDeltaDay> queryIterator = query.iterator();
        List<MioFuseHeartRateDeltaDay> mioFuseHeartRateDeltaDayList = new ArrayList<MioFuseHeartRateDeltaDay>(limit);
        while (queryIterator.hasNext()) {
            mioFuseHeartRateDeltaDayList.add(queryIterator.next());
        }
        return CollectionResponse.<MioFuseHeartRateDeltaDay>builder().setItems(mioFuseHeartRateDeltaDayList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(MioFuseHeartRateDeltaDay.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find MioFuseHeartRateDeltaDay with ID: " + _id);
        }
    }
}