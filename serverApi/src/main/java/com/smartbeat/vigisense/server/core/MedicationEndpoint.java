package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.smartbeat.vigisense.server.model.Medication;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * WARNING: This generated code is intended as a sample or starting point for using a
 * Google Cloud Endpoints RESTful API with an Objectify entity. It provides no data access
 * restrictions and no data validation.
 * <p>
 * DO NOT deploy this code unchanged as part of a real application to real users.
 */
@Api(
        name = "medicationApi",
        version = "v1",
        resource = "medication",
        namespace = @ApiNamespace(
                ownerDomain = "model.server.vigisense.smartbeat.com",
                ownerName = "model.server.vigisense.smartbeat.com",
                packagePath = ""
        )
)
public class MedicationEndpoint {

    private static final Logger logger = Logger.getLogger(MedicationEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(Medication.class);
    }

    /**
     * Returns the {@link Medication} with the corresponding ID.
     *
     * @param _id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code Medication} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "medication/{_id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public Medication get(@Named("_id") Long _id) throws NotFoundException {
        logger.info("Getting Medication with ID: " + _id);
        Medication medication = ofy().load().type(Medication.class).id(_id).now();
        if (medication == null) {
            throw new NotFoundException("Could not find Medication with ID: " + _id);
        }
        return medication;
    }

    /**
     * Inserts a new {@code Medication}.
     */
    @ApiMethod(
            name = "insert",
            path = "medication",
            httpMethod = ApiMethod.HttpMethod.POST)
    public Medication insert(Medication medication) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that medication._id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(medication).now();
        logger.info("Created Medication with ID: " + medication.get_id());

        return ofy().load().entity(medication).now();
    }

    /**
     * Updates an existing {@code Medication}.
     *
     * @param _id        the ID of the entity to be updated
     * @param medication the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code Medication}
     */
    @ApiMethod(
            name = "update",
            path = "medication/{_id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public Medication update(@Named("_id") Long _id, Medication medication) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(_id);
        ofy().save().entity(medication).now();
        logger.info("Updated Medication: " + medication);
        return ofy().load().entity(medication).now();
    }

    /**
     * Deletes the specified {@code Medication}.
     *
     * @param _id the ID of the entity to delete
     * @throws NotFoundException if the {@code _id} does not correspond to an existing
     *                           {@code Medication}
     */
    @ApiMethod(
            name = "remove",
            path = "medication/{_id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("_id") Long _id) throws NotFoundException {
        checkExists(_id);
        ofy().delete().type(Medication.class).id(_id).now();
        logger.info("Deleted Medication with ID: " + _id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "medication",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<Medication> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<Medication> query = ofy().load().type(Medication.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<Medication> queryIterator = query.iterator();
        List<Medication> medicationList = new ArrayList<Medication>(limit);
        while (queryIterator.hasNext()) {
            medicationList.add(queryIterator.next());
        }
        return CollectionResponse.<Medication>builder().setItems(medicationList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(Long _id) throws NotFoundException {
        try {
            ofy().load().type(Medication.class).id(_id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find Medication with ID: " + _id);
        }
    }
}