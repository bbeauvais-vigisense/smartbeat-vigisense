package com.smartbeat.vigisense.server.core;

import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.RetryOptions;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.repackaged.com.google.gson.Gson;
import com.google.appengine.repackaged.com.google.gson.JsonElement;
import com.googlecode.objectify.ObjectifyService;
import com.smartbeat.vigisense.server.api.Message;
import com.smartbeat.vigisense.server.model.BodyScale;
import com.smartbeat.vigisense.server.model.BpMonitor;
import com.smartbeat.vigisense.server.model.Fibrillation;
import com.smartbeat.vigisense.server.model.MioFuseBattery;
import com.smartbeat.vigisense.server.model.MioFuseDailyActivity;
import com.smartbeat.vigisense.server.model.MioFuseHeartRateDay;
import com.smartbeat.vigisense.server.model.MioFuseHeartRateHour;
import com.smartbeat.vigisense.server.model.Oximeter;
import com.smartbeat.vigisense.server.model.Questionnaire;
import com.smartbeat.vigisense.server.model.ServerListener;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CreateDemoData extends HttpServlet {

    public static final String USER_ID = "userid";
    public static final long MIN = 60 * 1000;
    public static final long HOUR = 60 * MIN;
    public static final long DAY = 24 * HOUR;
    private static final Logger log;
    private static final String DAY_NUMBER = "days";
    private  Queue queue;

    static {
        log = Logger.getLogger(PartnerEndpoint.class.getName());
        log.setLevel(Level.ALL);
    }
    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(BodyScale.class);
        ObjectifyService.register(BpMonitor.class);
        ObjectifyService.register(MioFuseDailyActivity.class);
        ObjectifyService.register(MioFuseBattery.class);
        ObjectifyService.register(Questionnaire.class);
        ObjectifyService.register(MioFuseHeartRateHour.class);
        ObjectifyService.register(MioFuseHeartRateDay.class);
        ObjectifyService.register(Fibrillation.class);
        ObjectifyService.register(Oximeter.class);
    }

    @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.setHeader("Content-Type", "text/plain");
            PrintWriter writer = resp.getWriter();
            int dayMax = 3;
            try {
                String daysNumberStr = req.getParameter(DAY_NUMBER);
                if (daysNumberStr == null || daysNumberStr.equals(""))
                    daysNumberStr = "3";
                dayMax =  Integer.valueOf(daysNumberStr);
                if (dayMax > 31)
                    dayMax = 31;
            }catch(Exception e){
                log.info(e.getMessage());
            }

            String userId = req.getParameter(USER_ID);
            if (userId==null || userId.equals("")) {
                userId = "demo"+((int)(Math.random() * 1000));
            }
            long now = System.currentTimeMillis();
            long start = now - dayMax * DAY;
            writer.println("Data generation for userId: "+userId+", from "+new SimpleDateFormat("yyyy/MM/dd HH").format(new Date(start))+"h");
            for (int day = 0 ; day <= dayMax; day++){

                BodyScale bodyScale = new BodyScale();
                bodyScale.setUserId(userId);
                bodyScale.setTimestampMs(start + day * DAY + rndLong(10 * MIN));
                bodyScale.setBatteryPercentInt(rndInt(70,80));
                bodyScale.setFatPercentFloat(rndFloat(20,30));
                bodyScale.setWeightKgFloat(rndFloat(70,80));
                save(bodyScale, writer);

                BpMonitor bp = new BpMonitor();
                bp.setUserId(userId);
                bp.setTimestampMs(start + day * DAY + 10 * MIN + rndLong(10 * MIN));
                bp.setSystoleInt(rndInt(150, 90));
                bp.setDiastoleInt(rndInt(50, 90));
                bp.setHeartRateInt(rndInt(50, 180));
                save(bp, writer);

                Oximeter oximeter = new Oximeter();
                bp.setUserId(userId);
                bp.setTimestampMs(start + day * DAY + 10 * MIN + rndLong(10 * MIN));
                oximeter.setSpo2(rndInt(85, 99));
                save(oximeter, writer);

                MioFuseDailyActivity daily = new MioFuseDailyActivity();
                daily.setUserId(userId);
                daily.setTimestampMs(start + day * DAY + 20 * MIN + rndLong(10 * MIN));
                daily.setStepsInt(rndInt(1000, 4000));
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(start + day * DAY + 20 * MIN + rndLong(10 * MIN));
                Integer dayStr = c.get(Calendar.DAY_OF_MONTH);
                Integer month = c.get(Calendar.MONTH) + 1;
                Integer year = c.get(Calendar.YEAR);
                daily.setDateStr(year + "/" + month + "/" + dayStr);
                save(daily, writer);

                MioFuseBattery battery = new MioFuseBattery();
                battery.setUserId(userId);
                battery.setBatteryPercentInt(rndInt(10, 100));
                battery.setTimestampMs(start + day * DAY + 30 * MIN + rndLong(10 * MIN));
                save(battery, writer);

                Questionnaire questionnaire = new Questionnaire();
                questionnaire.setUserId(userId);
                questionnaire.setTimestampMs(start + day * DAY + 40 * MIN + rndLong(10 * MIN));
                questionnaire.setJson("{\"questionnaire\":[{\"answer\":\"No\",\"answer_index\":1,\"question\":\"1.Since your last checkup, did you faint?\"},\n" +
                        "                {\"answer\":\"No\",\"answer_index\":0,\"question\":\"2.Do you have difficulties in breathing, when you are laying at night?\"},\n" +
                        "                {\"answer\":\"Yes, for medium efforts\",\"answer_index\":2,\"question\":\"3.Do you have difficulties in breathing, during daily activities?\"}]}");
                save(questionnaire, writer);

                Fibrillation fibrillation = new Fibrillation();
                fibrillation.setUserId(userId);
                fibrillation.setTimestampMs(start + day * DAY + 40 * MIN + rndLong(10 * MIN));
                fibrillation.setJson("[{\"heart_beat\":\""+rndInt(50,150)+"\",\"fibrillation\":\""+((rndInt(0,20) > 19) ? "yes":"no")+"\"}]");
                save(fibrillation, writer);

                MioFuseHeartRateDay hrDay = new MioFuseHeartRateDay();
                hrDay.setTimestampMs(start + day * DAY );
                hrDay.setUserId(userId);
                hrDay.setMin(rndInt(45,60));
                hrDay.setFirstDecile(rndInt(60,70));
                hrDay.setMedian(rndInt(60,80));
                hrDay.setMean(rndInt(70,100));
                hrDay.setLastDecile(rndInt(110,120));
                hrDay.setMax(rndInt(120,140));
                hrDay.setStandardDeviation(rndInt(1,10));
                save(hrDay, writer);
                for (int i = 0; i < 24; i++) {
                    MioFuseHeartRateHour hrHour = new MioFuseHeartRateHour();
                    hrHour.setTimestampMs(start + day * DAY + HOUR * i);
                    hrHour.setUserId(userId);
                    hrHour.setMin(rndInt(45,60));
                    hrHour.setFirstDecile(rndInt(60,70));
                    hrHour.setMedian(rndInt(60,80));
                    hrHour.setMean(rndInt(70,100));
                    hrHour.setLastDecile(rndInt(110,120));
                    hrHour.setMax(rndInt(120,140));
                    hrHour.setStandardDeviation(rndInt(1,10));
                    save(hrHour, writer);
                }
            }


            resp.setStatus(HttpServletResponse.SC_OK);
            writer.close();
        }catch(Exception e){
            log.severe(e.getMessage());
            e.printStackTrace();
        }
    }

    private void save(Object entity, PrintWriter writer) {
        try {
        ofy().save().entity(entity).now();
        String row =  push(entity);
        writer.println(row);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  String push(Object row) {
        CollectionResponse<ServerListener> servers = new ServerListenerEndpoint().list(null, 50);
        JsonElement jsonElement = new Gson().toJsonTree(row);
        jsonElement.getAsJsonObject().addProperty(Message.TABLE_NAME, row.getClass().getSimpleName());
        String jsonString = jsonElement.toString();
        for (ServerListener server: servers.getItems()) {
            try {
//                PushTask.push(jsonString,server.getUrl());
                addTask(jsonString, server);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonString;
    }

    private  void addTask(String jsonString, ServerListener server) {
        if (queue==null)
            queue = QueueFactory.getDefaultQueue();
        queue.add(TaskOptions.Builder.withUrl("/"+PushTask.class.getSimpleName())
                .method(TaskOptions.Method.POST)
                .param(PushTask.ROW, jsonString)
                .param(PushTask.URL, server.getUrl())
                .retryOptions(RetryOptions.Builder.withTaskRetryLimit(3))
        );
    }
    private long rndTime(long maxDelay) {
        return System.currentTimeMillis() - rndLong(maxDelay);
    }

    private float rndFloat(int min, int max) {
        return (float) (min + Math.random() * (max - min));
    }

    private int rndInt(int min, int max) {
        return (int) (min + Math.random() * (max - min));
    }

    private long rndLong(long maxDelay) {
        return (long) (Math.random() * maxDelay);
    }

}