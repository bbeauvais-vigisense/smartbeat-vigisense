package com.smartbeat.vigisense.server.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
@Entity
public class MioFuseHeartRate  {

    @Id
    Long _id;
    @Index
    String userId;
    @Index
    Long timestampMs;
    Integer heartRateInt;

    public MioFuseHeartRate() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getHeartRateInt() {
        return heartRateInt;
    }

    public void setHeartRateInt(Integer heartRateInt) {
        this.heartRateInt = heartRateInt;
    }

    @Override
    public String toString() {
        return "MioFuseHeartRate{" +
                "id=" + _id +
                ", userId='" + userId + '\'' +
                ", timestampMsLong=" + timestampMs +
                ", heartRateInt=" + heartRateInt +
                '}';
    }
}