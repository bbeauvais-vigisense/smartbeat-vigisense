package com.smartbeat.vigisense.server.example;

import com.google.appengine.repackaged.com.google.gson.Gson;
import com.google.appengine.repackaged.com.google.gson.JsonArray;
import com.google.appengine.repackaged.com.google.gson.JsonElement;
import com.google.appengine.repackaged.com.google.gson.JsonParser;
import com.google.appengine.repackaged.com.google.gson.reflect.TypeToken;
import com.smartbeat.vigisense.server.api.Message;
import com.smartbeat.vigisense.server.model.BodyScale;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PullData extends HttpServlet {
    private static final Logger log;

    static {
        log = Logger.getLogger(HandlePush.class.getName());
        log.setLevel(Level.ALL);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String out = pullLastBodyScaleMeasures(Message.DEMO_USER_ID_VALUE,10);
            responseToClient(response, out);
        } catch (Exception e) {
            e.printStackTrace();
            log.log(Level.SEVERE, e.toString(), e);
            responseToClient(response, e.getMessage());
        }
    }

    private void responseToClient(HttpServletResponse response, String out) throws IOException {
        log.fine(out);
        response.setHeader("Content-Type", "text/plain");
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter writer = response.getWriter();
        writer.write(out);
        writer.close();
    }
    /**
     * To build your request visit https://apis-explorer.appspot.com/apis-explorer/?base=https://vigisense-api.appspot.com/_ah/api#p/partnerApi/v1/
     */
    private String pullLastBodyScaleMeasures(String userId, Integer limit) throws Exception {
        //https://vigisense-api.appspot.com/_ah/api/partnerApi/v1/collectionresponse_bodyscale/demoUserId?limit=10
        return postToPartnerApi("collectionresponse_bodyscale",userId+"?limit="+limit);
    }
    private String postToPartnerApi(String apiUrl, String values) throws Exception {
        URL url = new URL(Message.SERVER_API_URL + "/" + apiUrl+"/"+values);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        int respCode = conn.getResponseCode();
        if (respCode == HttpURLConnection.HTTP_OK || respCode == HttpURLConnection.HTTP_NO_CONTENT) {
            StringBuffer response = new StringBuffer();
            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
            String jsonString = URLDecoder.decode(response.toString(), "UTF-8");
            String out = "postToPartnerApi, url: " +url+", response: "+ jsonString+"\n";
            log.fine("postToPartnerApi, jsonString: "+jsonString);
            JsonArray jsonArray = new JsonParser().parse(jsonString).getAsJsonObject().get("items").getAsJsonArray();
            out += "\n";
            for(final JsonElement jsonObject : jsonArray)
                out += "bodyScale, weight: "+jsonObject.getAsJsonObject().get(Message.BODY_SCALE_WEIGHT_KG_FLOAT).getAsFloat()+"\n";
            out += "\n";
            List<BodyScale> bodyScales = new Gson().fromJson(jsonArray,  new TypeToken<List<BodyScale>>(){}.getType());
            out += "bodyScale list: "+bodyScales+"\n";
            return out;
        } else {
            return "error, code: " + conn.getResponseCode() + ", msg: " + conn.getResponseMessage()+", url: "+url;
        }
    }
}