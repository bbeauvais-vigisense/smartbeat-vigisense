# Smartbeat: Vigisense Components Integration (updated 22/02/2017) #

## Architecture ##

![Architecture.png](https://docs.google.com/drawings/d/1a-9P7zCh0yiQFZu8KaLuCLcG0_G-WgUR91VS2xJP2sE/pub?w=511&amp;h=195)

## Features ##

 * Supported bluetooth devices : 
    * Zeroner activity tracker (not part of the project anymore)
    * Aoem U80E blood pressure monitor
    * Yunmai body scale
    * Mio Fuse. [User guide](http://help.mioglobal.com/hc/en-us/categories/200229234-Mio-FUSE-)
    * Oximeter JPD-500F
 * Extra fields :
    * heart rate every minute. Server synchronisation every hour. 
    * heart rate aggregation daily and hourly
    * Questionnaire
    * Fibrillation

* Minimum android version : 5.1
* OAuth 2.0 is still not enabled

## Server component ##
**Subscribe to get new data**

```
#!java
import com.smartbeat.vigisense.server.api.Message;
//POST https://vigisense-api.appspot.com/_ah/api/partnerApi/v1/registerListener/http%3A%2F%2Fyour-domain.com%2Fyour-path
URL urlToQueryAsHttpPost = new java.net.URL(Message.SERVER_API_URL + "/registerListener/"+ URLEncoder.encode("http://your-domain.com/your-path", "UTF-8"));
```

* Example source code: [com.smartbeat.vigisense.server.example.RegisterListener](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/serverApi/src/main/java/com/smartbeat/vigisense/server/example/RegisterListener.java?at=master&fileviewer=file-view-default)

* Live test: [http://vigisense-api.appspot.com/RegisterListener](http://vigisense-api.appspot.com/RegisterListener)

**Handle published data (JSON)**


```
#!java
import com.smartbeat.vigisense.server.model.BodyScale;
import com.google.gson.*; //or other JSON library
BodyScale BodyScale = new Gson().fromJson(URLDecoder.decode(postedString, "UTF-8"), BodyScale.class);
```


* Field name references: [com.smartbeat.vigisense.server.api.Message](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/serverApi/src/main/java/com/smartbeat/vigisense/server/api/Message.java?at=master&fileviewer=file-view-default)

* Example source code: [com.smartbeat.vigisense.server.example.HandlePush](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/serverApi/src/main/java/com/smartbeat/vigisense/server/example/HandlePush.java?at=master&fileviewer=file-view-default)

**Pull data**

```
#!java
import com.smartbeat.vigisense.server.api.Message;
import com.smartbeat.vigisense.server.model.BodyScale;
import com.google.gson.*; //or other JSON library
//GET https://vigisense-api.appspot.com/_ah/api/partnerApi/v1/collectionresponse_bodyscale/demoUserId?limit=10
URL urlToQueryAsHttpGet = new java.net.URL(Message.SERVER_API_URL + "/collectionresponse_bodyscale/"+someUserId);
...
List<BodyScale> bodyScales = new Gson().fromJson(new JsonParser().parse(URLDecoder.decode(responseOfHttpGetMethod, "UTF-8")).getAsJsonObject().get("items").getAsJsonArray(),  new TypeToken<List<BodyScale>>(){}.getType());

```
* Explore the pull api:  https://apis-explorer.appspot.com/apis-explorer/?base=https://vigisense-api.appspot.com/_ah/api#p/partnerApi/v1/

* Example source code: [com.smartbeat.vigisense.server.example.PullData](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/serverApi/src/main/java/com/smartbeat/vigisense/server/example/PullData.java?at=master&fileviewer=file-view-default)

* Live test: [http://vigisense-api.appspot.com/PullData](http://vigisense-api.appspot.com/PullData)

**Access to server backend**

Access to the Google Cloud Platform project ([logs](https://console.cloud.google.com/logs?project=vigisense-api), [database](https://console.cloud.google.com/datastore?project=vigisense-api), ...): [https://console.cloud.google.com/?project=vigisense-api](https://console.cloud.google.com/?project=vigisense-api) 

 * login: vigisense.partner@gmail.com
 * password: vigisense

**Create demo data**

You can create data sample generated randomly using the url [http://vigisense-api.appspot.com/CreateDemoData](http://vigisense-api.appspot.com/CreateDemoData)

For example to create sample data for last 5 days for the user token "demo123" : [http://vigisense-api.appspot.com/CreateDemoData?userId=demo123&days=5](http://vigisense-api.appspot.com/CreateDemoData?userId=demo123&days=5)


## Android component ##

**Installation, project [integration ](Link URL)& Live Test**

* Install from the play store [https://play.google.com/store/apps/details?id=com.smartbeat.vigisense](https://play.google.com/store/apps/details?id=com.smartbeat.vigisense) (without Mio Fuse & Questionnaire support)

* Install manually from the [apk](http://vigisense-api.appspot.com/smartbeat-vigisense.apk) (03/08/2016)

* Download the source code [https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/downloads](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/downloads)

* Clone this repository and integrate the source code into your project. The source code is ready to be imported into android studio.

```
#!
git clone https://bbeauvais-vigisense@bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense.git

```

**Push cmds**

```
#!java
import com.smartbeat.vigisense.android.api.Input;
sendBroadcast(new Intent().setAction(Input.START_SCAN_ACTION /*"com.smartbeat.vigisense.start_scan"*/)
    .putExtra(Input.START_SCAN_USER_ID_STR /*"userIdStr"*/, "someUserId");
```


* Example source code: [com.smartbeat.vigisense.android.example.InputActivity](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/app/src/main/java/com/smartbeat/vigisense/android/example/InputActivity.java?at=master&fileviewer=file-view-default)

* Action references: [com.smartbeat.vigisense.android.api.Input](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/app/src/main/java/com/smartbeat/vigisense/android/api/Input.java?at=master&fileviewer=file-view-default)

**Handle broadcasted data**


```
#!java
import com.smartbeat.vigisense.android.api.Output;
if (intent.getAction().equals(Output.BODY_SCALER_MEASURE_ACTION /*"com.smartbeat.vigisense.body_scaler_measure"*/))
    float weight = intent.getFloatExtra(Output.BODY_SCALER_WEIGHT_FLOAT /*"weightFloat"*/, -1);
```

* Example source code: [com.smartbeat.vigisense.android.example.OutputReceiver](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/app/src/main/java/com/smartbeat/vigisense/android/example/OutputReceiver.java?at=master&fileviewer=file-view-default)

* Action references: [com.smartbeat.vigisense.android.api.Output](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/app/src/main/java/com/smartbeat/vigisense/android/api/Output.java?at=master&fileviewer=file-view-default)

**Pull data**


```
#!java
import com.smartbeat.vigisense.android.api.Output;
import com.smartbeat.vigisense.android.api.Database;
Uri contentProviderUri = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT)
  .authority(Database.PROVIDER_NAME) //.authority("com.smartbeat.provider.Vigisense")
  .appendPath(Database.PEDOMETER_TABLE) //.appendPath("Pedometer")
  .build();
Cursor cursor = getContentResolver().query(contentProviderUri, null, null, null, Database.TIMESTAMP_MS+" DESC" /*"timestampMs DESC"*/);
if (!cursor .moveToFirst())
 float weight = cursor.getString(cursor.getColumnIndex(Database.PEDOMETER_STEPS_INT /*"stepsInt"*/));

```

* Example source code: [com.smartbeat.vigisense.android.example.InputActivity](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/app/src/main/java/com/smartbeat/vigisense/android/example/InputActivity.java?at=master&fileviewer=file-view-default)

* Database references: [com.smartbeat.vigisense.android.api.Database](https://bitbucket.org/bbeauvais-vigisense/smartbeat-vigisense/src/d5ea03b213a0820958f3e7b6e91e1eb687d7f811/app/src/main/java/com/smartbeat/vigisense/android/api/Database.java?at=master&fileviewer=file-view-default)