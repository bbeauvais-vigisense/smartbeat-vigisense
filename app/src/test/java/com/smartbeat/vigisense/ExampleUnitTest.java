package com.smartbeat.vigisense;

import org.junit.Test;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        SimpleDateFormat ddMMyyy = new SimpleDateFormat("dd-MM-yyyy");
        System.out.append(ddMMyyy.parse("27-03-2016").getTime()+"");
        assertEquals(4, 2 + 2);
    }
}