package com.smartbeat.vigisense.android.model;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

@AdditionalAnnotation.Contract()
@DatabaseTable(tableName = "Oximeter")
@AdditionalAnnotation.DefaultContentUri(authority = "com.smartbeat.provider.Vigisense", path = "Oximeter")
@AdditionalAnnotation.DefaultContentMimeTypeVnd(name = "com.smartbeat.provider.Vigisense", type = "Oximeter")
public class Oximeter extends MyBean {

    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    @AdditionalAnnotation.DefaultSortOrder
    Long _id;
    @DatabaseField(defaultValue = "true")
    Boolean sync;

    @DatabaseField(canBeNull = false)
    String userId;

    @DatabaseField(canBeNull = false)
    Long timestampMs;

    @DatabaseField(canBeNull = false)
    Integer spo2;

    public Oximeter() {
    }

    @Override
    public Long get_id() {
        return _id;
    }

    @Override
    public void set_id(Long _id) {
        this._id = _id;
    }

    @Override
    public Boolean getSync() {
        return sync;
    }

    @Override
    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getSpo2() {
        return spo2;
    }

    public void setSpo2(Integer spo2) {
        this.spo2 = spo2;
    }

    @Override
    public String toString() {
        return "Oximeter{" +
                "_id=" + _id +
                ", sync=" + sync +
                ", userId='" + userId + '\'' +
                ", timestampMs=" + timestampMs +
                ", spo2=" + spo2 +
                '}';
    }
}