package com.smartbeat.vigisense.android.core;

import java.util.Timer;
import java.util.TimerTask;

public class MyLooper {
    private Timer timer;
    private TimerTask task;
    private boolean canceled;

    public MyLooper(final long period, final Runnable looper) {
    this(0,period,looper);
    }

    public MyLooper(final long delay,final long period, final Runnable looper) {
        timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                looper.run();
            }
        };
        timer.scheduleAtFixedRate(task, delay, period);
    }

    public void cancel() {
        if (timer!=null){
            timer.cancel();
            timer.purge();
        }
        canceled = true;
    }

    public boolean isCanceled() {
        return canceled;
    }
}
