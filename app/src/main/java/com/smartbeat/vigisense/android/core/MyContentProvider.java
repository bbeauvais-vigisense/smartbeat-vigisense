package com.smartbeat.vigisense.android.core;

import com.smartbeat.vigisense.android.model.MyBean;
import com.tojc.ormlite.android.OrmLiteSimpleContentProvider;
import com.tojc.ormlite.android.framework.MatcherController;
import com.tojc.ormlite.android.framework.MimeTypeVnd;

public class MyContentProvider extends OrmLiteSimpleContentProvider<MyDatabase> {
    @Override
    protected Class<MyDatabase> getHelperClass() {
        return MyDatabase.class;
    }

    @Override
    public boolean onCreate()
    {
        MatcherController uriMatcherCtr = new MatcherController();
        int count = 1;
        for (MyBean bean: PlugYouTables.getTables()){
            uriMatcherCtr.add( bean.getClass())
                    .add(MimeTypeVnd.SubType.DIRECTORY, "", count)
                    .add(MimeTypeVnd.SubType.ITEM, "#", count+1);
            count+= 2;
        }
        setMatcherController(uriMatcherCtr);
        return true;
    }

}
