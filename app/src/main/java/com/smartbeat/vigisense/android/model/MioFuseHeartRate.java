package com.smartbeat.vigisense.android.model;


import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

@AdditionalAnnotation.Contract()
@DatabaseTable(tableName = "MioFuseHeartRate")
@AdditionalAnnotation.DefaultContentUri(authority = "com.smartbeat.provider.Vigisense", path = "MioFuseHeartRate")
@AdditionalAnnotation.DefaultContentMimeTypeVnd(name = "com.smartbeat.provider.Vigisense", type = "MioFuseHeartRate")
public class MioFuseHeartRate extends MyBean {

    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    @AdditionalAnnotation.DefaultSortOrder
    Long _id;
    @DatabaseField(defaultValue = "false")
    Boolean sync;
    @DatabaseField(canBeNull = false)
    String userId;
    @DatabaseField(canBeNull = false)
    Long timestampMs;
    @DatabaseField(canBeNull = false)
    Integer heartRateInt;

    public MioFuseHeartRate() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public Boolean getSync() {
        return sync;
    }

    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getHeartRateInt() {
        return heartRateInt;
    }

    public void setHeartRateInt(Integer heartRateInt) {
        this.heartRateInt = heartRateInt;
    }

    @Override
    public String toString() {
        return "MioFuseHeartRate{" +
                "id=" + _id +
                ", sync=" + sync +
                ", userId='" + userId + '\'' +
                ", timestampMsLong=" + timestampMs +
                ", heartRateInt=" + heartRateInt +
                '}';
    }
}