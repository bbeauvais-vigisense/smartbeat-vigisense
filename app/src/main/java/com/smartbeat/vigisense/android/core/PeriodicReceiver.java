package com.smartbeat.vigisense.android.core;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.smartbeat.vigisense.android.api.Input;

public class PeriodicReceiver extends BroadcastReceiver{

    private static final String TAG = MyService.class.getName();
    public static final int DISABLE = 0;
    public static final int DEFAULT_MS = 15 * 60 * 1000;
    public static final String PERIODIC = "periodic";

    public void onReceive(Context context, Intent intent){
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            resetAlarm(context, context.getSharedPreferences(MyContext.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getInt(Input.START_SCAN_INTERVALL_INT, DEFAULT_MS));
        }
    }

    static public void resetAlarm(Context context, int intervalMillis) {
        cancelAlarm(context);
        setAlarm(context, intervalMillis);
    }

    static public void setAlarm(Context context, int intervalMillis) {
        Log.d(TAG,"setAlarm, intervalMillis: "+intervalMillis);
        if (intervalMillis==DISABLE)
            return;
        AlarmManager alarmManager =(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent startScanIntent = new Intent(context, MyReceiver.class);
        startScanIntent.setAction(Input.START_SCAN_ACTION);
        startScanIntent.putExtra(PERIODIC,true);
        PendingIntent periodicIntent = PendingIntent.getBroadcast(context, 0, startScanIntent, 0);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + intervalMillis, intervalMillis , periodicIntent);
    }

    static public void cancelAlarm(Context context) {
        Log.d(TAG,"cancelAlarm");
        Intent intent = new Intent(context, MyReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}
