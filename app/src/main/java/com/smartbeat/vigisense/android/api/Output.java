package com.smartbeat.vigisense.android.api;

public class Output {

    /**
     * This action is sent when MyService is stopped. All connections and processes are terminated.
     */
    public static final String SERVICE_STOPPED_ACTION = "com.smartbeat.vigisense.service_stopped";

    /**
     * This action is sent when the bluetooth scan has started with success.
     */
    public static final String SCAN_STARTED_ACTION = "com.smartbeat.vigisense.scan_started";

    /**
     * This action is sent when the bluetooth scan is unable to start.
     * See ScanCallback constants to interpret error code.
     * Use ERROR_MSG_STR as key to access the error message.
     */
    public static final String SCAN_FAILED_ACTION = "com.smartbeat.vigisense.scan_failed";
    public static final String ERROR_MSG_STR = "errorMsgStr";

    /**
     * When the bluetooth scanning is started with action
     * This action is sent when the bluetooth scan is unable to start.
     * See ScanCallback constants to interpret error code.
     * Use ERROR_MSG_STR as key to access the error message.
     */
    public static final String SCAN_TIMEOUT_ACTION = "com.smartbeat.vigisense.scan_timeout";

    /**
     * An id (unique) value is associated with all measurement.
     * This id has to be sent back to validate certain measurements BODY_SCALER, BP_MONITOR
     */
    public static final String ID = "id";

    ///////////// PEDOMETER /////////////
    /**
     * This action is sent when the pedometer is connected with the device.
     * The data indicate the data count for the current day.
     *  Use as bundle key PEDOMETER_STEPS_INT, PEDOMETER_DISTANCE_METER_FLOAT, PEDOMETER_CALORIE_FLOAT
     */
    public static final String PEDOMETER_CURRENT_DAY_ACTION = "com.smartbeat.vigisense.pedometer_current_day";
    /**
     * Unit is number of steps.
     */
    public static final String PEDOMETER_STEPS_INT = "stepsInt";
    /**
     * Unit is meter.
     */
    public static final String PEDOMETER_DISTANCE_FLOAT = "distanceFloat";
    /**
     * Unit is kilo calorie.
     */
    public static final String PEDOMETER_CALORIE_FLOAT = "calorieFloat";

    /**
     * This action is sent after a scan when the device is found during a bluetooth scan.
     * Connection and data synchronisation follow automatically.
     */
    public static final String PEDOMETER_CONNECTING_ACTION = "com.smartbeat.vigisense.pedometer_connecting";

    /**
     * This action is sent when the connection with the device is lost.
     * The disconnection occurs after measurement (PEDOMETER_CURRENT_DAY_ACTION or PEDOMETER_LAST_DAY_ACTION) or
     * an error during the process (PEDOMETER_ERROR_ACTION).
     */
    public static final String PEDOMETER_DISCONNECTED_ACTION = "com.smartbeat.vigisense.pedometer_disconnected";

    /**
     * This action is sent when the device is connecting or connected to the device.
     * To interpret error code see BluetoothGatt constants.
     * Use ERROR_MSG_STR as key to access the error message.
     */
    public static final String PEDOMETER_ERROR_ACTION = "com.smartbeat.vigisense.pedometer_error";


    ///////////// OXIMETER /////////////
    /**
     * This action is sent when the device has been found. The user can start to use the device.
     */
    public static final String OXIMETER_CONNECTED_ACTION =  "com.smartbeat.vigisense.oximeter_connecting";


    ///////////// BODY_SCALER /////////////
    /**
     * This action is sent when the device is connecting or connected to the device.
     * To interpret error code see BluetoothGatt constants.
     * * Use ERROR_MSG_STR as key to access the error message.
     */
    public static final String BODY_SCALER_ERROR_ACTION = "com.smartbeat.vigisense.body_scaler_error";

    /**
     * This action is sent when the device has been found. The user can start to use the device.
     */
    public static final String BODY_SCALER_CONNECTED_ACTION =  "com.smartbeat.vigisense.body_scaler_connecting";

    /**
     * This action is sent when the connection with the device is lost.
     * The disconnection occurs after measurement (BODY_SCALER_MEASURE_ACTION) or
     * an error (BODY_SCALER_ERROR_ACTION).
     */
    public static final String BODY_SCALER_DISCONNECTED_ACTION = "com.smartbeat.vigisense.body_scaler_disconnected";

    /**
     * This action is sent when the measure is completed.
     * Use as bundle key BODY_SCALER_WEIGHT_FLOAT and BODY_SCALER_FAT_FLOAT
     */
    public static final String BODY_SCALER_MEASURE_ACTION = "com.smartbeat.vigisense.body_scaler_measure";
    /**
     * Unit is Kilogram.
     */
    public static final String BODY_SCALER_WEIGHT_FLOAT = "weightFloat";
    /**
     * Unit is percent of fat.
     */
    public static final String BODY_SCALER_FAT_FLOAT = "fatFloatt";
    /**
     * This action is sent after the connection.
     * Use as bundle key BODY_SCALER_BATTERY_INT
     */
    public static final String BODY_SCALER_BATTERY_ACTION = "com.smartbeat.vigisense.body_scaler_battery";
    /**
     * Unit is reamining percent of charge.
     */
    public static final String BODY_SCALER_BATTERY_INT = "batteryInt";

    ///////////// BP_MONITOR /////////////
    /**
     * This action is sent when the device is connected and ready to start the measure.
     */
    public static final String BP_MONITOR_CONNECTED_ACTION = "com.smartbeat.vigisense.bp_monitor_connected";

    /**
     * This action is sent when the connection with the device is lost.
     * The disconnection occurs after measurement (BP_MONITOR_MEASURE_ACTION) or
     * an error (BP_MONITOR_ERROR_ACTION).
     */
    public static final String BP_MONITOR_DISCONNECTED_ACTION = "com.smartbeat.vigisense.bp_monitor_disconnected";

    /**
     * This action is sent when the measure is completed.
     * Use as bundle key BP_MONITOR_SYSTOLE_INT, BP_MONITOR_DIASTOLE_INT and BP_MONITOR_HEART_RATE_INT
     */
    public static final String BP_MONITOR_MEASURE_ACTION = "com.smartbeat.vigisense.bp_monitor_measure";

    /**
     * Unit is millimeter of mercury (mmHg).
     */
    public static final String BP_MONITOR_SYSTOLE_INT = "systoleInt";
    /**
     * Unit is millimeter of mercury (mmHg).
     */
    public static final String BP_MONITOR_DIASTOLE_INT = "diastoleInt";
    /**
     * Unit is number of contractions of the heart per minute (bpm).
     */
    public static final String BP_MONITOR_HEART_RATE_INT = "heartRateInt";
    /**
     * This action is sent when the device is connecting or connected to the device.
     * To interpret error code see BluetoothGatt constants.
     * Use ERROR_MSG_STR as key to access the error message.
     */
    public static final String BP_MONITOR_ERROR_ACTION = "com.smartbeat.vigisense.bp_monitor_error";

    /**
     * This action is sent when a synchronisation triggered manually has been done with success.
     */
    public static final String SYNC_DONE = "com.smartbeat.vigisense.sync_done";

    ///////////// MIO FUSE /////////////
    /**
     * This action is sent when the device is connecting or connected to the device.
     * To interpret error code see BluetoothGatt constants.
     * * Use ERROR_MSG_STR as key to access the error message.
     */
    public static final String MIO_FUSE_ERROR_ACTION = "com.smartbeat.vigisense.mio_fuse_error";

    /**
     * This action is sent when the device has been found.
     */
    public static final String MIO_FUSE_CONNECTED_ACTION =  "com.smartbeat.vigisense.mio_fuse_connecting";

    /**
     * This action is sent when the connection with the device is lost.
     */
    public static final String MIO_FUSE_DISCONNECTED_ACTION = "com.smartbeat.vigisense.mio_fuse_disconnected";

    /**
     * This action is sent when a number of step for a previous day has been downloaded. Note, only the last 14 previous days are stored inside the bracelet.
     * The action will be send only once for each day. The number of step for the current day is ignored.
     * Use as bundle key MIO_FUSE_STEP_INT and MIO_FUSE_STEP_DATE
     * Note the heart rate feature has to be disabled (manual) to download data concerning steps (see Mio Fuse user guide).
     */
    public static final String MIO_FUSE_STEP_ACTION = "com.smartbeat.vigisense.mio_fuse_step";
    /**
     * Unit is number of steps.
     */
    public static final String MIO_FUSE_STEP_INT = "stepsInt";
    /**
     * The date use the format yyyy/mm/dd. eg "2016/08/02"
     */
    public static final String MIO_FUSE_STEP_DATE = "stepsDate";
    /**
     * The value is a timestamp and the unit is millisecond.
     */
    public static final String MIO_FUSE_STEP_TIMESTAMP = "timestampMs";
    /**
     * This action is sent when a the heart rate is updated. Note this feature has to bee enable/disable manually by the user (see Mio Fuse user guide).
     */
    public static final String MIO_FUSE_HEART_RATE_ACTION = "com.smartbeat.vigisense.mio_fuse_heart_rate";
    /**
     * Unit is number of contractions of the heart per minute (bpm).
     */
    public static final String MIO_FUSE_HEART_RATE_INT = "heartRateInt";
    /**
     * The value is a timestamp and the unit is millisecond.
     */
    public static final String MIO_FUSE_HEART_RATE_TIMESTAMP_MS = "timestampMs";

    /**
     * This action is sent after the connection.
     * Use as bundle key MIO_FUSE_BATTERY_INT
     */
    public static final String MIO_FUSE_BATTERY_ACTION = "com.smartbeat.vigisense.mio_fuse_battery";
    /**
     * Unit is reamining percent of charge.
     */
    public static final String MIO_FUSE_BATTERY_INT = "batteryInt";

    /**
     * This action is sent after the connection with the oximeter.
     * Use as bundle key OXIMETER_PSO2_INT
     */
    public static final String OXIMETER_ACTION = "com.smartbeat.vigisense.oximeter";
    /**
     * Unit is percent of oxygen saturation.
     */
    public static final String OXIMETER_PSO2_INT = "pso2Int";

    /**
     * This action is sent when the connection with the device is lost.
     */
    public static final String OXIMETER_DISCONNECTED_ACTION = "oximeter_disconnected";
}
