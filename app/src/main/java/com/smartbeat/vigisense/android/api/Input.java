package com.smartbeat.vigisense.android.api;

public class Input {
    /**
     * Connect the blood pressure monitor using classic bluetooth (not low energy).
     * First the device must be switch on and bonded to the device by the user using the android interface.
     * The use of the action START_SCAN_ACTION is not necessary.
     * BP_MONITOR_SCENARIO_BOOLEAN is optional. This parameter trigger a scenarios to broadcast sample data in order to perform tests.
     *
     */
    // @deprecated
    // public static final String CONNECT_BP_MONITOR_ACTION = "com.smartbeat.vigisense.connect_bp_monitor";
    /**
     * true to enable.
     */
    // @deprecated
//    public static final String BP_MONITOR_SCENARIO_BOOLEAN = "scenarioBoolean";

    /**
     * Start the blood pressure measure.
     * First the action BP_MONITOR_CONNECTED_ACTION must be received.
     * BP_MONITOR_SCENARIO_BOOLEAN is optional. This parameter trigger a scenarios to broadcast sample data in order to perform tests.
     */
    // @deprecated
//    public static final String START_BP_MONITOR_ACTION = "com.smartbeat.vigisense.start_bp_monitor";

    /**
     * Stop all connections and eventually scanning process. Use this action when interaction with devices are completed.
     */
    public static final String STOP_SERVICE_ACTION = "com.smartbeat.vigisense.stop_service";

    /**
     * Start the bluetooth scan. The user must enable : location (GPS), the bluetooth and all app authorisations.
     * When a miofuse bracelet, body scaler, oximeter is detected, the connection is automatic and new data are broadcasted.
     * use as bundle key (optional) START_SCAN_DURATION_INT, START_SCAN_USER_ID_STR, START_SCAN_SCENARIO_INT, START_SCAN_INTERVALL_INT, START_SCAN_CONNECTION_CHOICE
     */
    public static final String START_SCAN_ACTION = "com.smartbeat.vigisense.start_scan";

    /**
     * Set the device to connect. If not given try to connect to all devices except blood pressure monitor. Must be one of the enum DEVICE_TO_CONNECT
     */
    public static final String START_SCAN_CONNECTION_CHOICE = "connection_choice";

    public enum DEVICE_TO_CONNECT{
        BODY_SCALE,
        MIO_FUSE,
        OXIMETER,
        BLOOD_PRESSURE_LE
    }

    /**
     * Set the device to disconnect. Add extra DEVICE_TO_CONNECT
     */
    public static final String DISCONNECT_FROM_DEVICE = "com.smartbeat.vigisense.disconnect_device";

    /**
     * Indicate the unique identifier of the user. This string identify the user data on the web server. When the identifier is changed previous data are deleted.
     */
    public static final String START_SCAN_USER_ID_STR = "userIdStr";

    /**
     * Indicate the duration of the scanning process.When the timeout is reached, the action SCAN_TIMEOUT_ACTION is broadcasted.
     * Optional. Default value 20000.
     */
    public static final String START_SCAN_DURATION_INT = "scanDurationMs";

    /**
     * Indicate the interval of synchronisation for continuous monitoring device as pedometer.
     * Optional. Default value 20000. Set value to 0 to disable.
     */
    public static final String START_SCAN_INTERVALL_INT = "intervalInt";

    /**
     * This parameter trigger scenarios broadcasting sample data in order to perform tests. Optional.
     * See bellow the available scenarios.
     */
    public static final String START_SCAN_SCENARIO_INT ="scenarioInt";


    public static final int START_SCAN_FAKE_DATA_DISABLE = 0;
    public static final int START_SCAN_SCENARIO_PEDOMETER = 1;
    public static final int START_SCAN_SCENARIO_BODY_SCALER = 2;
    public static final int START_SCAN_SCENARIO_FAILED = 3;
    public static final int START_SCAN_SCENARIO_MIO_FUSE = 4;
    public static final int START_SCAN_SCENARIO_OXIMETER = 5;

    /**
     * Trigger data synchronisation with the sever.
     * Note synchronisation is performed when new values are inserted in the content provider and periodically during the day.
     */
    public static final String SYNC_WITH_SERVER = "com.smartbeat.vigisense.synch_with_server";

    /**
     * Validate measurement for oximeter, blood pressure monitor, body scale. The value will be pushed to the server during the next synchronisation.
     * These bundle extra keys must be filled up VALIDATE_MEASUREMENT_TABLE, VALIDATE_MEASUREMENT_ID
     * The
     */
    public static final String VALIDATE_MEASUREMENT = "com.smartbeat.vigisense.validate_measurement";
    /**
     * Type of measurement : Database.OXIMETER_TABLE, Database.BP_MONITOR_TABLE, Database.BODY_SCALE_TABLE.
     */
    public static final String VALIDATE_MEASUREMENT_TABLE = "table";

    /**
     * The id of the measure to validate.
     * Given as extra for the actions : Output.OXIMETER_ACTION, Output.BP_MONITOR_MEASURE_ACTION, Output.BODY_SCALER_MEASURE_ACTION
     * with the key Output.ID
     */
    public static final String VALIDATE_MEASUREMENT_ID = "id";


}