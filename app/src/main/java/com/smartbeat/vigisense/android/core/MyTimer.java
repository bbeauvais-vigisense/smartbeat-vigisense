package com.smartbeat.vigisense.android.core;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


public class MyTimer {

	protected boolean done;
	protected boolean canceled;
	private ScheduledFuture<?> onceFuture;
	private static ScheduledExecutorService scheduledExecutorService;

	public static void clear(){
		if (scheduledExecutorService!=null)
			scheduledExecutorService.shutdown();
		scheduledExecutorService = null;
	}
	
	public MyTimer(Runnable asynch) {
		this(0, asynch);
	}

	public MyTimer(int delay, final Runnable once) {
		if (scheduledExecutorService==null)
			 scheduledExecutorService = Executors. newScheduledThreadPool(30);
		onceFuture = scheduledExecutorService.schedule(new Runnable() {
			public void run() {
					once.run();
				done = true;
			}
		}, delay, TimeUnit.MILLISECONDS);

	}

	public boolean isDone() {
		return done;
	}


	public void cancel() {
		if (onceFuture!=null){
			onceFuture.cancel(false);
		}
		canceled = true;
	}


	public boolean isCanceled() {
		return canceled;
	}

}
