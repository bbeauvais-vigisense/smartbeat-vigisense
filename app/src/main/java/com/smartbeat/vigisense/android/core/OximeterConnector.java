package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.smartbeat.vigisense.android.model.Oximeter;

public class OximeterConnector extends MyGattCallback {

    public static final String notif_b86d = "cdeacb81-5235-4c07-8846-93a37ee6b86d"; //read notif
    private final MyContext ctx;

    private static final String TAG = OximeterConnector.class.getName();
    private final MyBluetoothGatt myGatt;
    private BluetoothDevice device;

    public OximeterConnector(MyContext myContext) {
        ctx = myContext;
        myGatt = new MyBluetoothGatt(ctx, this);
    }

    public void onScan(BluetoothDevice btDevice) {
        try {
            device = btDevice;
            myGatt.connect(device);
        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }

    public void onDestroy() {
        if (myGatt != null)
            myGatt.onDestroy();
    }

    public void onConnected() {
        Log.e(TAG, "onConnected");
        myGatt.notification(notif_b86d);
        myGatt.nextCmd();
        ctx.getLogicOutput().oximeterConnecting();
    }


    public void onCharacteristicChanged(BluetoothGattCharacteristic chr) throws Exception {
        Log.i(TAG, "onCharacteristicChanged: "+ chr.getUuid().toString());
        byte[] bytes = chr.getValue();
        if (!chr.getUuid().toString().equals(notif_b86d))
            return;
        if (bytes[0]!=-127)
            return;
        Integer spo2 = Integer.valueOf(bytes[2]);
        if (spo2>100)
            return;
        Log.i(TAG, "oximeterHandle: "+ spo2);
        Oximeter oximeter = new Oximeter();
        oximeter.setUserId(ctx.getUserId());
        oximeter.setTimestampMs(System.currentTimeMillis());
        oximeter.setSpo2(spo2);
        ctx.getLogicOutput().oximeterNewData(oximeter);
    }

    public void onDisconnected() {
        ctx.getLogicOutput().oximeterDisconnected();
    }

    public void onError(String message) {
        onDestroy();
        ctx.getLogicOutput().oximeterDisconnected();
    }
}
