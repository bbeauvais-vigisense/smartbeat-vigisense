package com.smartbeat.vigisense.android.core;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.smartbeat.vigisense.android.api.Input;


public class MyService extends Service {
    private static final String TAG = MyService.class.getName();
    private MyContext ctx;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        ctx.destroy();
        super.onDestroy();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ctx = new MyContext(this);
//        ctx.getSyncroniser().syncManual();
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        if (intent == null) {
            Log.d(TAG, "onStartCommand, intent: " + intent);
            return START_NOT_STICKY;
        }
        Log.d(TAG, "onStartCommand, startId: " + startId + (intent.hasExtra(PeriodicReceiver.PERIODIC) ? ", PERIODIC" : "") + ", flags: " + flags + ", intent: " + intent);
       if (intent.getAction().equals(Input.STOP_SERVICE_ACTION)){
            Log.i(TAG, "Stop service");
            ctx.destroy();
            stopSelf();
            return START_NOT_STICKY;
        }
        ctx.getLogicInput().handleIntent(intent);
        return START_STICKY;
    }

}