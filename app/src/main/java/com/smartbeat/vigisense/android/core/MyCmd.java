package com.smartbeat.vigisense.android.core;

import java.util.Arrays;

public class MyCmd {

    public static final int WRITE_CHAR = 0;
    public static final int NOTIF_ENABLED = 1;
    public static final int READ_CHAR = 2;
    public static final byte[] nullByte = {};
    int type;
    String uuid;
    byte[] value;

    public MyCmd(int type, String uuid, byte[] value) {
        this.type = type;
        this.uuid = uuid.toLowerCase();
        this.value = value;
    }

    public MyCmd(int type, String uuid, String value) {
        this(type,uuid,nullByte);
        this.value = hexStringToByteArray(value);
    }
    public static byte[] hexStringToByteArray(String s) {
        s = s.replace(" ","");
        s = s.replace(":","");
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    public int getType() {
        return type;
    }

    public String getUuid() {
        return uuid;
    }

    public byte[] getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "type=" + type +
                ", uuid='" + uuid + '\'' +
                ", value=" + Arrays.toString(value);
    }


}
