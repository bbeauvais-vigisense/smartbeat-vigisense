package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.util.Log;

import com.smartbeat.vigisense.android.api.Input;

import java.util.ArrayList;
import java.util.List;

public class DeviceScanner extends ScanCallback {
    private static final String TAG = DeviceScanner.class.getName();
    private static BluetoothAdapter mBluetoothAdapter;
    private static BluetoothLeScanner mLEScanner;
    private final MyContext ctx;
    private final BluetoothManager bluetoothManager;
    private boolean scanning;
    private boolean scanTimeoutFuture;
    private MyTimer timeoutFuture;
    private boolean pedometerSynchronised;
    private boolean bodyScalerSynchronised;
    private boolean mioSync;
    private boolean oximeterSync;
    private boolean bpMonitorSync;
    private boolean _37Sync;
    private Input.DEVICE_TO_CONNECT deviceToConnect;
    private BluetoothDevice reconnectDevice = null;

    public DeviceScanner(MyContext myCtx) {
        this.ctx = myCtx;
        bluetoothManager = (BluetoothManager) ctx.getCtx().getSystemService(Context.BLUETOOTH_SERVICE);
    }

    public void scanAndConnect(final int duration, Input.DEVICE_TO_CONNECT deviceToConnect) {
        this.deviceToConnect = deviceToConnect;
        try {
            if (!isBluetoothEnabled())
                return;
            if (mBluetoothAdapter == null)
                mBluetoothAdapter = bluetoothManager.getAdapter();
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            Log.i(TAG, "startScan...");
            stopScan();
            pedometerSynchronised = false;
            bodyScalerSynchronised = false;
            mioSync = false;
            oximeterSync = false;
            bpMonitorSync = false;
            ScanSettings settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            ArrayList<ScanFilter> filters = new ArrayList<ScanFilter>();
            mLEScanner.startScan(filters, settings, DeviceScanner.this);
            scanning = true;
            Log.i(TAG, "startScan...done. Stop after " + duration + "ms");

            if (timeoutFuture != null && !timeoutFuture.isCanceled())
                timeoutFuture.cancel();
            timeoutFuture = new MyTimer(duration, new Runnable() {
                public void run() {
                    if (scanning)
                        ctx.getLogicOutput().scanTimeout();
                    mLEScanner.stopScan(DeviceScanner.this);
                    scanning = false;
                }
            });
            ctx.getLogicOutput().scanStarted();
        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }

    /**
     * Reconnects to the same device if the Bluetooth adapter is unexpectedly disabled
     * Connect in case of enabling
     * @param duration
     * @param device
     */
    public void scanAndConnectByAddress(final int duration, BluetoothDevice device) {
        try {
            if (!isBluetoothEnabled())
                return;
            if (mBluetoothAdapter == null)
                mBluetoothAdapter = bluetoothManager.getAdapter();
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            Log.i(TAG, "startScan...");
            stopScan();
            ScanSettings settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            ArrayList<ScanFilter> filters = new ArrayList<>();
            filters.add(new ScanFilter.Builder().setDeviceAddress(device.getAddress()).build());
            reconnectDevice = device;
            mLEScanner.startScan(filters, settings, DeviceScanner.this);
            scanning = true;
            Log.i(TAG, "startScan...done. Stop after " + duration + "ms");

            if (timeoutFuture != null && !timeoutFuture.isCanceled())
                timeoutFuture.cancel();
            timeoutFuture = new MyTimer(duration, new Runnable() {
                public void run() {
                    if (scanning)
                        ctx.getLogicOutput().scanTimeout();
                    mLEScanner.stopScan(DeviceScanner.this);
                    scanning = false;
                }
            });
            ctx.getLogicOutput().scanStarted();
        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }

    public void onDestroy() {
        stopScan();
    }

    private void stopScan() {
        try {
            if (timeoutFuture != null && !timeoutFuture.isCanceled())
                timeoutFuture.cancel();
            if (mLEScanner != null) {
                if (mBluetoothAdapter.getState() != BluetoothAdapter.STATE_TURNING_OFF || mBluetoothAdapter.getState() != BluetoothAdapter.STATE_OFF)
                    mLEScanner.stopScan(DeviceScanner.this);
                mLEScanner.flushPendingScanResults(DeviceScanner.this);
            }
        }
        catch (Exception e){
            Log.e(TAG, "Error stopping scanner");
        }
        scanning = false;
    }

    public void onScanResult(int callbackType, ScanResult result) {
        try {
            Log.d(TAG, "deviceName: " + result.getDevice().getName() + ", device: " + deviceToConnect);
            Log.v(TAG, "callbackType: " + callbackType + ", result: " + result.toString());
            BluetoothDevice btDevice = result.getDevice();
            String name = btDevice.getName();
            if (name == null)
                return;

            //reconnection only works for MioFuse
            if(reconnectDevice != null){
                if (reconnectDevice.getAddress().equals(btDevice.getAddress())) {
                    ctx.getMioFuse().onScan(btDevice);
                    reconnectDevice = null;
                }
                return;
            }

            if (!pedometerSynchronised && name.indexOf("Braceli") > -1) {
                pedometerSynchronised = true;
                ctx.getPedometer().onScan(btDevice);
            }

            if (!bodyScalerSynchronised && name.indexOf("YUNMAI") > -1
                    && (deviceToConnect != null && deviceToConnect.equals(Input.DEVICE_TO_CONNECT.BODY_SCALE))) {
                bodyScalerSynchronised = true;
                ctx.getBodyScaler().onScan(btDevice);
            }
            if (!mioSync && name.indexOf("FUSE") > -1
                    && (deviceToConnect != null && deviceToConnect.equals(Input.DEVICE_TO_CONNECT.MIO_FUSE))) {
                mioSync = true;
                ctx.getMioFuse().onScan(btDevice);
            }
            if (!_37Sync && name.indexOf("Journey") > -1
                    && (deviceToConnect != null && deviceToConnect.equals(Input.DEVICE_TO_CONNECT.MIO_FUSE))) {
                _37Sync = true;
                ctx.get37().onScan(btDevice);
            }
            if (!oximeterSync && name.indexOf("My Oximeter") > -1
                    && (deviceToConnect != null && deviceToConnect.equals(Input.DEVICE_TO_CONNECT.OXIMETER))) {
                oximeterSync = true;
                ctx.getOximeter().onScan(btDevice);
            }
            if (!bpMonitorSync && name.indexOf("Bluetooth BP") > -1
                    && (deviceToConnect != null && deviceToConnect.equals(Input.DEVICE_TO_CONNECT.BLOOD_PRESSURE_LE))) {
                bpMonitorSync = true;
                ctx.getBpMonitorLE().onScan(btDevice);
            }

        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }

    public void onBatchScanResults(List<ScanResult> results) {
        for (ScanResult result : results)
            onScanResult(-1, result);
    }

    public void onScanFailed(int errorCode) {
        Log.e(TAG, "onScanFailed, Error Code: " + errorCode);
        onError("onScanFailed, Error Code: " + errorCode);

        //this is the error that occurs which causes the bluetooth adapter to fatally fail
        if(errorCode == SCAN_FAILED_APPLICATION_REGISTRATION_FAILED){

        }
    }


    private void onError(String message) {
        scanning = false;
        onDestroy();
        ctx.getLogicOutput().scanFailed(message);
    }

    /**
     * Returns the bluetooth adapter status
     * Because of Wiko Pulp, we are disabling and enabling the adapter everytime we perform a measurement,
     * so the onError in case bluetooth adapter is disabled is not needed.
     * @return
     */
    private boolean isBluetoothEnabled() {
        BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            onError("bluetooth not supported");
            return false;
        }
        if (mBluetoothAdapter.isEnabled())
            return true;
        else {
//            onError("bluetooth not enabled");
            scanning = false;
            onDestroy();
        }
        return false;
    }
}