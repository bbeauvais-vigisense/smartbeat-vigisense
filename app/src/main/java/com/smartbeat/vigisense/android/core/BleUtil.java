package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

public class BleUtil {

    public static String toStr(BluetoothGattDescriptor descriptor) {
        String out = "D "+descriptor.getUuid();
        out += toHexStr(descriptor.getValue());
        return out;
    }

    public static String toStr(BluetoothGattCharacteristic characteristic) {
        String out = "C "+characteristic.getUuid();
        out += val(characteristic);
        return out;
    }

    public static String val(BluetoothGattCharacteristic chr) {
        if (chr.getValue() == null)
            return "null";
        String hexStr = toHexStr(chr.getValue());
        String out = "hex: "+hexStr;

//        out += getString("ui8", chr, 1, BluetoothGattCharacteristic.FORMAT_UINT8);
//        out += getString("i8", chr, 1, BluetoothGattCharacteristic.FORMAT_SINT8);
//        out += getString("ui16", chr, 2, BluetoothGattCharacteristic.FORMAT_UINT16);
//        out += getString("si16", chr, 2, BluetoothGattCharacteristic.FORMAT_SINT16);
        out += getString("ui32", chr, 4, BluetoothGattCharacteristic.FORMAT_UINT32);
//        out += getString("si32", chr, 4, BluetoothGattCharacteristic.FORMAT_SINT32);
//        out += getString("uf8", chr, 1, BluetoothGattCharacteristic.FORMAT_FLOAT);
//        out += getString("sf8", chr, 1, BluetoothGattCharacteristic.FORMAT_SFLOAT);
//        out += getString("uf16", chr, 2, BluetoothGattCharacteristic.FORMAT_FLOAT);
//        out += getString("sf16", chr, 2, BluetoothGattCharacteristic.FORMAT_SFLOAT);
//        out += getString("uf32", chr, 4, BluetoothGattCharacteristic.FORMAT_FLOAT);
//        out += getString("sf32", chr, 4, BluetoothGattCharacteristic.FORMAT_SFLOAT);

//        out += "str: "+chr.getStringValue(0).replaceAll("\\p{C}", "");

        return out;
    }

    public static String toHexStr(byte[] value) {
        String hexStr ="";
        for (byte b : value)
            hexStr += String.format("%02x", b);
        return hexStr;
    }

    public static String getString(String title, BluetoothGattCharacteristic chr, int padding, int format) {
        String out = "| "+title+": ";
        for (int i = 0; i <= chr.getValue().length - padding; i+= padding ) {
            if (format == BluetoothGattCharacteristic.FORMAT_FLOAT || format == BluetoothGattCharacteristic.FORMAT_SFLOAT)
                out += chr.getFloatValue(format, i) + ", ";
            else
                out += chr.getIntValue(format, i) + ", ";
        }
        return out;
    }


}
