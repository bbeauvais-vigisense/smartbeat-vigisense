package com.smartbeat.vigisense.android.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {
    private static final String TAG = MyReceiver.class.getName();
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive: "+intent);
//        intent = new Intent(intent);
        context.startService(intent.setClass(context,MyService.class));
    }
}