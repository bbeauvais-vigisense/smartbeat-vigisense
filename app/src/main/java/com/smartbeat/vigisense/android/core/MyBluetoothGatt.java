package com.smartbeat.vigisense.android.core;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import static android.bluetooth.BluetoothDevice.TRANSPORT_LE;

public class MyBluetoothGatt extends BluetoothGattCallback {
    public static HashMap<String, BluetoothGattCharacteristic> chars = new HashMap<>();
    static ConcurrentLinkedQueue<MyCmd> cmds = new ConcurrentLinkedQueue<MyCmd>();
    private final MyContext ctx;
    private final MyGattCallback callback;
    private String TAG = MyBluetoothGatt.class.getName();
    private BluetoothGatt gatt;
    private boolean connected;
    private BluetoothDevice device;

    public MyBluetoothGatt(MyContext myContext, MyGattCallback callback) {
        this.ctx = myContext;
        this.callback = callback;
        TAG += "-"+callback.getClass().getSimpleName();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void connect(BluetoothDevice btDevice) {
        try {
            if (connected) {
                Log.d(TAG, "Device already connected or connecting, device: " + device);
                return;
            }
            connected = true;
            device = btDevice;
            if (gatt != null)
                gatt.disconnect();
            Log.d(TAG, "connect... device: " + device);
            gatt = device.connectGatt(ctx.getCtx(), false, this, TRANSPORT_LE);
        }catch(Exception e){
            e.printStackTrace();
            callback.onError(e.getMessage());
         }
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
     try {
        Log.d(TAG, "onCharacteristicChanged: "+ BleUtil.toStr(characteristic));
        callback.onCharacteristicChanged(characteristic);
    }catch(Exception e){
        e.printStackTrace();
        callback.onError(e.getMessage());
    }
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        try {
        Log.v(TAG, "Status: " + status);
        switch (newState) {
            case BluetoothProfile.STATE_CONNECTED:
                Log.d(TAG, "STATE_CONNECTED");
                gatt.discoverServices();
                break;
            case BluetoothProfile.STATE_DISCONNECTED:
                Log.e(TAG, "STATE_DISCONNECTED");
                callback.onDisconnected();
                connected = false;
                break;
            default:
                Log.e(TAG, "STATE_OTHER: "+newState);
                callback.onError("onConnectionStateChange, errorCode: "+newState);
        }
        }catch(Exception e){
            e.printStackTrace();
            callback.onError(e.getMessage());
        }
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        try {
        Log.d(TAG, "onDescriptorWrite: "+status+", "+BleUtil.toStr(descriptor));
        switch (status) {
            case BluetoothGatt.GATT_SUCCESS:
                Log.v(TAG, "GATT_SUCCESS");
                nextCmd();
                break;
            default:
                Log.e(TAG, "STATE_OTHER");
                callback.onError("onDescriptorWrite, errorCode: "+status);
        }
        }catch(Exception e){
            e.printStackTrace();
            callback.onError(e.getMessage());
        }
    }

    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        try {
        Log.v(TAG, "onCharacteristicWrite: "+status);
        switch (status) {
            case BluetoothGatt.GATT_SUCCESS:
                Log.v(TAG, "GATT_SUCCESS");
                nextCmd();
                break;
            default:
                Log.e(TAG, "STATE_OTHER, status: "+status+", characteristic: "+BleUtil.toStr(characteristic));
                callback.onError("onCharacteristicWrite, errorCode: "+status);
        }
        }catch(Exception e){
            e.printStackTrace();
            callback.onError(e.getMessage());
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        try {
        List<BluetoothGattService> mServices = gatt.getServices();
        Log.v(TAG, "onServicesDiscovered");
        for (BluetoothGattService service : mServices){
            for (BluetoothGattCharacteristic charact : service.getCharacteristics()) {
                chars.put(charact.getUuid().toString(), charact);
            }
        }
        Log.d(TAG, "chars: "+chars.keySet());
        cmds.clear();
        callback.onConnected();
        }catch(Exception e){
            e.printStackTrace();
            callback.onError(e.getMessage());
        }
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        try {
        Log.v(TAG, "onCharacteristicRead: "+BleUtil.toStr(characteristic));
        switch (status) {
            case BluetoothGatt.GATT_SUCCESS:
                Log.v(TAG, "GATT_SUCCESS");
                callback.onCharacteristicRead(characteristic);
                nextCmd();
                break;
            default:
                Log.e(TAG, "STATE_OTHER, status: "+status+", characteristic: "+BleUtil.toStr(characteristic));
                callback.onError("onCharacteristicRead, errorCode: "+status);
        }
        }catch(Exception e){
            e.printStackTrace();
            callback.onError(e.getMessage());
        }
    }

    public void nextCmd() {
        try {
            SystemClock.sleep(100);
            MyCmd cmd = cmds.poll();
            if (cmd == null)
                return;
            if (!connected){
                Log.d(TAG,"nextCmd not sent, connected: "+ connected +", cmd: "+cmd);
                return;
            }
            Log.v(TAG,"nextCmd cmd: "+cmd);
            if (cmd.getType()==MyCmd.WRITE_CHAR){
                BluetoothGattCharacteristic charact = chars.get(cmd.getUuid());
                charact.setValue(cmd.getValue());
                gatt.writeCharacteristic(charact);
                return;
            }
            if (cmd.getType()==MyCmd.NOTIF_ENABLED){
                String uuid = cmd.getUuid();
                gatt.setCharacteristicNotification(chars.get(uuid),true);
                BluetoothGattDescriptor desc = chars.get(uuid).getDescriptors().get(0);
                desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                gatt.writeDescriptor(desc);
                return;
            }
            if (cmd.getType()==MyCmd.READ_CHAR){
                gatt.readCharacteristic(chars.get(cmd.getUuid()));
                return;
            }
            throw new Exception("No handle for cmd: "+cmd);
        }catch(Exception e){
            e.printStackTrace();
            callback.onError(e.getMessage());
        }
    }

    public void onDestroy() {
            connected = false;
            if (gatt != null)
                gatt.disconnect();
    }


    public void notification(String uuid) {
        cmds.add(new MyCmd(MyCmd.NOTIF_ENABLED, uuid,BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE));
    }

    public void write(String uuid, String hexBytes) {
        cmds.add(new MyCmd(MyCmd.WRITE_CHAR, uuid,hexBytes));
    }
    public void write(String uuid, byte[] bytes) {
        cmds.add(new MyCmd(MyCmd.WRITE_CHAR, uuid,bytes));
    }


    public void read(String uuid) {
        cmds.add(new MyCmd(MyCmd.READ_CHAR, uuid, ""));
    }

    public boolean isConnected() {
        return connected;
    }

    public void resetCmd() {
        cmds.clear();
    }

    public void disconnect() {
        gatt.disconnect();
    }
}
