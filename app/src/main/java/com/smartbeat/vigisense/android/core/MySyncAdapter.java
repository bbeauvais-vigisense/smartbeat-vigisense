package com.smartbeat.vigisense.android.core;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.smartbeat.vigisense.android.R;
import com.smartbeat.vigisense.android.api.Database;
import com.smartbeat.vigisense.android.model.MioFuseActivity;
import com.smartbeat.vigisense.android.model.MioFuseHeartRate;
import com.smartbeat.vigisense.android.model.MyBean;
import com.smartbeat.vigisense.api.androidApi.AndroidApi;
import com.smartbeat.vigisense.api.bodyScaleApi.model.BodyScale;

import org.json.JSONArray;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//import com.smartbeat.vigisense.api.androidApi2.AndroidApi;
//import com.smartbeat.vigisense.api.partnerApi.*;

public class MySyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = MySyncAdapter.class.getName();
    private static final long HOUR = 60 * 60 * 1000;
    ContentResolver mContentResolver;
    private MyDatabase db;
    private MyContext ctx;
    private AndroidApi myApiService;
    private Long lastHeartRateSync;
    private Long lastActivitySync;

    public MySyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        init(context);
    }

    /**
     * For retro-compatibility
     */
    @SuppressWarnings("unused")
    public MySyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        init(context);
    }

    private void init(Context context) {
        Log.v(TAG, "MySyncAdapter");
        ctx = new MyContext(context);
        db = ctx.getDb();
        mContentResolver = context.getContentResolver();
        new BodyScale();
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        try {
            Log.d(TAG, "onPerformSync");
            initAppService();
            syncTables();
            long now = System.currentTimeMillis();
            syncHeartRate(now);
            //Steps
            syncActivity(now);
            ctx.getLogicOutput().syncDone();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void syncTables() {
        for (Map.Entry<MyBean, SyncCmd> entry : PlugYouTables.getTablesSync().entrySet()) {
            try {
                MyBean table = entry.getKey();
                Log.i(TAG, "sync table: " + table.getClass().getName());
                Dao<MyBean, Object> dao = ((Dao<MyBean, Object>) db.dao(table.getClass()));
                QueryBuilder<? extends MyBean, Object> queryBuilder = dao.queryBuilder();
                queryBuilder.where().eq(Database.SYNC, false);
                queryBuilder.orderBy(Database.TIMESTAMP_MS, true);
                List<? extends MyBean> items = queryBuilder.query();
                for (MyBean row : items) {
                    Log.i(TAG, "row: " + row);
                    AbstractGoogleClientRequest inserted = entry.getValue().insert(myApiService.androidEndpoint(), row);
                    Object res = inserted.execute();
                    Log.v(TAG, "sync done, status: " + inserted.getLastStatusCode() + ", res: " + res);
                    row.setSync(true);
                    dao.update(row);
                }
                Log.d(TAG, "sync done, row: " + items.size() + " for: " + table.getClass().getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void syncActivity(long now) throws java.sql.SQLException, IOException {
        if (lastActivitySync == null)
            lastActivitySync = ctx.getLong("lastActivitySync", 0);
        Log.i(TAG, "syncActivity, lastActivitySync: " + (now - lastActivitySync) / 60000 + "min");
//            if (now - lastHeartRateSync > HOUR){
        if (now - lastActivitySync > 60000) {
            lastActivitySync = now;
            ctx.getEdit().putLong("lastActivitySync", now);
            Dao<MioFuseActivity, Object> dao = ((Dao<MioFuseActivity, Object>) db.dao(MioFuseActivity.class));
            QueryBuilder<MioFuseActivity, Object> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq(Database.SYNC, false);
            queryBuilder.orderBy(Database.TIMESTAMP_MS, true);
            List<MioFuseActivity> items = queryBuilder.query();
            if (items.size() > 0) {
                Iterator<MioFuseActivity> iterator = items.iterator();
                JSONArray jsonBatch = new JSONArray();
                String userId = items.get(0).getUserId();
                for (int i = 0; i < items.size(); i++) {
                    MioFuseActivity step = iterator.next();
                    JSONArray jsonRow = new JSONArray();
                    jsonRow.put(step.getTimestampMs());
                    jsonRow.put(step.getStepsInt());
                    jsonBatch.put(jsonRow);
                    if (i % 30 == 0 || i == items.size()-1) {
                        myApiService.androidEndpoint().insertMioFuseActivities(userId, userId,jsonBatch.toString()).execute();
                        Log.d(TAG, "sync MioFuseActivity, batch: " + jsonBatch.toString());
                        jsonBatch = new JSONArray();
                    }
                    step.setSync(true);
                    dao.update(step);
                }

                Log.d(TAG, "sync MioFuseActivity, size: " + items.size());
            }

        }
    }

    private void syncHeartRate(long now) throws java.sql.SQLException, IOException {
        if (lastHeartRateSync == null)
            lastHeartRateSync = ctx.getLong("lastHeartRateSync", 0);
        Log.i(TAG, "sync MioFuseHeartRate, lastHeartRateSync: " + (now - lastHeartRateSync) / 60000 + "min");
//            if (now - lastHeartRateSync > HOUR){
        if (now - lastHeartRateSync > 60000) {
            lastHeartRateSync = now;
            ctx.getEdit().putLong("lastHeartRateSync", now);
            Dao<MioFuseHeartRate, Object> dao = ((Dao<MioFuseHeartRate, Object>) db.dao(MioFuseHeartRate.class));
            QueryBuilder<MioFuseHeartRate, Object> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq(Database.SYNC, false);
            queryBuilder.orderBy(Database.TIMESTAMP_MS, true);
            List<MioFuseHeartRate> items = queryBuilder.query();
            if (items.size() > 0) {
                Iterator<MioFuseHeartRate> iterator = items.iterator();
                JSONArray jsonBatch = new JSONArray();
                String userId = items.get(0).getUserId();
                for (int i = 0; i < items.size(); i++) {
                    MioFuseHeartRate hr = iterator.next();
                    JSONArray jsonRow = new JSONArray();
                    jsonRow.put(hr.getTimestampMs());
                    jsonRow.put(hr.getHeartRateInt());
                    jsonBatch.put(jsonRow);
                    if (i % 30 == 0 || i == items.size()-1) {
                        myApiService.androidEndpoint().insertMioFuseHeartRates(userId, jsonBatch.toString()).execute();
                        Log.d(TAG, "sync MioFuseHeartRate, batch: " + jsonBatch.toString());
                        jsonBatch = new JSONArray();
                    }
                    hr.setSync(true);
                    dao.update(hr);
                }
                Log.d(TAG, "sync MioFuseHeartRate, size: " + items.size());
            }

        }
    }

    private void initAppService() {
        if (myApiService == null) {
            String rootUrl = "https://" + ctx.getCtx().getString(R.string.server_name) + ".appspot.com/_ah/api/";
//            String rootUrl = "http://127.0.0.1:8080/_ah/api/";
            Log.v(TAG, "initAppService, url: " + rootUrl);
            AndroidApi.Builder builder = new AndroidApi.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    .setApplicationName(ctx.getCtx().getString(R.string.server_name))
                    .setRootUrl(rootUrl)
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                        @Override
                        public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                            abstractGoogleClientRequest.setDisableGZipContent(true);
                        }
                    });
            myApiService = builder.build();
        }
    }

    public interface SyncCmd {
        AbstractGoogleClientRequest insert(AndroidApi.AndroidEndpoint myApiService, MyBean row) throws Exception;
    }

}
