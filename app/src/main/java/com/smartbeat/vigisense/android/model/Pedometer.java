package com.smartbeat.vigisense.android.model;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

@AdditionalAnnotation.Contract()
@DatabaseTable(tableName = "Pedometer")
@AdditionalAnnotation.DefaultContentUri(authority = "com.smartbeat.provider.Vigisense", path = "Pedometer")
@AdditionalAnnotation.DefaultContentMimeTypeVnd(name = "com.smartbeat.provider.Vigisense", type = "Pedometer")
public class Pedometer extends MyBean {
    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    @AdditionalAnnotation.DefaultSortOrder
    Long _id;
    @DatabaseField(defaultValue = "false")
    Boolean sync;
    @DatabaseField(canBeNull = false)
    String userId;
    @DatabaseField(canBeNull = false)
    Long timestampMs;
    @DatabaseField(canBeNull = false)
    Integer stepsInt;
    @DatabaseField(canBeNull = false)
    Float distanceMeterFloat;
    @DatabaseField(canBeNull = false)
    Float calorieFloat;

    public Pedometer() {
    }

    @Override
    public Long get_id() {
        return _id;
    }

    @Override
    public void set_id(Long _id) {
        this._id = _id;
    }

    @Override
    public Boolean getSync() {
        return sync;
    }

    @Override
    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getStepsInt() {
        return stepsInt;
    }

    public void setStepsInt(Integer stepsInt) {
        this.stepsInt = stepsInt;
    }

    public Float getDistanceMeterFloat() {
        return distanceMeterFloat;
    }

    public void setDistanceMeterFloat(Float distanceMeterFloat) {
        this.distanceMeterFloat = distanceMeterFloat;
    }

    public Float getCalorieFloat() {
        return calorieFloat;
    }

    public void setCalorieFloat(Float calorieFloat) {
        this.calorieFloat = calorieFloat;
    }

    @Override
    public String toString() {
        return "Pedometer{" +
                "_id=" + _id +
                ", sync=" + sync +
                ", userId='" + userId + '\'' +
                ", timestampMsLong=" + timestampMs +
                ", stepsInt=" + stepsInt +
                ", distanceMeterFloat=" + distanceMeterFloat +
                ", calorieFloat=" + calorieFloat +
                '}';
    }
}
