package com.smartbeat.vigisense.android.model;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

@AdditionalAnnotation.Contract()
@DatabaseTable(tableName = "MioFuseHeartRateDeltaDay")
@AdditionalAnnotation.DefaultContentUri(authority = "com.smartbeat.provider.Vigisense", path = "MioFuseHeartRateDeltaDay")
@AdditionalAnnotation.DefaultContentMimeTypeVnd(name = "com.smartbeat.provider.Vigisense", type = "MioFuseHeartRateDeltaDay")
public class MioFuseHeartRateDeltaDay extends MyBean {

    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    @AdditionalAnnotation.DefaultSortOrder
    Long _id;
    @DatabaseField(defaultValue = "false")
    Boolean sync;
    @DatabaseField(canBeNull = false)
    String userId;
    @DatabaseField(canBeNull = false)
    Long timestampMs;

    @DatabaseField(canBeNull = false)
    Integer median;

    @DatabaseField(canBeNull = false)
    Integer mean;

    @DatabaseField(canBeNull = false)
    Integer standardDeviation;

    @DatabaseField(canBeNull = false)
    Integer min;

    @DatabaseField(canBeNull = false)
    Integer firstDecile;

    @DatabaseField(canBeNull = false)
    Integer lastDecile;

    @DatabaseField(canBeNull = false)
    Integer max;

    @DatabaseField(canBeNull = false)
    Integer firstQuartile;

    @DatabaseField(canBeNull = false)
    Integer lastQuartile;

    @DatabaseField(canBeNull = false)
    Long sampleSize;

    public Integer getFirstQuartile() {
        return firstQuartile;
    }

    public void setFirstQuartile(Integer firstQuartile) {
        this.firstQuartile = firstQuartile;
    }

    public Integer getLastQuartile() {
        return lastQuartile;
    }

    public void setLastQuartile(Integer lastQuartile) {
        this.lastQuartile = lastQuartile;
    }

    public Long getSampleSize() {
        return sampleSize;
    }

    public void setSampleSize(Long sampleSize) {
        this.sampleSize = sampleSize;
    }

    public MioFuseHeartRateDeltaDay() {
    }

    @Override
    public Long get_id() {
        return _id;
    }

    @Override
    public void set_id(Long _id) {
        this._id = _id;
    }

    @Override
    public Boolean getSync() {
        return sync;
    }

    @Override
    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getMedian() {
        return median;
    }

    public void setMedian(Integer median) {
        this.median = median;
    }

    public Integer getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(Integer standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getFirstDecile() {
        return firstDecile;
    }

    public void setFirstDecile(Integer firstDecile) {
        this.firstDecile = firstDecile;
    }

    public Integer getLastDecile() {
        return lastDecile;
    }

    public void setLastDecile(Integer lastDecile) {
        this.lastDecile = lastDecile;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getMean() {
        return mean;
    }

    public void setMean(Integer mean) {
        this.mean = mean;
    }

    @Override
    public String toString() {
        return "MioFuseHeartRateDeltaDay{" +
                "_id=" + _id +
                ", sync=" + sync +
                ", userId='" + userId + '\'' +
                ", timestampMs=" + timestampMs +
                ", median=" + median +
                ", mean=" + mean +
                ", standardDeviation=" + standardDeviation +
                ", min=" + min +
                ", firstDecile=" + firstDecile +
                ", lastDecile=" + lastDecile +
                ", max=" + max +
                ", firstQuartile=" + firstQuartile +
                ", lastQuartile=" + lastQuartile +
                ", sampleSize=" + sampleSize +
                '}';
    }
}