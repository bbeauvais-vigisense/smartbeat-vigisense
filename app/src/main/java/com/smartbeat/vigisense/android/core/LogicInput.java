package com.smartbeat.vigisense.android.core;

import android.accounts.Account;
import android.content.Intent;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.smartbeat.vigisense.android.api.Database;
import com.smartbeat.vigisense.android.api.Input;
import com.smartbeat.vigisense.android.model.BodyScale;
import com.smartbeat.vigisense.android.model.BpMonitor;
import com.smartbeat.vigisense.android.model.Oximeter;

public class LogicInput {
    private final MyContext ctx;

    private static final String TAG = LogicInput.class.getName();
    private Account account;

    public LogicInput(MyContext myContext) {
        ctx = myContext;
    }

    public void handleIntent(final Intent intent) {
        new MyTimer(new Runnable() {
            public void run() {
                try {
                    switch (intent.getAction()) {
                        case Input.START_SCAN_ACTION :
                            startScan(intent);
                            break;
//                        case Input.CONNECT_BP_MONITOR_ACTION:
//                            connectBpMonitor(intent);
//                            break;
//                        case Input.START_BP_MONITOR_ACTION:
//                            startBpMonitor(intent);
//                            break;
                        case Input.SYNC_WITH_SERVER:
                            sync();
                            break;
                        case Input.VALIDATE_MEASUREMENT:
                            validate(intent);
                            break;
                        case Input.DISCONNECT_FROM_DEVICE:
                            disconnect(intent);
                        default:
                            Log.w(TAG, "intent not handled: " + intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void validate(Intent intent) throws Exception {
        if (!intent.hasExtra(Input.VALIDATE_MEASUREMENT_ID) || !intent.hasExtra(Input.VALIDATE_MEASUREMENT_TABLE))
            throw new Exception ("Missing extra Input.VALIDATE_MEASUREMENT_ID & Input.VALIDATE_MEASUREMENT_TABLE from intent : "+intent);
        long rowId = intent.getLongExtra(Input.VALIDATE_MEASUREMENT_ID, 0);
        String table = intent.getStringExtra(Input.VALIDATE_MEASUREMENT_TABLE);

        if (Database.BODY_SCALE_TABLE.equals(table)) {
            Dao<BodyScale, Long> dao = DaoManager.createDao(ctx.getDb().getConnectionSource(), BodyScale.class);
            BodyScale row = dao.queryForId(rowId);
            row.setSync(false); // so it will be push next sync
            dao.update(row);
        }
        if (Database.OXIMETER_TABLE.equals(table)){
            Dao<Oximeter, Long> dao = DaoManager.createDao(ctx.getDb().getConnectionSource(), Oximeter.class);
            Oximeter row = dao.queryForId(rowId);
            row.setSync(false); // so it will be push next sync
            dao.update(row);
        }
        if (Database.BP_MONITOR_TABLE.equals(table)){
            Dao<BpMonitor, Long> dao = DaoManager.createDao(ctx.getDb().getConnectionSource(), BpMonitor.class);
            BpMonitor row = dao.queryForId(rowId);
            row.setSync(false); // so it will be push next sync
            dao.update(row);
        }
    }


    private void sync() {
        Log.i(TAG, "Synchronisation with server");
        ctx.getSyncroniser().syncManual();
    }

//    private void startBpMonitor(Intent intent) throws Exception {
//        Log.e(TAG, "case Input.START_BP_MONITOR_ACTION:, " + intent.getAction());
//        boolean useScenario2 = intent.getBooleanExtra(Input.BP_MONITOR_SCENARIO_BOOLEAN, false);
//        if (useScenario2)
//            ctx.getScenario().runBp(intent.getAction());
//        else
//            ctx.getBpMonitorConnector().startMeasure();
//    }
//
//    private void connectBpMonitor(Intent intent) throws Exception {
//        boolean useScenario = intent.getBooleanExtra(Input.BP_MONITOR_SCENARIO_BOOLEAN, false);
//        if (useScenario)
//            ctx.getScenario().runBp(intent.getAction());
//        else
//            ctx.getBpMonitorConnector().connect();
//    }

    private void startScan(Intent intent) throws Exception {
        String userId = ctx.getUserId();
//        String userId = ctx.getString(Input.START_SCAN_USER_ID_STR, "defaultUserId");
        if (intent.hasExtra(Input.START_SCAN_USER_ID_STR)) {
            String newUserId = intent.getStringExtra(Input.START_SCAN_USER_ID_STR);
            if (!userId.equals(newUserId)) {
                Log.i(TAG, "new userId: " + newUserId);
                ctx.getEdit().putString(Input.START_SCAN_USER_ID_STR, newUserId).commit();
                ctx.getDb().reset();
                userId = newUserId;
            }
        }
        int scanDuration = intent.getIntExtra(Input.START_SCAN_DURATION_INT, 20 * 1000);
        int fakeScenario = intent.getIntExtra(Input.START_SCAN_SCENARIO_INT, 0);
        if (intent.hasExtra(Input.START_SCAN_INTERVALL_INT)) {
            int periodic = intent.getIntExtra(Input.START_SCAN_INTERVALL_INT, PeriodicReceiver.DEFAULT_MS);
            ctx.getEdit().putInt(Input.START_SCAN_INTERVALL_INT, periodic).commit();
        }
        PeriodicReceiver.resetAlarm(ctx.getCtx(), ctx.getInt(Input.START_SCAN_INTERVALL_INT, PeriodicReceiver.DEFAULT_MS));
        Input.DEVICE_TO_CONNECT device = null;
        if (intent.hasExtra(Input.START_SCAN_CONNECTION_CHOICE))
            device = (Input.DEVICE_TO_CONNECT )intent.getSerializableExtra(Input.START_SCAN_CONNECTION_CHOICE);

        Log.i(TAG, "startScan, userId: " + userId + ", scanDuration: " + scanDuration + ", fakeScenario: " + fakeScenario+", device: "+device);

        if (fakeScenario == Input.START_SCAN_FAKE_DATA_DISABLE)
            ctx.getScanner().scanAndConnect(scanDuration, device);
        else
            ctx.getScenario().run(fakeScenario);
    }

    /**
     * Disconnects from each device individually
     * @param intent
     * @throws Exception
     */
    private void disconnect(Intent intent) throws Exception {
        Input.DEVICE_TO_CONNECT device = null;
        if (intent.hasExtra(Input.START_SCAN_CONNECTION_CHOICE))
            device = (Input.DEVICE_TO_CONNECT )intent.getSerializableExtra(Input.START_SCAN_CONNECTION_CHOICE);

        if (device == null)
            return;

        switch (device){
            case BLOOD_PRESSURE_LE:
                ctx.getBpMonitorLE().onDestroy();
                break;
            case BODY_SCALE:
                ctx.getBodyScaler().onDestroy();
                break;
            case OXIMETER:
                ctx.getOximeter().onDestroy();
                break;
            case MIO_FUSE:
                ctx.getMioFuse().onDestroy();
                break;
            default:
                break;
        }
    }

}