package com.smartbeat.vigisense.android.example;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

import com.smartbeat.vigisense.android.api.Output;

public class OutputReceiver {
    private static final String TAG = OutputReceiver.class.getName();
    private final InputActivity ctx;
    private BroadcastReceiver receiver;
    private int lastHr;
    private int lastPso2;

    public OutputReceiver(InputActivity output) {
        ctx = output;
    }

    public OutputReceiver init() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Output.SERVICE_STOPPED_ACTION);
        filter.addAction(Output.SCAN_FAILED_ACTION);
        filter.addAction(Output.SCAN_STARTED_ACTION);
        filter.addAction(Output.SCAN_TIMEOUT_ACTION);
        filter.addAction(Output.PEDOMETER_CONNECTING_ACTION);
        filter.addAction(Output.PEDOMETER_ERROR_ACTION);
        filter.addAction(Output.PEDOMETER_CURRENT_DAY_ACTION);
        filter.addAction(Output.PEDOMETER_DISCONNECTED_ACTION);
        filter.addAction(Output.BODY_SCALER_BATTERY_ACTION);
        filter.addAction(Output.BODY_SCALER_CONNECTED_ACTION);
        filter.addAction(Output.BODY_SCALER_DISCONNECTED_ACTION);
        filter.addAction(Output.BODY_SCALER_MEASURE_ACTION);
        filter.addAction(Output.BODY_SCALER_ERROR_ACTION);
        filter.addAction(Output.BP_MONITOR_ERROR_ACTION);
        filter.addAction(Output.BP_MONITOR_CONNECTED_ACTION);
        filter.addAction(Output.BP_MONITOR_DISCONNECTED_ACTION);
        filter.addAction(Output.BP_MONITOR_MEASURE_ACTION);
        filter.addAction(Output.OXIMETER_CONNECTED_ACTION);
        filter.addAction(Output.OXIMETER_DISCONNECTED_ACTION);
        filter.addAction(Output.OXIMETER_ACTION);

        filter.addAction(Output.SYNC_DONE);

        filter.addAction(Output.MIO_FUSE_ERROR_ACTION);
        filter.addAction(Output.MIO_FUSE_CONNECTED_ACTION);
        filter.addAction(Output.MIO_FUSE_DISCONNECTED_ACTION);
        filter.addAction(Output.MIO_FUSE_STEP_ACTION);
        filter.addAction(Output.MIO_FUSE_HEART_RATE_ACTION);
        filter.addAction(Output.MIO_FUSE_BATTERY_ACTION);

        receiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG,"onReceive: "+intent);
                String action = intent.getAction();
                switch (action){
                    case Output.SCAN_STARTED_ACTION:
                    case Output.SCAN_TIMEOUT_ACTION:
                    case Output.PEDOMETER_CONNECTING_ACTION:
                    case Output.PEDOMETER_DISCONNECTED_ACTION:
                    case Output.BODY_SCALER_CONNECTED_ACTION:
                    case Output.BODY_SCALER_DISCONNECTED_ACTION:
                    case Output.BP_MONITOR_CONNECTED_ACTION:
                    case Output.BP_MONITOR_DISCONNECTED_ACTION:
                    case Output.SERVICE_STOPPED_ACTION:
                    case Output.SYNC_DONE:
                    case Output.MIO_FUSE_CONNECTED_ACTION:
                    case Output.MIO_FUSE_DISCONNECTED_ACTION:
                    case Output.OXIMETER_CONNECTED_ACTION:
                    case Output.OXIMETER_DISCONNECTED_ACTION:
                        toast("Action: "+action);
                        break;
                    case Output.PEDOMETER_ERROR_ACTION:
                    case Output.BODY_SCALER_ERROR_ACTION:
                    case Output.BP_MONITOR_ERROR_ACTION:
                    case Output.SCAN_FAILED_ACTION:
                    case Output.MIO_FUSE_ERROR_ACTION:
                        toast("Error: "+action+", message: "+intent.getStringExtra(Output.ERROR_MSG_STR));
                        break;
                    case Output.PEDOMETER_CURRENT_DAY_ACTION:
                        int steps = intent.getIntExtra(Output.PEDOMETER_STEPS_INT, 0);
                        float distance = intent.getFloatExtra(Output.PEDOMETER_DISTANCE_FLOAT, 0);
                        float calorie = intent.getFloatExtra(Output.PEDOMETER_CALORIE_FLOAT, 0);
                        toast("PedometerConnector: "+action+", steps: "+steps+", distance: "+distance+"m, calorie: "+calorie);
                        break;
                    case Output.BODY_SCALER_BATTERY_ACTION:
                        int batteryLevel = intent.getIntExtra(Output.BODY_SCALER_BATTERY_INT, 0);
                        toast("Body Scaler: "+action+", battery: "+batteryLevel+"%");
                        break;
                    case Output.BODY_SCALER_MEASURE_ACTION:
                        float weight = intent.getFloatExtra(Output.BODY_SCALER_WEIGHT_FLOAT, 0);
                        float fat = intent.getFloatExtra(Output.BODY_SCALER_FAT_FLOAT, 0);
                        toast("Body Scaler: "+action+", weight: "+weight+"kg, fat: "+fat+"%");
                        break;
                    case Output.BP_MONITOR_MEASURE_ACTION:
                        int systol = intent.getIntExtra(Output.BP_MONITOR_SYSTOLE_INT, 0);
                        int diastol = intent.getIntExtra(Output.BP_MONITOR_DIASTOLE_INT, 0);
                        int heartRate = intent.getIntExtra(Output.BP_MONITOR_HEART_RATE_INT, 0);
                        toast("Body Scaler: "+action+", systol: "+systol+"mmHg, diastol: "+diastol+"mmHg, heart rate: "+heartRate+"bpm");
                        break;
                    case Output.MIO_FUSE_BATTERY_ACTION:
                        int batteryLevelMio = intent.getIntExtra(Output.MIO_FUSE_BATTERY_INT, 0);
                        toast("Mio Fuse: "+action+", battery: "+batteryLevelMio+"%");
                        break;
                    case Output.MIO_FUSE_STEP_ACTION:
                        int stepsMio = intent.getIntExtra(Output.MIO_FUSE_STEP_INT, 0);
                        String date = intent.getStringExtra(Output.MIO_FUSE_STEP_DATE);
                        toast("Mio Fuse: "+action+", steps: "+stepsMio+", date: "+date);
                        break;
                    case Output.MIO_FUSE_HEART_RATE_ACTION:
                        int hr = intent.getIntExtra(Output.MIO_FUSE_HEART_RATE_INT, 0);
                        if (hr!=lastHr)
                            toast(""+hr+"bpm",Toast.LENGTH_SHORT);
                        lastHr = hr;
                        break;
                    case Output.OXIMETER_ACTION:
                        int pso2 = intent.getIntExtra(Output.OXIMETER_PSO2_INT, 0);
                        if (pso2!=lastPso2)
                            toast(""+pso2+"%",Toast.LENGTH_SHORT);
                        lastPso2 = pso2;
                        break;
                    default:
                        toast("Not handled action: "+intent);
                }

            }
        };
        ctx.registerReceiver(receiver, filter);
        return this;
    }

    private void toast(final String str) {
        toast(str, Toast.LENGTH_LONG);
    }
    private void toast(final String str,final int duration) {
        Log.i(TAG,"toast: "+str);
        ctx.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(ctx, str, duration).show();
            }
        });
    }

    protected void onDestroy() {
        if (receiver != null) {
            ctx.unregisterReceiver(receiver);
            receiver = null;
        }
    }
}
