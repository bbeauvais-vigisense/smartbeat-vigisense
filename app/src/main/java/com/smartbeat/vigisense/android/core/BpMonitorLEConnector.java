package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.smartbeat.vigisense.android.model.BpMonitor;

public class BpMonitorLEConnector extends MyGattCallback {
    public static final String write_ffe9 = "0000fff2-0000-1000-8000-00805f9b34fb"; //start ?
    public static final String notif_fff1 = "0000fff1-0000-1000-8000-00805f9b34fb"; //data flux
    public static final byte[] startCmd =  new byte[]{(byte) -3, (byte) -3, (byte) -6, (byte) 5, (byte) 13, (byte) 10};
    private final MyContext ctx;

    private static final String TAG = BpMonitorLEConnector.class.getName();
    private final MyBluetoothGatt myGatt;
    private BluetoothDevice device;
    private boolean measureDone;

    public BpMonitorLEConnector(MyContext myContext) {
        ctx = myContext;
        myGatt = new MyBluetoothGatt(ctx, this);
    }

    public void onScan(BluetoothDevice btDevice) {
        try {
            device = btDevice;
            myGatt.connect(device);
        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }

    public void onDestroy() {
        if (myGatt != null)
            myGatt.onDestroy();
    }

    public void onConnected() {
        Log.e(TAG, "onConnected");
        measureDone = false;
        myGatt.write(write_ffe9, startCmd);
        myGatt.notification(notif_fff1);
        myGatt.nextCmd();
        ctx.getLogicOutput().bpMonitorConnected();
    }


    public void onCharacteristicChanged(BluetoothGattCharacteristic chr) throws Exception {
        Log.i(TAG, "onCharacteristicChanged: "+ chr.getUuid().toString());
        try {

            if (!chr.getUuid().toString().equals(notif_fff1)) {
                return;
            }
            Integer type = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
            if (type==null)
                return;
            if (type==251 || type==6) {
                return;
            }
            // FIXME  - this happens too many often but the measurement continues normally sometimes. I remove the broadcast so I don't show an error message when the measurement continues.
            if (type!=252){
//                myGatt.disconnect();
//                ctx.getLogicOutput().bpMonitorOnError("error: "+chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3));
                return;
            }
            final int systol = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            final int diastol = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 4);
            final int heartRate = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 5);
            if (measureDone)
                return;
            measureDone = true;

            Log.i(TAG, "systol: " + systol + ", diastol: " + diastol + ", heartRate: " + heartRate);
            BpMonitor bpMonitor = new BpMonitor();
            bpMonitor.setUserId(ctx.getUserId());
            bpMonitor.setTimestampMs(System.currentTimeMillis());
            bpMonitor.setSystoleInt(systol);
            bpMonitor.setDiastoleInt(diastol);
            bpMonitor.setHeartRateInt(heartRate);
            ctx.getLogicOutput().bpMonitorNewData(bpMonitor);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void onDisconnected() {
        ctx.getLogicOutput().bpMonitorDisconnected();
    }

    public void onError(String message) {
        onDestroy();
        ctx.getLogicOutput().bpMonitorOnError(message);
    }

}