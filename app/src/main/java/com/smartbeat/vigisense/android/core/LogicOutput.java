package com.smartbeat.vigisense.android.core;

import android.content.Intent;
import android.util.Log;

import com.smartbeat.vigisense.android.api.Output;
import com.smartbeat.vigisense.android.model.BodyScale;
import com.smartbeat.vigisense.android.model.BpMonitor;
import com.smartbeat.vigisense.android.model.MioFuseActivity;
import com.smartbeat.vigisense.android.model.MioFuseBattery;
import com.smartbeat.vigisense.android.model.MioFuseDailyActivity;
import com.smartbeat.vigisense.android.model.MioFuseHeartRate;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDay;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDelta;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDeltaDay;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDeltaHour;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateHour;
import com.smartbeat.vigisense.android.model.Oximeter;
import com.smartbeat.vigisense.android.model.Pedometer;

import java.sql.SQLException;

public class LogicOutput {
    private static final String TAG = LogicOutput.class.getName();
    private final MyContext ctx;

    public LogicOutput(MyContext myContext) {
        ctx = myContext;
    }

    public void scanTimeout() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.SCAN_TIMEOUT_ACTION));
    }

    public void scanStarted() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.SCAN_STARTED_ACTION));
    }

    public void scanFailed(String message) {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.SCAN_FAILED_ACTION).putExtra(Output.ERROR_MSG_STR,message));
    }

    public void onDestroy() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.SERVICE_STOPPED_ACTION));
    }

    public void pedometerOnError(String message) {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.PEDOMETER_ERROR_ACTION).putExtra(Output.ERROR_MSG_STR,message));
    }

    public void pedometerDisconnected() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.PEDOMETER_DISCONNECTED_ACTION));
    }

    public void pedometerConnecting() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.PEDOMETER_CONNECTING_ACTION));
    }

//    public void podometerCurrentDayData(int steps, float distance, float calorie) {
    public void podometerCurrentDayData(Pedometer p) throws Exception {
        ctx.getDb().create(p);
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.PEDOMETER_CURRENT_DAY_ACTION)
                .putExtra(Output.ID,p.get_id())
                .putExtra(Output.PEDOMETER_STEPS_INT,p.getStepsInt())
                .putExtra(Output.PEDOMETER_CALORIE_FLOAT,p.getCalorieFloat())
                .putExtra(Output.PEDOMETER_DISTANCE_FLOAT,p.getDistanceMeterFloat()));
    }

    public void oximeterConnecting() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.OXIMETER_CONNECTED_ACTION));
    }

    void oximeterDisconnected() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.OXIMETER_DISCONNECTED_ACTION));
    }

    public Oximeter oximeterNewData(Oximeter oximeter) throws SQLException {
        ctx.getDb().create(oximeter);
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.OXIMETER_ACTION)
                .putExtra(Output.ID,oximeter.get_id())
                .putExtra(Output.OXIMETER_PSO2_INT,oximeter.getSpo2()));
        return oximeter;
    }


    public void bodyScalerConnecting() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.BODY_SCALER_CONNECTED_ACTION));
    }


    public void bodyScalerDisconnected() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.BODY_SCALER_DISCONNECTED_ACTION));
    }
    public BodyScale bodyScalerNewData(BodyScale bs) throws Exception {
        ctx.getDb().create(bs);
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.BODY_SCALER_MEASURE_ACTION)
                .putExtra(Output.ID,bs.get_id())
                .putExtra(Output.BODY_SCALER_WEIGHT_FLOAT,bs.getWeightKgFloat())
                .putExtra(Output.BODY_SCALER_FAT_FLOAT,bs.getFatPercentFloat()));
        return bs;
    }

    public void bodyScalerBatteryCharge(BodyScale bs) {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.BODY_SCALER_BATTERY_ACTION)
                .putExtra(Output.BODY_SCALER_BATTERY_INT,bs.getBatteryPercentInt()));
    }
    public void bodyScalerOnError(String message) {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.BODY_SCALER_ERROR_ACTION).putExtra(Output.ERROR_MSG_STR,message));
    }

    public void bpMonitorOnError(String message) {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.BP_MONITOR_ERROR_ACTION).putExtra(Output.ERROR_MSG_STR,message));
    }

    public void bpMonitorConnected() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.BP_MONITOR_CONNECTED_ACTION)); //no
    }

    public void bpMonitorDisconnected() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.BP_MONITOR_DISCONNECTED_ACTION));
    }

    public BpMonitor bpMonitorNewData(BpMonitor bp) throws Exception {
        ctx.getDb().create(bp);
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.BP_MONITOR_MEASURE_ACTION)
                .putExtra(Output.ID,bp.get_id())
                .putExtra(Output.BP_MONITOR_SYSTOLE_INT,bp.getSystoleInt())
                .putExtra(Output.BP_MONITOR_DIASTOLE_INT,bp.getDiastoleInt())
                .putExtra(Output.BP_MONITOR_HEART_RATE_INT,bp.getHeartRateInt()));
        return bp;
    }

    public void syncDone() {
        String userId = ctx.getUserId();
        Log.d(TAG, "sync done");
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.SYNC_DONE));
    }


    public void mioFuseDisconnected() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.MIO_FUSE_DISCONNECTED_ACTION));
    }
    public void mioFuseDailyActivity(MioFuseDailyActivity activity) throws Exception {
        ctx.getDb().create(activity);
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.MIO_FUSE_STEP_ACTION)
                .putExtra(Output.ID,activity.get_id())
                .putExtra(Output.MIO_FUSE_STEP_INT,activity.getStepsInt())
                .putExtra(Output.MIO_FUSE_STEP_TIMESTAMP,activity.getTimestampMs())
                .putExtra(Output.MIO_FUSE_STEP_DATE,activity.getDateStr()));
    }


    public void mioFuseActivity(MioFuseActivity activity) throws Exception {
        ctx.getDb().create(activity);
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.MIO_FUSE_STEP_ACTION)
                .putExtra(Output.ID,activity.get_id())
                .putExtra(Output.MIO_FUSE_STEP_INT,activity.getStepsInt())
                .putExtra(Output.MIO_FUSE_STEP_TIMESTAMP,activity.getTimestampMs()));

    }

    public void mioFuseHeartRateNoSave(MioFuseHeartRate hr) throws Exception {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.MIO_FUSE_HEART_RATE_ACTION)
                .putExtra(Output.MIO_FUSE_HEART_RATE_INT,hr.getHeartRateInt())
                .putExtra(Output.MIO_FUSE_HEART_RATE_TIMESTAMP_MS,hr.getTimestampMs()));
    }
    public void mioFuseHeartRate(MioFuseHeartRate hr) throws Exception {
        ctx.getDb().create(hr);
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.MIO_FUSE_HEART_RATE_ACTION)
                .putExtra(Output.ID,hr.get_id())
                .putExtra(Output.MIO_FUSE_HEART_RATE_INT,hr.getHeartRateInt())
                .putExtra(Output.MIO_FUSE_HEART_RATE_TIMESTAMP_MS,hr.getTimestampMs()));
    }
    public void mioFuseHeartRateMem(MioFuseHeartRate hr) throws Exception {
        ctx.getDb().create(hr);
    }
    public void mioFuseHeartRateBroadcast(MioFuseHeartRate hr) throws Exception {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.MIO_FUSE_HEART_RATE_ACTION)
                .putExtra(Output.ID,hr.get_id())
                .putExtra(Output.MIO_FUSE_HEART_RATE_INT,hr.getHeartRateInt())
                .putExtra(Output.MIO_FUSE_HEART_RATE_TIMESTAMP_MS,hr.getTimestampMs()));
    }

    public void mioFuseHeartRateDelta(MioFuseHeartRateDelta mioFuseHeartRateDelta) throws Exception {
        ctx.getDb().create(mioFuseHeartRateDelta);
    }

    public void mioFuseHeartRateHour(MioFuseHeartRateHour hour) throws SQLException {
        ctx.getDb().create(hour);
    }
    public void mioFuseHeartRateDeltaHour(MioFuseHeartRateDeltaHour hour) throws SQLException {
        ctx.getDb().create(hour);
    }

    public void mioFuseHeartRateDay(MioFuseHeartRateDay day) throws SQLException {
        ctx.getDb().create(day);
    }
    public void mioFuseHeartRateDeltaDay(MioFuseHeartRateDeltaDay day) throws SQLException {
        ctx.getDb().create(day);
    }

    public void mioFuseBatteryCharge(MioFuseBattery battery) throws Exception {
        ctx.getDb().create(battery);
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.MIO_FUSE_BATTERY_ACTION)
                .putExtra(Output.ID,battery.get_id())
                .putExtra(Output.MIO_FUSE_BATTERY_INT,battery.getBatteryPercentInt()));
    }
    public void mioFuseOnError(String message) {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.MIO_FUSE_ERROR_ACTION).putExtra(Output.ERROR_MSG_STR,message));
    }

    public void mioFuseConnecting() {
        ctx.getCtx().sendBroadcast(new Intent().setAction(Output.MIO_FUSE_CONNECTED_ACTION));
    }


}
