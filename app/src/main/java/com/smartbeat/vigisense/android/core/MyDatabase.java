package com.smartbeat.vigisense.android.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.smartbeat.vigisense.android.api.Database;
import com.smartbeat.vigisense.android.model.BodyScale;
import com.smartbeat.vigisense.android.model.BpMonitor;
import com.smartbeat.vigisense.android.model.MioFuseActivity;
import com.smartbeat.vigisense.android.model.MioFuseBattery;
import com.smartbeat.vigisense.android.model.MioFuseDailyActivity;
import com.smartbeat.vigisense.android.model.MioFuseHeartRate;
import com.smartbeat.vigisense.android.model.MyBean;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class MyDatabase extends OrmLiteSqliteOpenHelper {

    private static final long HOUR = 60 * 60 * 1000;
    private static final long DAY = 24 * 60 * 60 * 1000;
    protected static String TAG = SQLiteOpenHelper.class.getName();
    protected boolean createDemoData = true;

    public MyDatabase(Context context) {
        this(context, Database.DATABASE_NAME);
    }
    public MyDatabase(Context context, String databaseName) {
        super(context, databaseName, null, Database.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
       createAll();
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        Log.w(TAG,"Upgrading database from version " + oldVersion + " to " + newVersion + ". Old data will be destroyed");
        deleteAll();
        createAll();

    }

    protected void createAll() {
        for (MyBean table : PlugYouTables.getTables())
            try {
                TableUtils.createTable(connectionSource, table.getClass());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        if (createDemoData)
            createDemoData();
    }

    private void deleteAll() {
        for (MyBean table : PlugYouTables.getTables())
            try {
                TableUtils.dropTable(connectionSource, table.getClass(),true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    public void reset() {
        Log.i(TAG,"reset");
        deleteAll();
        createAll();
    }

    public <T extends MyBean> MyBean create(T row) throws SQLException {
        Dao<T, ?> dao = (Dao<T, ?>) dao(row.getClass());
        dao.create(row);
        return row;
    }

    public <T extends MyBean> MyBean createWithId(T row) throws SQLException {
        Dao<T, ?> dao = (Dao<T, ?>) dao(row.getClass());
        return  dao.createIfNotExists(row);
    }

    public <T extends MyBean> List<T> getAll(Class<T> clazz) throws SQLException {
        Dao<T, ?> dao = dao(clazz);
        return dao.queryForAll();
    }

    public <T extends MyBean, D> T get(Class<T> clazz, D id) throws SQLException {
        Dao<T, D> dao = dao(clazz);
        return dao.queryForId(id);
    }

    public <T extends MyBean> T get(T row) throws SQLException {
        Dao<T, ?> dao = (Dao<T, ?>) dao(row.getClass());
        List<T> rows = dao.queryForMatching(row);
        if (rows.size() == 0)
            return null;
        return rows.get(0);
    }

    public <T extends MyBean> List<T> select(T row) throws SQLException {
        Dao<T, ?> dao = (Dao<T, ?>) dao(row.getClass());
        return dao.queryForMatching(row);
    }

    public <T extends MyBean> boolean update(T row) throws SQLException {
        Dao<T, ?> dao = (Dao<T, ?>) dao(row.getClass());
        return dao.update(row) == 1;
    }

    public <T extends MyBean> void createOrUpdate(T row) throws SQLException {
        Dao<T, ?> dao = (Dao<T, ?>) dao(row.getClass());
        dao.createOrUpdate(row);
    }

    public <T extends MyBean, D> Dao<T, D> dao(Class<T > clazz) throws SQLException {
        return getDao(clazz);
    }

    public void createDemoData() {
        new MyTimer(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.i(TAG,"createAll, insert demo data...");
                    Calendar midnight = Calendar.getInstance();
                    midnight.set(Calendar.HOUR_OF_DAY, 0);
                    midnight.set(Calendar.MINUTE, 0);
                    long startMeasure = midnight.getTimeInMillis() - 28 * DAY;

                    int batStart = rndInt(50, 100);
                    for (int day = 0; day < 21; day++) {
                        long time = startMeasure+ day * DAY + 6 * HOUR + rndLong(0,2*HOUR);


                        BodyScale bs = new BodyScale();
                        bs.setUserId(Database.DEMO_USER_ID_VALUE);
                        bs.setTimestampMs(time);
                        bs.setBatteryPercentInt(batStart - day);
                        bs.setWeightKgFloat(rndFloat(60,80) + day);
                        bs.setFatPercentFloat(rndFloat(20,30)  + day * 0.2f);
                        create(bs);

                        BpMonitor bp = new BpMonitor();
                        bp.setUserId(Database.DEMO_USER_ID_VALUE);
                        bp.setTimestampMs(time);
                        bp.setSystoleInt(rndInt(110,130) - day);
                        bp.setDiastoleInt(rndInt(70,90)  - day);
                        bp.setHeartRateInt( rndInt(50,150));
                        create(bp);

                        MioFuseActivity step = new MioFuseActivity();
                        step.setTimestampMs(time);
                        step.setUserId(Database.DEMO_USER_ID_VALUE);
                        step.setStepsInt( rndInt(1, 200));
                        create(step);

                        MioFuseDailyActivity daily = new MioFuseDailyActivity();
                        Calendar c = Calendar.getInstance();
                        c.setTimeInMillis(time);
                        Integer dayOfM = c.get(Calendar.DAY_OF_MONTH);
                        Integer month = c.get(Calendar.MONTH);
                        Integer year = c.get(Calendar.YEAR);
                        daily.setUserId(Database.DEMO_USER_ID_VALUE);
                        Integer steps = rndInt(1000, 4000);
                        daily.setStepsInt(steps);
                        SimpleDateFormat ddMMyyy = new SimpleDateFormat("ddMMyyyy");
                        daily.setTimestampMs(ddMMyyy.parse(dayOfM+""+month+""+year).getTime());
                        daily.setDateStr(year + "/" + month + "/" + day);

                        MioFuseBattery battery = new MioFuseBattery();
                        battery.setUserId(Database.DEMO_USER_ID_VALUE);
                        battery.setBatteryPercentInt(rndInt(10, 100));
                        battery.setTimestampMs(time);

                        int total = 0;
                        for (int hour = 0; hour < 24; hour++) {
//                            if (hour > 7 && hour < 21 )
//                                total +=  rndInt(0, 500);
//                            Pedometer p = new Pedometer();
//                            p.setUserId(Database.DEMO_USER_ID_VALUE);
//                            p.setTimestampMs(time + hour + rndTime(0,15000));
//                            p.setStepsInt(total);
//                            p.setDistanceMeterFloat(total/ 1.08f);
//                            p.setCalorieFloat(total / 18f);
//                            create(p);
                            MioFuseHeartRate hrBean = new MioFuseHeartRate();
                            hrBean.setUserId(Database.DEMO_USER_ID_VALUE);
                            hrBean.setHeartRateInt(rndInt(60, 110));
                            hrBean.setTimestampMs(time + hour);
                        }

                    }
                    Log.i(TAG,"createAll, insert demo data done.");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private long rndTime(long maxDelay, long minDelay) {
        return System.currentTimeMillis() - rndLong(minDelay, maxDelay);
    }

    private float rndFloat(int min, int max) {
        return (float) (min + Math.random() * (max - min));
    }

    private int rndInt(int min, int max) {
        return (int) (min + Math.random() * (max - min));
    }

    private long rndLong(long min, long max) {
        return (long) (min + Math.random() *  (max - min));
    }

    public void onDestroy() {

    }
}
