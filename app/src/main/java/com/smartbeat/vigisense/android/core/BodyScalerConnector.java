package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.smartbeat.vigisense.android.model.BodyScale;

import java.math.BigDecimal;
import java.nio.ByteBuffer;

public class BodyScalerConnector extends MyGattCallback {
    public static final String battery_2a19 = "00002a19-0000-1000-8000-00805f9b34fb";
    public static final String notif_ffe4 = "0000ffe4-0000-1000-8000-00805f9b34fb";
    public static final String write_ffe9 = "0000ffe9-0000-1000-8000-00805f9b34fb"; //Key Press State
    private final MyContext ctx;

    private static final String TAG = BodyScalerConnector.class.getName();
    private final MyBluetoothGatt myGatt;
    private final BodyScale bodyScaler;
    private BluetoothDevice device;

    public BodyScalerConnector(MyContext myContext) {
        ctx = myContext;
        myGatt = new MyBluetoothGatt(ctx, this);
        bodyScaler = new BodyScale();
    }

    public void onScan(BluetoothDevice btDevice) {
        try {
//            Log.e(TAG,"connecting");
            device = btDevice;
            myGatt.connect(device);
        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }

    public void onDestroy() {
        if (myGatt != null)
            myGatt.onDestroy();
    }

    public void onConnected() {
        Log.e(TAG, "onConnected");
        myGatt.notification(notif_ffe4);

        myGatt.write(write_ffe9, "0d11100100000001af020055001f4001a7");
        myGatt.write(write_ffe9, toCmd(1, 70, 175, 1));
        myGatt.read(battery_2a19);

        myGatt.nextCmd();
        ctx.getLogicOutput().bodyScalerConnecting();
    }

//    INIT 0d1e12060100000001af010055001b5801b3, 36
//    MEASURING 0d1e0b01000006a91871d2,
//    END 0d1e140200000006ae000000012106005b0000dd, 40

    public void onCharacteristicChanged(BluetoothGattCharacteristic chr) throws Exception {
        byte[] bytes = chr.getValue();
        String str = BleUtil.toHexStr(bytes);
        Log.d(TAG, "onCharacteristicChanged: " + str);
        if (str.length() >= 40) {
            float tmp = (((float) Integer.parseInt(str.substring(26, 30), 16)) * 0.01f) + 0.005f;
            float weight_kg = (Float.isNaN(tmp) || ((double) tmp) == 0.0d) ? tmp : new BigDecimal((double) tmp).setScale(2, 4).floatValue();
            float fat_percent = 0.0f;
            int parseInt = Integer.parseInt(str.substring(2, 4), 16);
            if (parseInt >= 30)
                fat_percent = ((float) Integer.parseInt(str.substring(34, 38), 16)) * 0.01f;
//                Log.i(TAG, "parseInt: " + parseInt + ", parseInt2: " + parseInt2 + ", date: " + date.getTime() + ", parseInt3: " + parseInt3 + ", a2: " + weight + ", parseInt4: " + parseInt4 + ", f: " + fat);
            Log.i(TAG, "weight: " + weight_kg + "kg, fat: " + fat_percent + "%");
            bodyScaler.setUserId(ctx.getUserId());
            bodyScaler.setTimestampMs(System.currentTimeMillis());
            bodyScaler.setWeightKgFloat(weight_kg);
            bodyScaler.setFatPercentFloat(fat_percent);
            ctx.getLogicOutput().bodyScalerNewData(bodyScaler);
            onDestroy();
        }
    }

    public void onCharacteristicRead(BluetoothGattCharacteristic characteristic) {
        if (characteristic.getUuid().toString().equalsIgnoreCase(battery_2a19)) {
            int lastBateryPercent = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0) * 100 / 255;
            Log.i(TAG, "battery: " + lastBateryPercent + "%");
            bodyScaler.setBatteryPercentInt(lastBateryPercent);
            ctx.getLogicOutput().bodyScalerBatteryCharge(bodyScaler);
        }
    }

    public void onDisconnected() {
        ctx.getLogicOutput().bodyScalerDisconnected();
    }

    public void onError(String message) {
        onDestroy();
        ctx.getLogicOutput().bodyScalerOnError(message);
    }

    public String toCmd(int userId, int basisWight, int heightCm, int sex) {
        byte b = 0x01;
        byte[] c = m13577c((int) (basisWight * 100.0f), 4); // basisWeight
        byte[] c2 = m13577c(userId, 4); //userId
        byte[] bArr = new byte[17];
        bArr[0] = 0x0d;
        bArr[1] = (byte) 17;
        bArr[2] = (byte) 16; //const 16
        bArr[3] = b;
        bArr[4] = c2[0];
        bArr[5] = c2[1];
        bArr[6] = c2[2];
        bArr[7] = c2[3];
        bArr[8] = (byte) heightCm; //height
        bArr[9] = (byte) sex; //sex
        bArr[10] = m13560a(0);
        bArr[11] = (byte) 85; //waistLine
        bArr[12] = 0; //bust
        bArr[13] = c[2];
        bArr[14] = c[3];
        bArr[15] = 1; //unit
        bArr[16] = (byte) ((((((((((((((bArr[1] ^ bArr[2]) ^ bArr[3]) ^ bArr[4]) ^ bArr[5]) ^ bArr[6]) ^ bArr[7]) ^ bArr[8]) ^ bArr[9]) ^ bArr[10]) ^ bArr[11]) ^ bArr[12]) ^ bArr[13]) ^ bArr[14]) ^ bArr[15]);
        return toHexStr(bArr);
    }

    public static String toHexStr(byte[] value) {
        String hexStr = "";
        for (byte b : value)
            hexStr += String.format("%02x", b);
        return hexStr;
    }

    public static byte[] m13577c(int i, int i2) {
        ByteBuffer wrap = ByteBuffer.wrap(new byte[i2]);
        wrap.asIntBuffer().put(i);
        return wrap.array();
    }

    public static byte m13560a(int i) {
        int i2 = i % 256;
        if (i < 0) {
            if (i2 < 128) {
                i2 += 256;
            }
            return (byte) i2;
        }
        if (i2 > 127) {
            i2 += 0xffffff00;
        }
        return (byte) i2;
    }
}
