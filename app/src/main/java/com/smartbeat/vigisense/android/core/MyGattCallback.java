package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothGattCharacteristic;

/**
 * Created by bruno on 26/04/2016.
 */
public abstract class MyGattCallback {

    public void onCharacteristicChanged(BluetoothGattCharacteristic characteristic) throws Exception {

    }

    public void onDisconnected() {

    }

    public void onError(String status){

    }

    public void onConnected() {

    }

    public void onCharacteristicRead(BluetoothGattCharacteristic characteristic) {

    }
}
