package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.support.annotation.NonNull;
import android.util.Log;

import com.smartbeat.vigisense.android.model.Pedometer;

import java.util.Calendar;


public class PedometerConnector extends MyGattCallback {
    private static final long DAY = 24 * 60 * 60 * 1000;
    private final MyContext ctx;

    private static final String TAG = PedometerConnector.class.getName();
    private final MyBluetoothGatt myGatt;
    private BluetoothDevice device;
    private MyLooper looper;

    public PedometerConnector(MyContext myContext) {
        ctx = myContext;
        myGatt = new MyBluetoothGatt(myContext, this);
    }

    public void onScan(BluetoothDevice btDevice) {
        try {
            device = btDevice;
            ctx.getLogicOutput().pedometerConnecting();
            myGatt.connect(device);
        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }

    public void onError(String message) {
        onDestroy();
        ctx.getLogicOutput().pedometerOnError(message);
    }

    public void onDestroy() {
        if (myGatt != null)
            myGatt.onDestroy();
        if (looper != null && !looper.isCanceled())
            looper.cancel();
    }

    public static final String notif1_ff22 = "0000ff22-0000-1000-8000-00805f9b34fb";
    public static final String write1_ff21 = "0000ff21-0000-1000-8000-00805f9b34fb";

    public void onConnected() {
        myGatt.notification(notif1_ff22);
        for (int i = 1; i < 8; i++)
            myGatt.write(write1_ff21, "21FF1405000" + i + "000000");
        setTime();

        myGatt.write(write1_ff21, "21FF12020001");
        myGatt.write(write1_ff21, "21FF0000");
        myGatt.write(write1_ff21, "21FF1100");
        myGatt.write(write1_ff21, "21FF2700");
        myGatt.write(write1_ff21, "21FF2300");
        myGatt.write(write1_ff21, "21FF2500");
        myGatt.write( write1_ff21, "21FF2700");
        myGatt.write(write1_ff21, "21FF180700010000000000");
        myGatt.write(write1_ff21, "21FF1100");
        myGatt.write(write1_ff21, "21FF2500");
        myGatt.write( write1_ff21, "21FF2700");
        myGatt.write( write1_ff21, getDateTimeStr());
        myGatt.write( write1_ff21, "21FF2700");
        myGatt.nextCmd();
//        runLoop(10000);
    }

    private void setTime() {
        String hexBytes = getDateTimeStr();
        myGatt.write(write1_ff21, hexBytes);
    }

    @NonNull
    private String getDateTimeStr() {
        Calendar c = Calendar.getInstance();
//        String h = "00";
        String y = String.format("%02x", c.get(Calendar.YEAR)-2000);
        String month = String.format("%02x",c.get(Calendar.MONTH)-1);
        String day = String.format("%02x",c.get(Calendar.DAY_OF_MONTH)-1);

        String s = String.format("%02x", c.get(Calendar.SECOND));
        String m = String.format("%02x",c.get(Calendar.MINUTE));
        String h = String.format("%02x",c.get(Calendar.HOUR));
        String setTime =  y + month + day + h + m + s;
        Log.e(TAG, "setTime: "+setTime);
        return "21FF1006" + setTime;
//        return "22FF1106" + setTime;
    }

//    private void runLoop(final int loopDuration) {
//        if (looper!=null && !looper.isCanceled())
//            looper.cancel();
//        looper = new MyLooper(loopDuration, new Runnable() {
//            public void run() {
//                try {
//                    Log.v(TAG, "loop");
//                    myGatt.write(write1_ff21, "21FF2700");
//                    myGatt.write(write1_ff21, "21FF1100");
//                    myGatt.write(write1_ff21, "21FF2500");
//                    setTime();
//                    myGatt.nextCmd();
//                    if (myGatt != null && myGatt.isConnected())
//                        looper.cancel();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    looper.cancel();
//                }
//            }
//        });
//    }

    public void onCharacteristicChanged(BluetoothGattCharacteristic chr) throws Exception {
        String hexStr = BleUtil.toHexStr(chr.getValue());
        if (hexStr.startsWith("22ff27")) {
            int nowStepTotal = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 8);
            float nowDistanceTotal = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 12) * 0.1f;
            float nowCalorieTotal = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 16) * .01f;
            Log.i(TAG, "step: " + nowStepTotal + ", distance: " + nowDistanceTotal + ", calorie: " + nowCalorieTotal);
            Pedometer pedometer = new Pedometer();
            pedometer.setUserId(ctx.getUserId());
            pedometer.setTimestampMs(System.currentTimeMillis());
            pedometer.setStepsInt(nowStepTotal);
            pedometer.setDistanceMeterFloat(nowDistanceTotal);
            pedometer.setCalorieFloat(nowCalorieTotal);
            ctx.getLogicOutput().podometerCurrentDayData(pedometer);
            onDestroy();
        }
    }


    public void onDisconnected() {
        ctx.getLogicOutput().pedometerDisconnected();
    }

}