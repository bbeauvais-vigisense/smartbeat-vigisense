package com.smartbeat.vigisense.android.core;

import android.content.Context;
import android.content.SharedPreferences;

import com.smartbeat.vigisense.android.api.Input;

public class MyContext {
    public static final String SHARED_PREFS_NAME = "com.smartbeat.vigisense";
    public static String lastHourlyAggregation  = "lastHourlyAggregation";
    public static String lastDayAggregation = "lastDayAggregation";
    public static String lastEndActivityDayAggregation;
    private final LogicOutput logicOutput;
    private final DeviceScanner scanner;
    private final Context ctx;
    private final PedometerConnector podometer;
//    private final BpMonitorConnector bpMonitorConnector;
    private final BodyScalerConnector bodyScaler;
    private final OximeterConnector oximeter;
    private final MioFuseConnector mioFuse;
    private final _37Connector _37;
    private final CloudSynchroniser syncroniser;
    private final SharedPreferences prefs;
    private final MyDatabase db;
    private final ScenariosRunner scenario;
    private final LogicInput logicInput;
    private final BpMonitorLEConnector bpMonitorLE;

    public MyContext(MyService mainService) {
            this(mainService.getBaseContext());

    }

    public MyContext(Context context) {
        ctx = context;
        prefs = ctx.getSharedPreferences(SHARED_PREFS_NAME,Context.MODE_PRIVATE);
        db = new MyDatabase(this.getCtx());
        logicOutput = new LogicOutput(this);
        logicInput = new LogicInput(this);
        syncroniser = new CloudSynchroniser(this);
        podometer = new PedometerConnector(this);
//        bpMonitorConnector = new BpMonitorConnector(this);
        bodyScaler = new BodyScalerConnector(this);
        oximeter = new OximeterConnector(this);
        mioFuse = new MioFuseConnector(this);
        _37 = new _37Connector(this);
        bpMonitorLE = new BpMonitorLEConnector(this);
        scanner = new DeviceScanner(this);
        scenario = new ScenariosRunner(this);
    }

    public void destroy() {
        if (scenario!=null)
            scenario.onDestroy();
        if (db!=null)
            db.onDestroy();
        if (logicOutput !=null)
            logicOutput.onDestroy();
        if (syncroniser!=null)
            syncroniser.onDestroy();
        if (podometer!=null)
            podometer.onDestroy();
//        if (bpMonitorConnector !=null)
//            bpMonitorConnector.onDestroy();
        if (bpMonitorLE !=null)
            bpMonitorLE.onDestroy();
        if (bodyScaler!=null)
            bodyScaler.onDestroy();
        if (oximeter!=null)
            oximeter.onDestroy();
        if (mioFuse!=null)
            mioFuse.onDestroy();
        if (scanner!=null)
            scanner.onDestroy();
        MyTimer.clear();
    }

    public String getUserId() {
     return getString(Input.START_SCAN_USER_ID_STR, "defaultUserId");
    }

    public SharedPreferences.Editor getEdit() {
        return prefs.edit();
    }

    public long getLong(String key, long defaultz) {
        return prefs.getLong(key,defaultz);
    }

    public float getFloat(String key, float defaultz) {
        return prefs.getFloat(key,defaultz);
    }
    public boolean getBool(String key, boolean defaultz) {
        return prefs.getBoolean(key,defaultz);
    }
    public int getInt(String key, int defaultz) {
            return prefs.getInt(key,defaultz);
    }
    public String getString(String key, String defaultz) {
        return prefs.getString(key,defaultz);
    }
    public LogicOutput getLogicOutput() {
        return logicOutput;
    }

    public DeviceScanner getScanner() {
        return scanner;
    }

    public Context getCtx() {
        return ctx;
    }

    public PedometerConnector getPedometer() {
        return podometer;
    }

//    public BpMonitorConnector getBpMonitorConnector() {
//        return bpMonitorConnector;
//    }

    public BodyScalerConnector getBodyScaler() {
        return bodyScaler;
    }

    public OximeterConnector getOximeter() {
        return oximeter;
    }

    public MioFuseConnector getMioFuse() {
        return mioFuse;
    }
    public _37Connector get37() {
        return _37;
    }

    public CloudSynchroniser getSyncroniser() {
        return syncroniser;
    }

    public MyDatabase getDb() {
        return db;
    }

    public ScenariosRunner getScenario() {
        return scenario;
    }

    public LogicInput getLogicInput() {
        return logicInput;
    }

    public String getString(int resid) {
        return getCtx().getString(resid);
    }

    public BpMonitorLEConnector getBpMonitorLE() {
        return bpMonitorLE;
    }
}
