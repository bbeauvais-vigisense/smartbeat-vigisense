package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.j256.ormlite.dao.CloseableIterator;
import com.smartbeat.vigisense.android.api.Database;
import com.smartbeat.vigisense.android.model.MioFuseBattery;
import com.smartbeat.vigisense.android.model.MioFuseDailyActivity;
import com.smartbeat.vigisense.android.model.MioFuseHeartRate;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDay;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDelta;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDeltaDay;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDeltaHour;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateHour;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MioFuseConnector extends MyGattCallback implements Runnable {

    public static final boolean DELTA_ENABLED = true;

    public static final String HEART_RATE = "00002a37-0000-1000-8000-00805f9b34fb";
    public static final String SEND_CMD = "6C722A80-5bf1-4f64-9170-381c08ec57ee";
    public static final String CMD_RESP = "6C722A82-5bf1-4f64-9170-381c08ec57ee";
    public static final String RECORD = "6C722A84-5bf1-4f64-9170-381c08ec57ee";
    public static final String BATTERY = "00002a19-0000-1000-8000-00805f9b34fb";

    public static final String CMD_TYPE_TODAY_ADL_GET = "03 25 02 01";
    public static final String CMD_TYPE_HR_SET = "09 02 4a 00 5a 6c 7e 90 a2 7e 90";
    public static final String CMD_TYPE_RUN_CMD_ENABLE = "01 12 11";
    public static final String CMD_TYPE_RUN_CMD_DISABLE = "01 12 10";
    private static final long HOUR = 60 * 60 * 1000;
    private static final long DAY = 24 * 60 * 60 * 1000;
    private static final String LAST_DAILY_KEY = "MioFuseDailyActivity";
    private static final String TAG = MioFuseConnector.class.getName();
    public static final long MEASUREMENT_INTERVAL = 15000L;
    private final MyContext ctx;
    private final MyBluetoothGatt myGatt;
    private BluetoothDevice device;
    private MyLooper looper;
    private Long lastSync = null;
    private String lastDate;
    private Long lastHourlyAggregation;
    private Long lastDayAggregation;
    private Long lastHeartRate = null;
    private Long lastHeartRateDelta;


    public MioFuseConnector(MyContext myContext) {
        ctx = myContext;
        myGatt = new MyBluetoothGatt(ctx, this);
        if (lastDate == null)
            lastDate = ctx.getString(LAST_DAILY_KEY, "0");
    }

    public static String toHexStr(byte[] value) {
        String hexStr = "";
        for (byte b : value)
            hexStr += String.format("%02x", b);
        return hexStr;
    }

    public void onScan(BluetoothDevice btDevice) {
        try {
//            Log.e(TAG,"connecting");
            device = btDevice;
            myGatt.connect(device);
        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }

    public void onDestroy() {
        if (myGatt != null)
            myGatt.onDestroy();
        if (looper != null)
            looper.cancel();
    }

    public void onConnected() {
        Log.e(TAG, "onConnected");
        myGatt.read(BATTERY);
        myGatt.notification(HEART_RATE);
        myGatt.notification(CMD_RESP);
        myGatt.notification(RECORD);
        myGatt.write(SEND_CMD, CMD_TYPE_HR_SET);
        myGatt.write(SEND_CMD, CMD_TYPE_RTC_SET());
        myGatt.write(SEND_CMD, CMD_TYPE_RUN_CMD_ENABLE);
        myGatt.write(SEND_CMD, CMD_TYPE_TODAY_ADL_GET);
        myGatt.nextCmd();
        if (looper != null)
            looper.cancel();
        looper = new MyLooper(20000, 20000, this);
        ctx.getLogicOutput().mioFuseConnecting();
    }

    @Override
    public void run() {
        Log.d(TAG, "loop");
        if (lastSyncIsTooOld()) {
            myGatt.write(SEND_CMD, CMD_TYPE_RUN_CMD_ENABLE);
            myGatt.write(SEND_CMD, CMD_TYPE_TODAY_ADL_GET);
            myGatt.nextCmd();
        }
    }

    private boolean lastSyncIsTooOld() {
        return lastSync == null || lastSync < System.currentTimeMillis() - DAY;
    }

    public void onCharacteristicChanged(BluetoothGattCharacteristic chr) throws Exception {
        byte[] bytes = chr.getValue();
        String str = BleUtil.toHexStr(bytes);
        Log.d(TAG, "onCharacteristicChanged: " + str);

        long now = System.currentTimeMillis();
        if (chr.getUuid().toString().equalsIgnoreCase(HEART_RATE)) {
            Integer hr = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 1);
            if (hr == null)
                return;
            onNewHr(now, hr);
            return;
        }
        //0d 80 25 00 02 05 00 01 00 1a 00 00 00 01 00
        if (chr.getUuid().toString().equalsIgnoreCase(CMD_RESP) && chr.getValue().length == 15 && lastSyncIsTooOld()) {
            lastSync = now;
            Integer length = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 5);
            Integer index = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 7);
            Log.e(TAG, "length: " + length + ", index: " + index);
            if (index == 1) {
                for (int i = 0; i < length; i++)
                    myGatt.write(SEND_CMD, CMD_TYPE_TODAY_ADL_GET);
                myGatt.write(SEND_CMD, CMD_TYPE_RUN_CMD_DISABLE);
                myGatt.nextCmd();
            }
        }

        //ed be 02 01 00 01 00 1a 00 1c 07 74 3c 05 40 00 00 00 2a 00
        if (chr.getUuid().toString().equalsIgnoreCase(RECORD) && chr.getValue().length == 20) {
            Integer steps = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 7 * 2);
            Integer day = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 9);
            Integer month = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 10);
            Integer year = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 11) + 1900;
            if (steps == null)
                return;
            MioFuseDailyActivity daily = new MioFuseDailyActivity();
            daily.setUserId(ctx.getUserId());
            SimpleDateFormat ddMMyyy = new SimpleDateFormat("dd-MM-yyyy");
            daily.setTimestampMs(ddMMyyy.parse(day + "-" + month + "-" + year).getTime());
            daily.setDateStr(year + "/" + month + "/" + day);
            if (ctx.getDb().get(daily) == null) {
                daily.setStepsInt(steps);
                Log.e(TAG, "MioFuseDailyActivity: " + daily);
                ctx.getLogicOutput().mioFuseDailyActivity(daily);
            } else {
                daily.setStepsInt(steps);
                Log.v(TAG, "Already exist MioFuseDailyActivity: " + daily);
            }
        }

    }

    public void onNewHr(long now, Integer hr) throws Exception {
        MioFuseHeartRate hrBean = new MioFuseHeartRate();
        hrBean.setUserId(ctx.getUserId());
        hrBean.setHeartRateInt(hr);
        hrBean.setTimestampMs(now);
        ctx.getLogicOutput().mioFuseHeartRateNoSave(hrBean);

        if (DELTA_ENABLED) {
            if (lastHeartRateDelta != null && now - lastHeartRateDelta < 5000L) {
                MioFuseHeartRateDelta mioFuseHeartRateDelta = new MioFuseHeartRateDelta();
                mioFuseHeartRateDelta.setUserId(ctx.getUserId());
                mioFuseHeartRateDelta.setDelta(now - lastHeartRateDelta);
                mioFuseHeartRateDelta.setTimestampMs(now);
                ctx.getLogicOutput().mioFuseHeartRateDelta(mioFuseHeartRateDelta);
            }
            lastHeartRateDelta = now;
        }
        Log.i(TAG, "HEART_RATE: " + hr + " bpm, lastSave: " + (lastHeartRate == null ? "null" : (now - lastHeartRate) + " mustBeSaved: " + (now - lastHeartRate > 60000)));
        if (lastHeartRate != null && (now - lastHeartRate) < MEASUREMENT_INTERVAL) {
            Log.v(TAG, "HEART_RATE ignored...");
            return;
        }
        lastHeartRate = now;

        Log.i(TAG, "HEART_RATE save...");
        ctx.getLogicOutput().mioFuseHeartRate(hrBean);
        aggregateIfNeed();
        return;
    }

    private synchronized void aggregateIfNeed() {
        long now = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(now);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        int nowHour = cal.get(Calendar.HOUR_OF_DAY);
        long nowHourTimestamp = cal.getTimeInMillis();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.setTimeInMillis(now);
        int nowDay = cal.get(Calendar.DAY_OF_MONTH);
        long nowDayTimestamp = cal.getTimeInMillis();

        if (lastHourlyAggregation == null) {
            lastHourlyAggregation = ctx.getLong(MyContext.lastHourlyAggregation, 0L);
            if (lastHourlyAggregation == 0L) {
                lastHourlyAggregation = now;
                ctx.getEdit().putLong(MyContext.lastHourlyAggregation, now).commit();
            }
        }

        cal.setTimeInMillis(lastHourlyAggregation);
        int lastHour = cal.get(Calendar.HOUR_OF_DAY);

        Log.i(TAG, "lastHourlyAggregation: " + lastHour + ", nowHour: " + nowHour);
        if (lastHour != nowHour) {
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            long lastHourTimestamp = cal.getTimeInMillis();
            aggregateLastHours(lastHourTimestamp, nowHourTimestamp);
            ctx.getEdit().putLong(MyContext.lastHourlyAggregation, now).commit();
            lastHourlyAggregation = now;
        }


        if (lastDayAggregation == null) {
            lastDayAggregation = ctx.getLong(MyContext.lastDayAggregation, 0L);
            if (lastDayAggregation == 0L) {
                lastDayAggregation = now;
                ctx.getEdit().putLong(MyContext.lastDayAggregation, now).commit();
            }
        }

        cal.setTimeInMillis(lastDayAggregation);
        int lastDay = cal.get(Calendar.DAY_OF_MONTH);

        Log.i(TAG, "lastDayAggregation: " + lastDay + ", nowHour: " + nowDay);
        if (lastDay != nowDay) {
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            long lastDayTimestamp = cal.getTimeInMillis();
            aggregateLastDays(lastDayTimestamp, nowDayTimestamp);
            lastDayAggregation = now;
            ctx.getEdit().putLong(MyContext.lastDayAggregation, now).commit();
        }
    }

    private void aggregateLastDays(final long lastDayTimestamp, final long nowDayTimestamp) {
        new MyTimer(new Runnable() {
            public void run() {
                for (long currentEndDay = lastDayTimestamp + DAY; currentEndDay <= nowDayTimestamp; currentEndDay += DAY)
                    aggregateDay(currentEndDay);
            }
        });
    }

    private void aggregateDay(final long endDayTimestamp) {
        new MyTimer(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.i(TAG, "aggregateLastDay from " + (endDayTimestamp - DAY));
                    CloseableIterator<MioFuseHeartRate> iterator = null;
                    try {
                        iterator = ctx.getDb().dao(MioFuseHeartRate.class).queryBuilder().where().gt(Database.TIMESTAMP_MS, endDayTimestamp - DAY).and().lt(Database.TIMESTAMP_MS, endDayTimestamp).iterator();
                        DescriptiveStatistics stats = new DescriptiveStatistics();
                        while (iterator.hasNext())
                            stats.addValue(iterator.next().getHeartRateInt());
                        MioFuseHeartRateDay day = new MioFuseHeartRateDay();
                        day.setUserId(ctx.getUserId());
                        day.setSync(false);
                        day.setTimestampMs(endDayTimestamp);
                        day.setMin((int) stats.getMin());
                        day.setFirstDecile((int) stats.getPercentile(10));
                        day.setMedian((int) stats.getPercentile(50));
                        day.setMean((int) stats.getMean());
                        day.setLastDecile((int) stats.getPercentile(90));
                        day.setMax((int) stats.getMax());
                        day.setStandardDeviation((int) stats.getStandardDeviation());
                        day.setSampleSize(stats.getN());
                        day.setFirstQuartile((int) stats.getPercentile(25));
                        day.setLastQuartile((int) stats.getPercentile(75));
                        ctx.getLogicOutput().mioFuseHeartRateDay(day);
                    } finally {
                        if (iterator != null)
                            iterator.close();
                    }

                    if (DELTA_ENABLED) {
                        Log.i(TAG, "aggregateLastDay MioFuseHeartRateDeltaDay...");
                        CloseableIterator<MioFuseHeartRateDelta> iteratorDelta = null;
                        try {
                            iteratorDelta = ctx.getDb().dao(MioFuseHeartRateDelta.class).queryBuilder().where().gt(Database.TIMESTAMP_MS, endDayTimestamp - DAY).and().lt(Database.TIMESTAMP_MS, endDayTimestamp).iterator();
                            DescriptiveStatistics stats = new DescriptiveStatistics();
                            while (iteratorDelta.hasNext())
                                stats.addValue(iteratorDelta.next().getDelta());
                            Log.i(TAG, "aggregateLastDay done, n: " + stats.getN());
                            MioFuseHeartRateDeltaDay day = new MioFuseHeartRateDeltaDay();
                            day.setUserId(ctx.getUserId());
                            day.setSync(false);
                            day.setTimestampMs(endDayTimestamp);
                            day.setMin((int) stats.getMin());
                            day.setFirstDecile((int) stats.getPercentile(10));
                            day.setMedian((int) stats.getPercentile(50));
                            day.setMean((int) stats.getMean());
                            day.setLastDecile((int) stats.getPercentile(90));
                            day.setMax((int) stats.getMax());
                            day.setStandardDeviation((int) stats.getStandardDeviation());
                            day.setSampleSize(stats.getN());
                            day.setFirstQuartile((int) stats.getPercentile(25));
                            day.setLastQuartile((int) stats.getPercentile(75));
                            Log.i(TAG, "aggregateLastDay " + (day));
                            ctx.getLogicOutput().mioFuseHeartRateDeltaDay(day);
                        } finally {
                            if (iteratorDelta != null)
                                iteratorDelta.close();
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "aggregateLastDay: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private void aggregateLastHours(final long lastHourTimestamp, final long nowHourTimestamp) {
        Log.i(TAG, "aggregateLastHours from " + lastHourTimestamp + " to " + nowHourTimestamp + "...");
        new MyTimer(new Runnable() {
            public void run() {
                Log.i(TAG, "aggregateLastHours from " + lastHourTimestamp + " to " + nowHourTimestamp + " !");
                for (long currentEndHour = lastHourTimestamp + HOUR; currentEndHour <= nowHourTimestamp; currentEndHour += HOUR)
                    aggregateHour(currentEndHour);
            }
        });
    }

    private void aggregateHour(long endHourTimestamp) {
        try {
            Log.i(TAG, "aggregateLastHours from " + (endHourTimestamp - HOUR));
            Log.i(TAG, "aggregateLastHours MioFuseHeartRateHour...");
            CloseableIterator<MioFuseHeartRate> iterator = null;
            try {
                iterator = ctx.getDb().dao(MioFuseHeartRate.class).queryBuilder().where().gt(Database.TIMESTAMP_MS, endHourTimestamp - HOUR).and().lt(Database.TIMESTAMP_MS, endHourTimestamp).iterator();
                DescriptiveStatistics stats = new DescriptiveStatistics();
                while (iterator.hasNext())
                    stats.addValue(iterator.next().getHeartRateInt());
                Log.i(TAG, "aggregateLastHours done, n: " + stats.getN());
                MioFuseHeartRateHour hour = new MioFuseHeartRateHour();
                hour.setUserId(ctx.getUserId());
                hour.setSync(false);
                hour.setTimestampMs(endHourTimestamp);
                hour.setMin((int) stats.getMin());
                hour.setFirstDecile((int) stats.getPercentile(10));
                hour.setMedian((int) stats.getPercentile(50));
                hour.setMean((int) stats.getMean());
                hour.setLastDecile((int) stats.getPercentile(90));
                hour.setMax((int) stats.getMax());
                hour.setStandardDeviation((int) stats.getStandardDeviation());
                hour.setSampleSize(stats.getN());
                hour.setFirstQuartile((int) stats.getPercentile(25));
                hour.setLastQuartile((int) stats.getPercentile(75));
                Log.i(TAG, "aggregateLastHours " + (hour));
                ctx.getLogicOutput().mioFuseHeartRateHour(hour);
            } finally {
                if (iterator != null)
                    iterator.close();
            }
            if (DELTA_ENABLED) {
                Log.i(TAG, "aggregateLastHours MioFuseHeartRateDeltaHour...");
                CloseableIterator<MioFuseHeartRateDelta> iteratorDelta = null;
                try {
                    iteratorDelta = ctx.getDb().dao(MioFuseHeartRateDelta.class).queryBuilder().where().gt(Database.TIMESTAMP_MS, endHourTimestamp - HOUR).and().lt(Database.TIMESTAMP_MS, endHourTimestamp).iterator();
                    DescriptiveStatistics stats = new DescriptiveStatistics();
                    while (iteratorDelta.hasNext())
                        stats.addValue(iteratorDelta.next().getDelta());
                    Log.i(TAG, "aggregateLastHours done, n: " + stats.getN());
                    MioFuseHeartRateDeltaHour hour = new MioFuseHeartRateDeltaHour();
                    hour.setUserId(ctx.getUserId());
                    hour.setSync(false);
                    hour.setTimestampMs(endHourTimestamp);
                    hour.setMin((int) stats.getMin());
                    hour.setFirstDecile((int) stats.getPercentile(10));
                    hour.setMedian((int) stats.getPercentile(50));
                    hour.setMean((int) stats.getMean());
                    hour.setLastDecile((int) stats.getPercentile(90));
                    hour.setMax((int) stats.getMax());
                    hour.setStandardDeviation((int) stats.getStandardDeviation());
                    hour.setSampleSize(stats.getN());
                    hour.setFirstQuartile((int) stats.getPercentile(25));
                    hour.setLastQuartile((int) stats.getPercentile(75));
                    Log.i(TAG, "aggregateLastHours " + (hour));
                    ctx.getLogicOutput().mioFuseHeartRateDeltaHour(hour);
                } finally {
                    if (iteratorDelta != null)
                        iteratorDelta.close();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "aggregateLastDay: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void onCharacteristicRead(BluetoothGattCharacteristic characteristic) {
        try {
            if (characteristic.getUuid().toString().equalsIgnoreCase(BATTERY)) {
                Integer lastBateryPercent = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0) * 100 / 255;
                if (lastBateryPercent == null)
                    return;
                Log.e(TAG, "battery: " + lastBateryPercent + "%");
                MioFuseBattery battery = new MioFuseBattery();
                battery.setUserId(ctx.getUserId());
                battery.setBatteryPercentInt(lastBateryPercent);
                battery.setTimestampMs(System.currentTimeMillis());
                ctx.getLogicOutput().mioFuseBatteryCharge(battery);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDisconnected() {
        if (looper != null)
            looper.cancel();
        ctx.getLogicOutput().mioFuseDisconnected();
    }

    public void onError(String message) {
        onDestroy();
        ctx.getLogicOutput().mioFuseOnError(message);
    }

    private byte[] CMD_TYPE_RTC_SET() {
        Calendar c = Calendar.getInstance();
        byte[] cmd = {(byte) 07, (byte) 17, (byte) 02, (byte) c.get(Calendar.SECOND), (byte) c.get(Calendar.MINUTE),
                (byte) c.get(Calendar.HOUR_OF_DAY), (byte) c.get(Calendar.DAY_OF_MONTH), (byte) c.get(Calendar.MONTH),
                (byte) (c.get(Calendar.YEAR) - 1900)};
        Log.i(TAG, "setTime: " + toHexStr(cmd) + ", hour: " + c.get(Calendar.HOUR_OF_DAY) + ", min: " + (byte) c.get(Calendar.MINUTE));
        return cmd;
    }
}
