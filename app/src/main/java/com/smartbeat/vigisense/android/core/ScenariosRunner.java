package com.smartbeat.vigisense.android.core;

import android.bluetooth.le.ScanCallback;
import android.content.Intent;
import android.util.Log;

import com.smartbeat.vigisense.android.api.Database;
import com.smartbeat.vigisense.android.api.Input;
import com.smartbeat.vigisense.android.model.BodyScale;
import com.smartbeat.vigisense.android.model.MioFuseBattery;
import com.smartbeat.vigisense.android.model.MioFuseDailyActivity;
import com.smartbeat.vigisense.android.model.MioFuseHeartRate;
import com.smartbeat.vigisense.android.model.Oximeter;
import com.smartbeat.vigisense.android.model.Pedometer;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ScenariosRunner {

    private static final String TAG = ScenariosRunner.class.getName();
    private final MyContext ctx;
    private LogicOutput proxy;

    public ScenariosRunner(MyContext myContext) {
        ctx = myContext;
    }

    public void run(int scenario) throws Exception {
        proxy = ctx.getLogicOutput();
        switch (scenario) {
            case Input.START_SCAN_SCENARIO_PEDOMETER:
                Log.v(TAG, "run START_SCAN_SCENARIO_PEDOMETER");
                proxy.scanStarted();
                proxy.pedometerConnecting();
                Pedometer pedometer = new Pedometer();
                pedometer.setUserId(ctx.getUserId());
                pedometer.setTimestampMs(System.currentTimeMillis());
                pedometer.setStepsInt(rndInt(20, 60));
                pedometer.setDistanceMeterFloat(rndFloat(10, 20));
                pedometer.setCalorieFloat(rndFloat(1, 5));
                proxy.podometerCurrentDayData(pedometer);
                proxy.pedometerDisconnected();
                proxy.scanTimeout();
                break;
            case Input.START_SCAN_SCENARIO_BODY_SCALER:
                Log.v(TAG, "run START_SCAN_SCENARIO_BODY_SCALER");
                proxy.scanStarted();
                proxy.bodyScalerConnecting();
                BodyScale bs = new BodyScale();
                bs.setUserId(ctx.getUserId());
                bs.setTimestampMs(System.currentTimeMillis());
                bs.setBatteryPercentInt(rndInt(10, 20));
                bs.setWeightKgFloat(rndFloat(20, 60));
                bs.setFatPercentFloat(rndFloat(10, 20));
                proxy.bodyScalerBatteryCharge(bs);
                bs = proxy.bodyScalerNewData(bs);
                proxy.bodyScalerDisconnected();
                proxy.scanTimeout();
                Intent bsI = new Intent().setAction(Input.VALIDATE_MEASUREMENT);
                bsI.putExtra(Input.VALIDATE_MEASUREMENT_ID,bs.get_id());
                bsI.putExtra(Input.VALIDATE_MEASUREMENT_TABLE, Database.BODY_SCALE_TABLE);
                ctx.getCtx().sendBroadcast(bsI);
                break;
            case Input.START_SCAN_SCENARIO_OXIMETER:
                Log.v(TAG, "run START_SCAN_SCENARIO_OXIMETER");
                proxy.scanStarted();
                proxy.oximeterConnecting();
                Oximeter oximeter = new Oximeter();
                oximeter.setUserId(ctx.getUserId());
                oximeter.setTimestampMs(System.currentTimeMillis());
                oximeter.setSpo2(rndInt(80, 100));
                oximeter = proxy.oximeterNewData(oximeter);
                proxy.oximeterDisconnected();
                proxy.scanTimeout();
                Intent oxiI = new Intent().setAction(Input.VALIDATE_MEASUREMENT);
                oxiI.putExtra(Input.VALIDATE_MEASUREMENT_ID,oximeter.get_id());
                oxiI.putExtra(Input.VALIDATE_MEASUREMENT_TABLE, Database.OXIMETER_TABLE);
                ctx.getCtx().sendBroadcast(oxiI);
                break;
            case Input.START_SCAN_SCENARIO_FAILED:
                Log.v(TAG, "run START_SCAN_SCENARIO_FAILED");
                proxy.scanStarted();
                proxy.scanFailed("onScanFailed, Error Code: "+ ScanCallback.SCAN_FAILED_INTERNAL_ERROR);
                break;
            case Input.START_SCAN_SCENARIO_MIO_FUSE:
                Log.v(TAG, "run START_SCAN_SCENARIO_MIO_FUSE");
                proxy.scanStarted();
                proxy.mioFuseConnecting();
                MioFuseDailyActivity daily = new MioFuseDailyActivity();
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(System.currentTimeMillis() - rndInt(24*60*60 *1000, 15*24*60*60 *1000));
                Integer day = c.get(Calendar.DAY_OF_MONTH);
                Integer month = c.get(Calendar.MONTH);
                Integer year = c.get(Calendar.YEAR);
                daily.setUserId(ctx.getUserId());
                Integer steps = rndInt(1000, 4000);
                daily.setStepsInt(steps);
                SimpleDateFormat ddMMyyy = new SimpleDateFormat("ddMMyyyy");
                daily.setTimestampMs(ddMMyyy.parse(day+""+month+""+year).getTime());
                daily.setDateStr(year + "/" + month + "/" + day);
                proxy.mioFuseDailyActivity(daily);

                MioFuseHeartRate hrBean = new MioFuseHeartRate();
                hrBean.setUserId(ctx.getUserId());
                hrBean.setHeartRateInt(rndInt(60, 160));
                hrBean.setTimestampMs(System.currentTimeMillis());
                proxy.mioFuseHeartRate(hrBean);

                MioFuseBattery battery = new MioFuseBattery();
                battery.setUserId(ctx.getUserId());
                battery.setBatteryPercentInt(rndInt(10, 100));
                battery.setTimestampMs(System.currentTimeMillis());
                proxy.mioFuseBatteryCharge(battery);

                proxy.mioFuseDisconnected();
                proxy.scanTimeout();
                break;
            default:
                Log.e(TAG, "Scenario not handled: " + scenario);
        }
    }

//    public void runBp(String action) throws Exception {
//        Log.e(TAG,"runBp:, "+action);
//        proxy = ctx.getLogicOutput();
//        switch (action) {
//            case Input.CONNECT_BP_MONITOR_ACTION:
//                Log.v(TAG, "run CONNECT_BP_MONITOR_ACTION");
//                proxy.bpMonitorConnected();
//                break;
//            case Input.START_BP_MONITOR_ACTION:
//                Log.v(TAG, "run START_BP_MONITOR_ACTION");
//                Log.e(TAG,"START_BP_MONITOR_ACTION, "+action);
//                BpMonitor bpMonitor = new BpMonitor();
//                bpMonitor.setUserId(ctx.getUserId());
//                bpMonitor.setTimestampMs(System.currentTimeMillis());
//                bpMonitor.setSystoleInt(rndInt(150, 90));
//                bpMonitor.setDiastoleInt(rndInt(50, 90));
//                bpMonitor.setHeartRateInt(rndInt(50, 180));
//                bpMonitor = proxy.bpMonitorNewData(bpMonitor);
//                proxy.bpMonitorDisconnected();
//                Intent pbI = new Intent().setAction(Input.VALIDATE_MEASUREMENT);
//                pbI.putExtra(Input.VALIDATE_MEASUREMENT_ID,bpMonitor.get_id());
//                pbI.putExtra(Input.VALIDATE_MEASUREMENT_TABLE, Database.BP_MONITOR_TABLE);
//                ctx.getCtx().sendBroadcast(pbI);
//                break;
//            default:
//                Log.e(TAG, "Scenario not handled: " + action);
//        }
//    }

     private float rndFloat(int min, int max) {
        return (float) (min + Math.random() * (max - min));
    }

    private int rndInt(int min, int max) {
        return (int) (min + Math.random() * (max - min));
    }

    public void onDestroy() {

    }


}
