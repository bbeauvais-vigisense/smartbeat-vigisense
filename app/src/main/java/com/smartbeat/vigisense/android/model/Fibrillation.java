package com.smartbeat.vigisense.android.model;


import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

@AdditionalAnnotation.Contract()
@DatabaseTable(tableName = "Fibrillation")
@AdditionalAnnotation.DefaultContentUri(authority = "com.smartbeat.provider.Vigisense", path = "Fibrillation")
@AdditionalAnnotation.DefaultContentMimeTypeVnd(name = "com.smartbeat.provider.Vigisense", type = "Fibrillation")

/**
 json=[{"heart_beat":"100","fibrillation":"yes"}]
 */
public class Fibrillation extends MyBean {

    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    @AdditionalAnnotation.DefaultSortOrder
    Long _id;
    @DatabaseField(defaultValue = "false")
    Boolean sync;
    @DatabaseField(canBeNull = false)
    String userId;
    @DatabaseField(canBeNull = false)
    Long timestampMs;
    @DatabaseField(canBeNull = false)
    String json;

    public Fibrillation() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public Boolean getSync() {
        return sync;
    }

    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    @Override
    public String toString() {
        return "Fibrillation{" +
                "id=" + _id +
                ", sync=" + sync +
                ", userId='" + userId + '\'' +
                ", timestampMs=" + timestampMs +
                ", json=" + json +
                '}';
    }
}