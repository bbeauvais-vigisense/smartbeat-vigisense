package com.smartbeat.vigisense.android.model;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

@AdditionalAnnotation.Contract()
@DatabaseTable(tableName = "BpMonitor")
@AdditionalAnnotation.DefaultContentUri(authority = "com.smartbeat.provider.Vigisense", path = "BpMonitor")
@AdditionalAnnotation.DefaultContentMimeTypeVnd(name = "com.smartbeat.provider.Vigisense", type = "BpMonitor")
public class BpMonitor extends MyBean {

    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    @AdditionalAnnotation.DefaultSortOrder
    Long _id;
    @DatabaseField(defaultValue = "true")
    Boolean sync;
    @DatabaseField(canBeNull = false)
    String userId;
    @DatabaseField(canBeNull = false)
    Long timestampMs;
    @DatabaseField(canBeNull = false)
    Integer systoleInt;
    @DatabaseField(canBeNull = false)
    Integer diastoleInt;
    @DatabaseField(canBeNull = false)
    Integer heartRateInt;

    public BpMonitor() {}

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public Boolean getSync() {
        return sync;
    }

    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getSystoleInt() {
        return systoleInt;
    }

    public void setSystoleInt(Integer systoleInt) {
        this.systoleInt = systoleInt;
    }

    public Integer getDiastoleInt() {
        return diastoleInt;
    }

    public void setDiastoleInt(Integer diastoleInt) {
        this.diastoleInt = diastoleInt;
    }

    public Integer getHeartRateInt() {
        return heartRateInt;
    }

    public void setHeartRateInt(Integer heartRateInt) {
        this.heartRateInt = heartRateInt;
    }

    @Override
    public String toString() {
        return "BpMonitor{" +
                "id=" + _id +
                ", sync=" + sync +
                ", userId='" + userId + '\'' +
                ", timestampMsLong=" + timestampMs +
                ", systoleInt=" + systoleInt +
                ", diastoleInt=" + diastoleInt +
                ", heartRateInt=" + heartRateInt +
                '}';
    }
}
