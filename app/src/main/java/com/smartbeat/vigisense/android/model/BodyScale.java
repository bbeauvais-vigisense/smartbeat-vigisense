package com.smartbeat.vigisense.android.model;


import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

@AdditionalAnnotation.Contract()
@DatabaseTable(tableName = "BodyScale")
@AdditionalAnnotation.DefaultContentUri(authority = "com.smartbeat.provider.Vigisense", path = "BodyScale")
@AdditionalAnnotation.DefaultContentMimeTypeVnd(name = "com.smartbeat.provider.Vigisense", type = "BodyScale")
public class BodyScale extends MyBean {

    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    @AdditionalAnnotation.DefaultSortOrder
    Long _id;
    @DatabaseField(defaultValue = "true")
    Boolean sync;
    @DatabaseField(canBeNull = false)
    String userId;
    @DatabaseField(canBeNull = false)
    Long timestampMs;
    @DatabaseField(canBeNull = false)
    Integer batteryPercentInt;
    @DatabaseField(canBeNull = false)
    Float weightKgFloat;
    @DatabaseField(canBeNull = false)
    Float fatPercentFloat;

    public BodyScale() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public Boolean getSync() {
        return sync;
    }

    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimestampMs() {
        return timestampMs;
    }

    public void setTimestampMs(Long timestampMs) {
        this.timestampMs = timestampMs;
    }

    public Integer getBatteryPercentInt() {
        return batteryPercentInt;
    }

    public void setBatteryPercentInt(Integer batteryPercentInt) {
        this.batteryPercentInt = batteryPercentInt;
    }

    public Float getWeightKgFloat() {
        return weightKgFloat;
    }

    public void setWeightKgFloat(Float weightKgFloat) {
        this.weightKgFloat = weightKgFloat;
    }

    public Float getFatPercentFloat() {
        return fatPercentFloat;
    }

    public void setFatPercentFloat(Float fatPercentFloat) {
        this.fatPercentFloat = fatPercentFloat;
    }

    @Override
    public String toString() {
        return "BodyScale{" +
                "id=" + _id +
                ", sync=" + sync +
                ", userId='" + userId + '\'' +
                ", timestampMs=" + timestampMs +
                ", batteryPercentInt=" + batteryPercentInt +
                ", weightKgFloat=" + weightKgFloat +
                ", fatPercentFloat=" + fatPercentFloat +
                '}';
    }
}