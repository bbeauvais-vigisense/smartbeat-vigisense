package com.smartbeat.vigisense.android.example;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartbeat.vigisense.android.api.Database;
import com.smartbeat.vigisense.android.api.Input;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InputActivity extends AppCompatActivity {

    private static final String TAG = InputActivity.class.getName();
    private LinearLayout.LayoutParams linearParams;
    private LinearLayout verticalLayout;
    private OutputReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUi();
        receiver = new OutputReceiver(this).init();
    }

    @Override
    protected void onDestroy() {
        receiver.onDestroy();
        super.onDestroy();
    }

    private void initUi() {
        linearParams = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        ScrollView scroll = new ScrollView(this);
        setContentView(scroll, linearParams);
        verticalLayout = new LinearLayout(this);
        scroll.addView(verticalLayout, linearParams);
        verticalLayout.setOrientation(LinearLayout.VERTICAL);

        addStartScanBtn(Input.START_SCAN_FAKE_DATA_DISABLE, "Scan & connect to all device");
        addStartScanBtn(Input.START_SCAN_FAKE_DATA_DISABLE, "Scan & connect to body scale", Input.DEVICE_TO_CONNECT.BODY_SCALE);
        addStartScanBtn(Input.START_SCAN_FAKE_DATA_DISABLE, "Scan & connect to 37°", Input.DEVICE_TO_CONNECT.MIO_FUSE);
        addStartScanBtn(Input.START_SCAN_FAKE_DATA_DISABLE, "Scan & connect to oximeter", Input.DEVICE_TO_CONNECT.OXIMETER);
        addStartScanBtn(Input.START_SCAN_FAKE_DATA_DISABLE, "Scan & connect to blood pressure monitor", Input.DEVICE_TO_CONNECT.BLOOD_PRESSURE_LE);
//        addActionBtn(Input.CONNECT_BP_MONITOR_ACTION, "Connect to the blood pressure device");
//        addActionBtn(Input.START_BP_MONITOR_ACTION, "Start to measure blood pressure");
        addActionBtn(Input.STOP_SERVICE_ACTION, "Close connections");
        addActionBtn(Input.SYNC_WITH_SERVER, "Synchronise data with server");

        addSep("Scenarios with devices simulation");

        addStartScanBtn(Input.START_SCAN_SCENARIO_PEDOMETER, "Scan and find PedometerConnector");
        addStartScanBtn(Input.START_SCAN_SCENARIO_BODY_SCALER, "Scan and find Body Scaler");
        addStartScanBtn(Input.START_SCAN_SCENARIO_MIO_FUSE, "Scan and find 37°");
        addStartScanBtn(Input.START_SCAN_SCENARIO_FAILED, "Scan and fail");
//        addBpBtn(Input.CONNECT_BP_MONITOR_ACTION, "connect to BP monitor");
//        addBpBtn(Input.START_BP_MONITOR_ACTION, "start to monitor BP");

        addSep("Use content provider");

        addBtn("insert body scaler data", new View.OnClickListener() {
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                values.put(Database.TIMESTAMP_MS, rndTime(5 * 60 * 1000));
                values.put(Database.USER_ID,"demoUserId");
                values.put(Database.BODY_SCALE_BATTERY_PERCENT_INT, rndInt(60,80));
                values.put(Database.BODY_SCALE_WEIGHT_KG_FLOAT, rndFloat(60,80));
                values.put(Database.BODY_SCALE_FAT_PERCENT_FLOAT, rndFloat(20,30));
                Uri uri = getContentResolver().insert(getUri(Database.BODY_SCALE_TABLE), values);
                Toast.makeText(getBaseContext(), "Data inserted to " + uri.toString(), Toast.LENGTH_LONG).show();
            }


        });

//        addBtn("Get today steps data", new View.OnClickListener() {
//            public void onClick(View view) {
//                Cursor c = getContentResolver().query(getUri(Database.PEDOMETER_TABLE), null, null, null, Database.TIMESTAMP_MS+" DESC");
//                if (!c.moveToFirst())
//                    Toast.makeText(getBaseContext(), "No steps recorded today", Toast.LENGTH_LONG).show();
//                else
//                    Toast.makeText(getBaseContext(), "Today steps count: "+c.getString(c.getColumnIndex(Database.PEDOMETER_STEPS_INT)) , Toast.LENGTH_LONG).show();
//            }
//        });
        addBtn("Delete last blood pressure measure", new View.OnClickListener() {
            public void onClick(View view) {
                Cursor c = getContentResolver().query(getUri(Database.BP_MONITOR_TABLE), null, null, null, Database.TIMESTAMP_MS+" DESC");
                if (!c.moveToFirst()) {
                    Toast.makeText(getBaseContext(), "No blood pressure measure recorded", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    long itemId = c.getLong(c.getColumnIndex(Database.ID));
                    Uri uri = getUri(Database.BP_MONITOR_TABLE + "/" + itemId);
                    int count = getContentResolver().delete(uri, null, null);
                    if (count != 1)
                        Toast.makeText(getBaseContext(), "Measure not deleted, "+uri.toString(), Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(getBaseContext(), "Measure deleted, "+uri.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });
        addBtn("insert Heart rate data", new View.OnClickListener() {
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                values.put(Database.TIMESTAMP_MS, rndTime(5 * 60 * 1000));
                values.put(Database.USER_ID,"demoUserId");
                values.put(Database.BODY_SCALE_BATTERY_PERCENT_INT, rndInt(60,80));
                values.put(Database.BODY_SCALE_WEIGHT_KG_FLOAT, rndFloat(60,80));
                values.put(Database.BODY_SCALE_FAT_PERCENT_FLOAT, rndFloat(20,30));
                Uri uri = getContentResolver().insert(getUri(Database.BODY_SCALE_TABLE), values);
                Toast.makeText(getBaseContext(), "Data inserted to " + uri.toString(), Toast.LENGTH_LONG).show();
            }


        });
        addBtn("insert 20 Heart rate data", new View.OnClickListener() {
            public void onClick(View view) {
                Uri uri = null;
                for (int i= 0; i < 20; i++) {
                    ContentValues values = new ContentValues();
                    values.put(Database.TIMESTAMP_MS, rndTime(5 * 60 * 1000));
                    values.put(Database.USER_ID, "demoUserId");
                    int hr = rndInt(60, 160);
                    values.put(Database.MIO_FUSE_HEART_RATE_INT, hr);
                    uri = getContentResolver().insert(getUri(Database.MIO_FUSE_HEART_RATE_TABLE), values);
                }
                Toast.makeText(getBaseContext(), "Data inserted to " + uri.toString(), Toast.LENGTH_LONG).show();
            }
        });
        addBtn("insert 37° Steps", new View.OnClickListener() {
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                long value = rndTime(15 * 24 * 60 * 60 * 1000);
                String dateStr = new SimpleDateFormat("yyyy/MM/dd").format(new Date(value));
                values.put(Database.TIMESTAMP_MS, value);
                values.put(Database.USER_ID,"demoUserId");
                int steps = rndInt(2000, 4000);
                values.put(Database.MIO_FUSE_STEPS_INT, steps);
                values.put(Database.MIO_FUSE_DATE_STR, dateStr);
                Uri uri = getContentResolver().insert(getUri(Database.MIO_FUSE_DAILY_TABLE), values);
                Toast.makeText(getBaseContext(), "Data inserted to " + uri.toString()+", date: "+dateStr+", Steps: "+steps, Toast.LENGTH_LONG).show();
            }


        });
        addBtn("insert Questionnaire", new View.OnClickListener() {
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                long value = rndTime(15 * 24 * 60 * 60 * 1000);
                values.put(Database.TIMESTAMP_MS, value);
                values.put(Database.USER_ID,"demoUserId");
                String json = "{\"someJsonKey\":\"someJsonValue\"}";
                values.put(Database.QUESTIONNAIRE_JSON, json);
                Uri uri = getContentResolver().insert(getUri(Database.QUESTIONNAIRE_TABLE), values);
                Toast.makeText(getBaseContext(), "Questionnaire inserted to " + uri.toString()+", json: "+json, Toast.LENGTH_LONG).show();
            }
        });
        addBtn("insert Fibrillation", new View.OnClickListener() {
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                long value = rndTime(15 * 24 * 60 * 60 * 1000);
                values.put(Database.TIMESTAMP_MS, value);
                values.put(Database.USER_ID,"8183e284-410b-408b-ae35-95a125df0000");
                String json = "{\"someJsonKey\":\"someJsonValue\"}";
                values.put(Database.FIBRILLATION_JSON, json);
                Uri uri = getContentResolver().insert(getUri(Database.FIBRILLATION_TABLE), values);
                Toast.makeText(getBaseContext(), "Fibrillation inserted to " + uri.toString()+", json: "+json, Toast.LENGTH_LONG).show();
            }
        });
        addBtn("insert Medication", new View.OnClickListener() {
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                long value = rndTime(15 * 24 * 60 * 60 * 1000);
                values.put(Database.TIMESTAMP_MS, value);
                values.put(Database.USER_ID,"8183e284-410b-408b-ae35-95a125df0000");
                String json = "[{\n" +
                        " \"name\":\"someString\",\n" +
                        " \"status\":1,\n" +
                        " \"choiceListId\":\"someString\",\n" +
                        " \"actionPartId\":\"someString\",\n" +
                        " \"actionId\":\"someString\",\n" +
                        " \"medicationTime\":123456789,\n" +
                        " }]";
                values.put(Database.MEDICATION_JSON, json);
                Uri uri = getContentResolver().insert(getUri(Database.MEDICATION_TABLE), values);
                Toast.makeText(getBaseContext(), "Medication inserted to " + uri.toString()+", json: "+json, Toast.LENGTH_LONG).show();
            }
        });
        addBtn("insert Oximeter", new View.OnClickListener() {
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                long value = rndTime(15 * 24 * 60 * 60 * 1000);
                int pso2 = rndInt(80, 100);
                values.put(Database.TIMESTAMP_MS, value);
                values.put(Database.USER_ID,"demoUserId");
                values.put(Database.OXIMETER_PSO2_INT, pso2);
                values.put(Database.SYNC, false);
                Uri uri = getContentResolver().insert(getUri(Database.OXIMETER_TABLE), values);
                Toast.makeText(getBaseContext(), "Oximeter inserted to " + uri.toString()+", pso2: "+pso2, Toast.LENGTH_LONG).show();
            }
        });
    }

    private Uri getUri(String tableName) {
        return new Uri.Builder()
                .scheme(ContentResolver.SCHEME_CONTENT)
                .authority(Database.PROVIDER_NAME)
                .appendPath(tableName)
                .build();
    }

    private void addBtn(String text, View.OnClickListener l) {
        Button insertDemoBtn = new Button(this);
        insertDemoBtn.setText(text);
        verticalLayout.addView(insertDemoBtn, linearParams);
        insertDemoBtn.setOnClickListener(l);
    }

    private void addSep(String text) {
        TextView separatorText = new TextView(this);
        separatorText.setText(text);
        separatorText.setGravity(Gravity.CENTER_HORIZONTAL);
        verticalLayout.addView(separatorText);
    }

    private void addActionBtn(final String action, String title) {
        addBtn(title, new View.OnClickListener() {
            public void onClick(View view) {
                Log.d(TAG, "click action, " + action);
                sendBroadcast(new Intent().setAction(action));
            }
        });
    }

//    private void addBpBtn(final String action, String title) {
//        addBtn(title, new View.OnClickListener() {
//            public void onClick(View view) {
//                sendBroadcast(new Intent().setAction(action)
//                        .putExtra(Input.BP_MONITOR_SCENARIO_BOOLEAN, true));
//            }
//        });
//    }

    private void addStartScanBtn(final int scenario, String title) {
        addStartScanBtn(scenario, title, null);
    }
    private void addStartScanBtn(final int scenario, String title, final Input.DEVICE_TO_CONNECT deviceToConnect) {
        addBtn(title, new View.OnClickListener() {
            public void onClick(View view) {

                sendBroadcast(new Intent().setAction(Input.START_SCAN_ACTION)
                        .putExtra(Input.START_SCAN_DURATION_INT, 20 * 1000)
                        .putExtra(Input.START_SCAN_INTERVALL_INT, 15 * 60 * 1000)
                        .putExtra(Input.START_SCAN_USER_ID_STR, Database.DEMO_USER_ID_VALUE)
                        .putExtra(Input.START_SCAN_USER_ID_STR, Database.DEMO_USER_ID_VALUE)
                        .putExtra(Input.START_SCAN_CONNECTION_CHOICE, deviceToConnect));
            }
        });
    }

    @Override
    public void sendBroadcast(Intent intent) {
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        super.sendBroadcast(intent);
    }

    private long rndTime(long maxDelay) {
        return System.currentTimeMillis() - rndLong(maxDelay);
    }

    private float rndFloat(int min, int max) {
        return (float) (min + Math.random() * (max - min));
    }

    private int rndInt(int min, int max) {
        return (int) (min + Math.random() * (max - min));
    }

    private long rndLong(long maxDelay) {
        return (long) (Math.random() * maxDelay);
    }

}
