package com.smartbeat.vigisense.android.model;

import android.util.Log;

import com.google.api.client.json.GenericJson;

import java.lang.reflect.Field;
import java.util.Arrays;

public abstract class MyBean {
    public abstract Long get_id() ;
    public abstract Boolean getSync();
    public abstract String getUserId();
    public abstract void set_id(Long id);
    public abstract void setSync(Boolean sync);
    public abstract void setUserId(String userId);

//    com.smartbeat.vigisense:sync E/com.smartbeat.vigisense.android.core.MyBean: com.smartbeat.vigisense.android.model.Fibrillation, [java.lang.Long com.smartbeat.vigisense.android.model.Fibrillation._id, java.lang.String com.smartbeat.vigisense.android.model.Fibrillation.json, java.lang.Boolean com.smartbeat.vigisense.android.model.Fibrillation.sync, java.lang.Long com.smartbeat.vigisense.android.model.Fibrillation.timestampMs, java.lang.String com.smartbeat.vigisense.android.model.Fibrillation.userId]
//    com.smartbeat.vigisense:sync E/com.smartbeat.vigisense.android.core.MyBean: com.smartbeat.vigisense.android.model.Fibrillation, [public java.lang.Long com.smartbeat.vigisense.android.model.Fibrillation._id, public java.lang.String com.smartbeat.vigisense.android.model.Fibrillation.json, public java.lang.Boolean com.smartbeat.vigisense.android.model.Fibrillation.sync, public java.lang.Long com.smartbeat.vigisense.android.model.Fibrillation.timestampMs, public java.lang.String com.smartbeat.vigisense.android.model.Fibrillation.userId, public static transient volatile com.android.tools.fd.runtime.IncrementalChange com.smartbeat.vigisense.android.model.Fibrillation.$change, public static final long com.smartbeat.vigisense.android.model.Fibrillation.serialVersionUID]
// OK

    // getField(name) => getDeclaredField(name)

    public <T extends GenericJson> T toApiObj(T serverBean) throws NoSuchFieldException, IllegalAccessException {
        for (Field serverField : serverBean.getClass().getDeclaredFields()){
            String name = serverField.getName();
            if (name.equals("id"))
                continue;
            Class<? extends MyBean> aClass = getClass();
            //getClass() should return : com.smartbeat.vigisense.android.model.Fibrillation
            Log.e(MyBean.class.getName(), aClass.getName() +", "+ Arrays.toString(aClass.getDeclaredFields()));
            //java.lang.String com.smartbeat.vigisense.android.model.Fibrillation.json
            Object androidFieldValue = aClass.getDeclaredField(name).get(this);
            serverBean.set(name,androidFieldValue);
        }
        return serverBean;
    }
}


//public abstract class MyBean { //com.smartbeat.vigisense.android.model.Fibrillation extends this class
//    public <T extends GenericJson> T toApiObj(T serverBean) throws NoSuchFieldException, IllegalAccessException {
//        for (Field serverField : serverBean.getClass().getDeclaredFields()) {
//            String name = serverField.getName();
//            if (name.equals("id"))
//                continue;
//            //getClass() should return : com.smartbeat.vigisense.android.model.Fibrillation
//            // But my guess for some JVM that's return  com.smartbeat.vigisense.android.core.MyBean
//            Object androidFieldValue = getClass().getField(name).get(this);
//            serverBean.set(name, androidFieldValue);
//        }
//        return serverBean;
//    }
//}