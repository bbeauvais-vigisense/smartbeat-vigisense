package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.smartbeat.vigisense.android.model.BpMonitor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.UUID;

public class BpMonitorConnector {
    private static final String TAG = BpMonitorConnector.class.getName();
    public static final String write_1101 = "00001101-0000-1000-8000-00805f9b34fb";
    private static BluetoothSocket writeSocket;
    private final MyContext ctx;
    private BluetoothDevice device;
    private boolean paired;
    private InputStream inputWrite;
    private boolean connected;
    protected StringBuffer currentTrame = new StringBuffer();
    private boolean measureDone;

    public BpMonitorConnector(MyContext myContext) {
        ctx = myContext;
    }



    private void onError(String message) {
        onDestroy();
        ctx.getLogicOutput().bpMonitorOnError(message);
    }

    public void onBleScan(BluetoothDevice btDevice) {
        try {
            connect();
        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }
//        0 0xfffd //start trame
//        4 0xfffd //start trame
//        8 0xfffd  //start trame
//        12 0x78 => Sys
//        14 0x55 => Dia //not present at starting
//        16 0x52 => BP //not present at starting
//        18 0x0d  //end trame
//        20 0x0a  //end trame
//        use last trame, 5 seconde sans trame == fin
//        processus dure 1min

    private void receive(final String pTrame) throws Exception {
        Log.d(TAG, "trame: " + pTrame + ", lenght: " + pTrame.length());
        String data = pTrame.replace("fffd", "").replace("0d0a", "");
        if (measureDone) {
            Log.d(TAG, "measure already done");
            return;
        }
        //fffdfffdfffd00fffd0d0a inflation
        if (pTrame.equals("fffdfffdfffd00010d0a")) {
            Log.v(TAG, "inflation...");
            return;
        }
        /**
         * FHP changed this
         */
        //fffdfffdfffd010d0a inflation FHP remove this, it showed error events when effectively the error did not occur
//        if (pTrame.length() == 18 && data.length() == 2) {
//            measureDone = true;
//            Log.e(TAG, "measure error, code: " + toInt(data, 0));
//            ctx.getLogicOutput().bpMonitorOnError("measure error, code: " + toInt(data, 0));
//            onDestroy();
//            return;
//        }
        if (pTrame.length() == 22 && data.length() == 6) {
            measureDone = true;
            final int systol = toInt(data, 0);
            final int diastol = toInt(data, 2);
            final int heartRate = toInt(data, 4);
            Log.i(TAG, "systol: " + systol + ", diastol: " + diastol + ", heartRate: " + heartRate);
            BpMonitor bpMonitor = new BpMonitor();
            bpMonitor.setUserId(ctx.getUserId());
            bpMonitor.setTimestampMs(System.currentTimeMillis());
            bpMonitor.setSystoleInt(systol);
            bpMonitor.setDiastoleInt(diastol);
            bpMonitor.setHeartRateInt(heartRate);
            ctx.getLogicOutput().bpMonitorNewData(bpMonitor);
            onDestroy();
        }
    }

    public void startMeasure() throws Exception {
        try {
            if (!connected || writeSocket == null || !writeSocket.isConnected()) {
                Log.e(TAG,"device not connected, connected: "+connected+", writeSocket: "+writeSocket+", "+(writeSocket == null?"":""+writeSocket.isConnected()));
                onError("device not connected");
            }
            measureDone = false;
            writeSocket.getOutputStream().write(new byte[]{(byte) -3, (byte) -3, (byte) -6, (byte) 5, (byte) 13, (byte) 10});
            Log.d(TAG, "inputWrite start");
            BufferedReader r = new BufferedReader(new InputStreamReader(inputWrite));
            int next;
            while ((next = r.read()) != -1) {
                String hexStr = String.format("%02x", next);
                Log.v(TAG, "inputWrite " + hexStr);
                currentTrame.append(hexStr);
                int endIndex = currentTrame.lastIndexOf("0d0a");
                Log.v(TAG, "endIndex: " + endIndex + ", currentTrame " + currentTrame);
                if (endIndex != -1) {
                    Log.d(TAG, "SEND TO RECEIVE");
                    String trame = currentTrame.substring(0, endIndex + 4);
                    receive(trame);
                    currentTrame.delete(0, endIndex + 4);
                }
            }
            connected = false;
            ctx.getLogicOutput().bpMonitorDisconnected();
            onDestroy();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "IOException: " + e.getMessage());
            onError("SOCKET ERROR");
        }
    }

    private int toInt(String trame, int i) {
        return Integer.parseInt(trame.substring(i, i + 2), 16);
    }

    public void connect() {
        try {
            paired = false;
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    Log.d(TAG, "paired: " + device.getName() + ", " + device.getAddress());
                    if (device.getName().equals("Bluetooth BP")) {
                        Log.i(TAG, "connecting: " + device.getName() + ", " + device.getAddress());
                        BpMonitorConnector.this.device = device;
                        paired = true;
                        ConnectThread();
                    }
                }
            }
            if (!paired)
                onError("device not paired or bluetooth off");
        } catch (IOException e) {
            Log.v(TAG,"IOException!!");
            e.printStackTrace();
            onError(e.getMessage());
            connected = false;
        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
            connected = false;
        }

    }

    public void ConnectThread() throws IOException {
        if (connected) {
            Log.v(TAG, "already connected, connected: " + connected);
            return;
        }
        Log.i(TAG, "connecting... " + device.getName() + ", " + device.getAddress());

        byte[] pin = "0000".getBytes();
        device.setPin(pin);
        Log.i(TAG, "set pin: " + new String(pin));
        if (writeSocket != null)
            writeSocket.close();
        if (inputWrite != null)
            inputWrite.close();
        writeSocket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString(write_1101));
        writeSocket.connect();
        Log.i(TAG, "Write Connected: " + writeSocket.isConnected() + "");
        inputWrite = writeSocket.getInputStream();
        connected = true;
        currentTrame.setLength(0);
        ctx.getLogicOutput().bpMonitorConnected();
    }


    public void onDestroy() {
        try {
            connected = false;
            if (writeSocket != null)
                writeSocket.close();
            if (inputWrite != null)
                inputWrite.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isBluetoothEnabled() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            onError("bluetooth not supported");
            return false;
        }
        if (mBluetoothAdapter.isEnabled())
            return true;
        else
            onError("bluetooth not enabled");
        return false;
    }

}
