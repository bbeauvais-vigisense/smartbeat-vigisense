package com.smartbeat.vigisense.android.core;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MySyncService extends Service {
    private static final String TAG = MySyncService.class.getName();
    private static MySyncAdapter sSyncAdapter = null;
    private static final Object sSyncAdapterLock = new Object();
    @Override
    public void onCreate() {
        Log.v(TAG,"onCreate");
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = new MySyncAdapter(getApplicationContext(), true);
            }
        }
    }
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG,"onBind");
        return sSyncAdapter.getSyncAdapterBinder();
    }
}
