package com.smartbeat.vigisense.android.core;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.j256.ormlite.dao.CloseableIterator;
import com.smartbeat.vigisense.android.api.Database;
import com.smartbeat.vigisense.android.model.MioFuseActivity;
import com.smartbeat.vigisense.android.model.MioFuseBattery;
import com.smartbeat.vigisense.android.model.MioFuseDailyActivity;
import com.smartbeat.vigisense.android.model.MioFuseHeartRate;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDay;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDelta;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDeltaDay;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateDeltaHour;
import com.smartbeat.vigisense.android.model.MioFuseHeartRateHour;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class _37Connector extends MyGattCallback implements Runnable {

    public static final boolean DELTA_ENABLED = true;

    public static final String write37 = "90000002-2203-4011-9c85-b0b619f5e668";
    public static final String notify37 = "90000003-2203-4011-9c85-b0b619f5e668"; //data flux

    private static final long HOUR = 60 * 60 * 1000;
    private static final long DAY = 24 * 60 * 60 * 1000;
    private static final String LAST_DAILY_KEY = "MioFuseDailyActivity";
    private static final String TAG = _37Connector.class.getName();
    public static final long MEASUREMENT_INTERVAL = 15000L;
    private final MyContext ctx;
    private final MyBluetoothGatt myGatt;
    private BluetoothDevice device;
    private MyLooper looper;
    private Long lastSync = null;
    private String lastDate;
    private Long lastHourlyAggregation;
    private Long lastDayAggregation;
    private Long lastHeartRateDelta;
    private StringBuffer memBuffer;
    private ArrayList<String> lineAr = new ArrayList<>();
    private String memCrurrent;
    private Long lastActivityEndDayAggregation;


    public _37Connector(MyContext myContext) {
        ctx = myContext;
        myGatt = new MyBluetoothGatt(ctx, this);
        if (lastDate == null)
            lastDate = ctx.getString(LAST_DAILY_KEY, "0");
    }

    public static String toHexStr(byte[] value) {
        String hexStr = "";
        for (byte b : value)
            hexStr += String.format("%02x", b);
        return hexStr;
    }

    public void onScan(BluetoothDevice btDevice) {
        try {
            Log.e(TAG, "connecting");
            device = btDevice;
            if (!myGatt.isConnected()) {
                myGatt.connect(device);
            } else {
                Log.w(TAG, "Already connected");
            }

        } catch (Exception e) {
            e.printStackTrace();
            onError(e.getMessage());
        }
    }

    public void onDestroy() {
        if (myGatt != null)
            myGatt.onDestroy();
        if (looper != null)
            looper.cancel();
    }

    public void onConnected() {
        Log.e(TAG, "onConnected");

        myGatt.notification(notify37);
        myGatt.write(write37, "18:01:d7:c0:13:13:c4:25:8c:cd");
        long now = System.currentTimeMillis();
        String s = Long.toHexString(now / 1000);
        Log.e(TAG, "timestamp second, hex: " + s + " dec: " + now + ", " + new SimpleDateFormat().format(new Date(now)));
        myGatt.write(write37, "09:04:00:00:00:00:" + s); //heure ?
        myGatt.write(write37, "0c:23:55:55:55:55:55:55:00:00:02:00:04:00");
        myGatt.write(write37, "0d:31:31:39:39:30:31:38:30:30:38:30:30");

        myGatt.write(write37, "0c:02:05"); //battery
        myGatt.write(write37, "0c:04:01");
        myGatt.write(write37, "0c:04:02");
        myGatt.write(write37, "0c:02:01");
        myGatt.write(write37, "0c:10:0a");

        myGatt.nextCmd();
        if (looper != null)
            looper.cancel();
        looper = new MyLooper(20000, 20000, this);
        ctx.getLogicOutput().mioFuseConnecting();
    }

    //SYNC every 10 MIN or When available

    @Override
    public void run() {
        Log.d(TAG, "loop");
        if (lastSyncIsTooOld()) {
            myGatt.write(write37, "0a:02:38:31:36:43:32:36:30:34:31:39:30:00:00:00:00:00:00:00");
            myGatt.write(write37, "0d:31:31:39:39:30:31:38:30:30:38:30:30");
            Log.d(TAG, "RESET");
//            myGatt.write(write37,  "0c:22:00");
            myGatt.write(write37,  "0c:10:0b");
            myGatt.write(write37, "0c:22:01");

            myGatt.nextCmd();
        }
    }

    private boolean lastSyncIsTooOld() {
        return lastSync == null || lastSync < System.currentTimeMillis() - 10 * HOUR;
    }

    public void onCharacteristicChanged(BluetoothGattCharacteristic chr) throws Exception {
        byte[] bytes = chr.getValue();
        String str = BleUtil.toHexStr(bytes);
//        Log.i(TAG, "onCharacteristicChanged: " + str);

        long now = System.currentTimeMillis();

        //06 00 1e 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
        if (str.startsWith("0600")) {
            Integer lastBateryPercent = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
            if (lastBateryPercent == null)
                return;
            Log.e(TAG, "battery: " + lastBateryPercent + "%");
            MioFuseBattery battery = new MioFuseBattery();
            battery.setUserId(ctx.getUserId());
            battery.setBatteryPercentInt(lastBateryPercent);
            battery.setTimestampMs(System.currentTimeMillis());
            ctx.getLogicOutput().mioFuseBatteryCharge(battery);
        }

        //17 04 58 69 3f 6c 53 01130000000000000000000000
        if (str.startsWith("1704")) {
            Integer hr = chr.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 6);
            Long timeMs = Long.parseLong(str.substring(4, 12), 16) * 1000;

            Log.e(TAG, "HR: " + hr + "bpm, " + new SimpleDateFormat().format(new Date(timeMs)) + " => " + BleUtil.val(chr));
            if (hr == null) {
                return;
            }
            MioFuseHeartRate hrBean = new MioFuseHeartRate();
            hrBean.setUserId(ctx.getUserId());
            hrBean.setHeartRateInt(hr);
            hrBean.setTimestampMs(timeMs);
            onNewHr(timeMs, hr);
//            ctx.getLogicOutput().mioFuseHeartRateNoSave(hrBean);
        }

        if (str.startsWith("0104")) {
//			log(timeStr);
            long stepsTime = Long.parseLong(str.substring(4, 12), 16) * 1000;
            String dateStr = new SimpleDateFormat().format(new Date(stepsTime ));

            int header1 = Integer.parseInt(str.substring(12, 14), 16);
            int header2 = Integer.parseInt(str.substring(14, 16), 16);
            int unknow = Integer.parseInt(str.substring(16, 18), 16);
            int steps = Integer.parseInt(str.substring(18, 20), 16);

            Log.e(TAG, "steps: " + dateStr + ", " + steps + "steps " + header1 + ", " + header2 + ", " + unknow + ", " + steps + ", ");

            if (((header1 > 0 && header1 < 4) || (header2 > 0 && header2 < 4)) && steps > 1 && steps != 255) {
                MioFuseActivity step = new MioFuseActivity();
                step.setTimestampMs(stepsTime);
                step.setUserId(ctx.getUserId());
                step.setStepsInt(steps);
                ctx.getLogicOutput().mioFuseActivity(step);
                aggregateActivityDay(stepsTime);
            }
            return;
        }
        if (str.startsWith("0f0a")) {
//            Log.e(TAG, "Endmem: " + str);
            handleMemory();
            return;
        }
        if (str.startsWith("12")) {
            String s1 = BleUtil.valStr(chr);
//            Log.e(TAG, "mem: " + s1);
            memCrurrent += str.substring(2);
            int indexEnd = memCrurrent.indexOf("0a");
            if (indexEnd > -1) {
                String line = memCrurrent.substring(0, indexEnd);
//                Log.e(TAG, "line: " + line);
                lineAr.add(line);
                memCrurrent = memCrurrent.substring(indexEnd + 2, memCrurrent.length());
            }
            return;
        }
    }

    private void aggregateActivityDay(long stepsTime) throws Exception {
//        Log.e(TAG, "aggregateActivityDay, stepsTime:" + stepsTime);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(stepsTime);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        int nowDay = cal.get(Calendar.DAY_OF_MONTH);
        long nowDayTimestamp = cal.getTimeInMillis();

//        Log.e(TAG, "aggregateActivityDay, nowDayTimestamp:" + nowDayTimestamp);

        if (lastActivityEndDayAggregation == null) {
//            Log.e(TAG, "aggregateActivityDay, lastActivityEndDayAggregation, lastActivityEndDayAggregation == null");
            lastActivityEndDayAggregation = ctx.getLong(MyContext.lastEndActivityDayAggregation, 0L);
            if (lastActivityEndDayAggregation == 0L) {
//                Log.e(TAG, "aggregateActivityDay, lastActivityEndDayAggregation == "+lastActivityEndDayAggregation);
                lastActivityEndDayAggregation = stepsTime;
                ctx.getEdit().putLong(MyContext.lastEndActivityDayAggregation, stepsTime).commit();
            }
        }
//        Log.e(TAG, "aggregateActivityDay, lastActivityEndDayAggregation: "+lastActivityEndDayAggregation);
        cal.setTimeInMillis(lastActivityEndDayAggregation);
        int lastDay = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        long lastDayTimestamp = cal.getTimeInMillis();
//        Log.e(TAG, "aggregateActivityDay, nowDay:"+nowDay+", != lastDay: "+lastDay);
        if (nowDay != lastDay) {
            Log.e(TAG, "aggregateActivityDay, nowDay != lastDay");
            aggregateLastActivityDays(lastDayTimestamp, nowDayTimestamp);
            lastActivityEndDayAggregation = stepsTime;
            ctx.getEdit().putLong(MyContext.lastEndActivityDayAggregation, stepsTime).commit();
        }
    }

    private void aggregateLastActivityDays(final long lastDayTimestamp, final long nowDayTimestamp) {
        new MyTimer(new Runnable() {
            public void run() {//
                for (long currentEndDay = lastDayTimestamp + DAY; currentEndDay <= nowDayTimestamp; currentEndDay += DAY)
                    aggregateActivityOneDay(currentEndDay);
            }
        });
    }


    private void aggregateActivityOneDay(final long endDayTimestamp) {
        new MyTimer(new Runnable() {
            @Override
            public void run() {
                CloseableIterator<MioFuseActivity> iterator = null;
                try {
                    Log.e(TAG, "aggregateActivityOneDay,  from: " +(endDayTimestamp - DAY)+", to: "+endDayTimestamp);
                    iterator = ctx.getDb().dao(MioFuseActivity.class).queryBuilder().where().gt(Database.TIMESTAMP_MS, endDayTimestamp - DAY).and().lt(Database.TIMESTAMP_MS, endDayTimestamp).iterator();
                    int stepTotal = 0;
                    while (iterator.hasNext()) {
                        MioFuseActivity next = iterator.next();
                        Log.e(TAG, "aggregateActivityDay,  next.getStepsInt(): " + next.getStepsInt());
                        stepTotal += next.getStepsInt();
                    }
                    SimpleDateFormat yyyMMdd = new SimpleDateFormat("yyyy/MM/dd");
                    MioFuseDailyActivity daily = new MioFuseDailyActivity();
                    daily.setTimestampMs(endDayTimestamp);
                    daily.setDateStr(yyyMMdd.format(new Date(endDayTimestamp - DAY)));
                    daily.setUserId(ctx.getUserId());
                    daily.setStepsInt(stepTotal);
                    Log.e(TAG, "aggregateActivityDay, stepTotal:" + stepTotal + "," + daily);
                    ctx.getLogicOutput().mioFuseDailyActivity(daily);
                } catch (Exception e) {
                    Log.e(TAG, "aggregateLastDay: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }
    private void handleMemory() {
        Log.e(TAG, "lineAr size " + lineAr.size());
        ArrayList<String> handledLineAr = lineAr;
        lineAr = new ArrayList<>();
        ArrayList<String> readable = new ArrayList<>();
        for (String lineHex : handledLineAr) {
            try {
                String line = new String(Hex.decodeHex(lineHex.toCharArray()));
                if (line.contains("xx")) {
//                    Log.e(TAG, "header " + line);
                    continue;
                }
                String title = line.substring(0, line.indexOf(":"));
                String readableLine = title + ",";
                String values = line.substring(line.indexOf(":") + 1);
                String[] cels = values.split(",");
                int celCount = 0;
                String time = "";
                Long timstampMs = null;
                Integer lastHr = null;

                for (String cel : cels) {
                    celCount++;
                    if (cel.length() == 8) {
                        timstampMs = Long.parseLong(cel, 16) * 1000;
                        time = new SimpleDateFormat().format(new Date(timstampMs ));
                        if (lastHr != null) {
                            onNewHr(timstampMs, lastHr);
                            lastHr = null;
                        }
                        continue;
                    }
                    if (title.equals("0C")) {
                        int valInt = Integer.parseInt(cel, 16);
                        if (celCount == 1) {
                            readableLine += valInt + "bpm,";
                            lastHr = valInt;
                            continue;
                        }
                    }
                    if (title.equals("02")) {
                        //5 => pas de valeur
                        int valInt = Integer.parseInt(cel, 16);
                        if (valInt == 5) {
                            readableLine += valInt + "ND,";
                            continue;
                        }
                        if (celCount == 1) {
                            readableLine += valInt + "Hg,";
                            continue;
                        }
                        if (celCount == 2) {
                            readableLine += valInt + "/,";
                            continue;
                        }
                        if (celCount == 3) {
                            readableLine += valInt + "resp/sec,";
                            continue;
                        }
                        if (celCount == 4) {
                            readableLine += valInt + "bpm,";
                            lastHr = valInt;
                            continue;
                        }
                        if (celCount == 5) {
                            readableLine += valInt + "?,";
                            continue;
                        }

                    }
                    readableLine += cel + ",";
                }
//                readable.add(time +","+readableLine+line.replace(":",","));
                readable.add(time + "," + readableLine);
            } catch (Exception e) {
                Log.e(TAG,e.getMessage());
                e.printStackTrace();
            }
        }
        Log.i(TAG, "readable: " + readable.size());
        logArr(readable);
//        Health_Unup:d380a14     12 48 65 61 6c 74 68 5f 55 6e 75 70 3a 64 33 38 30 61 31 34
//        5xxxxxxxxxxxxxxxxx      12 35 00 78 78 78 78 78 78 78 78 78 78 78 78 78 78 78 78 78
//        xx02:5,5,5,5,5,4,59     12 78 78 30 32 3a 35 2c 35 2c 35 2c 35 2c 35 2c 34 2c 35 39

        //02:86,5e,d,5b,c,4,598db880,2,13=>134(bp),94(bp),13,91(HR),12,4
        //12 32 2c 31 33 0a
        //0f 0a 00 00 2c 79

    }


    private void logArr(ArrayList<String> arr) {
        for (String line : arr) {
            Log.e(TAG, ">" + line);
        }
    }

    public void onNewHr(long hrTime, Integer hr) throws Exception {
        if (hr < 40 && hr > 200) {
            Log.v(TAG, "Wrong hr " + hr);
            return;
        }
        if (hrTime < 1483967716000l || hrTime > 1578575716000l) {
            Log.v(TAG, "Wrong timestamp " + hrTime);
            return;
        }
        MioFuseHeartRate hrBean = new MioFuseHeartRate();
        hrBean.setUserId(ctx.getUserId());
        hrBean.setHeartRateInt(hr);
        hrBean.setTimestampMs(hrTime);

        if (DELTA_ENABLED) {
            if (lastHeartRateDelta != null && hrTime - lastHeartRateDelta < 5000L) {
                MioFuseHeartRateDelta mioFuseHeartRateDelta = new MioFuseHeartRateDelta();
                mioFuseHeartRateDelta.setUserId(ctx.getUserId());
                mioFuseHeartRateDelta.setDelta(hrTime - lastHeartRateDelta);
                mioFuseHeartRateDelta.setTimestampMs(hrTime);
                ctx.getLogicOutput().mioFuseHeartRateDelta(mioFuseHeartRateDelta);
            }
            lastHeartRateDelta = hrTime;
        }
        Log.i(TAG, "HEART_RATE save... "+hrBean);
        ctx.getLogicOutput().mioFuseHeartRateMem(hrBean);
        aggregateIfNeed(hrTime);
        return;
    }

    private synchronized void aggregateIfNeed(Long lastHr) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(lastHr);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        int nowHour = cal.get(Calendar.HOUR_OF_DAY);
        long nowHourTimestamp = cal.getTimeInMillis();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.setTimeInMillis(lastHr);
        int nowDay = cal.get(Calendar.DAY_OF_MONTH);
        long nowDayTimestamp = cal.getTimeInMillis();

        if (lastHourlyAggregation == null) {
            lastHourlyAggregation = ctx.getLong(MyContext.lastHourlyAggregation, 0L);
            if (lastHourlyAggregation == 0L) {
                lastHourlyAggregation = lastHr;
                ctx.getEdit().putLong(MyContext.lastHourlyAggregation, lastHr).commit();
            }
        }

        cal.setTimeInMillis(lastHourlyAggregation);
        int lastHour = cal.get(Calendar.HOUR_OF_DAY);

        Log.i(TAG, "lastHourlyAggregation: " + lastHour + ", nowHour: " + nowHour);
        if (lastHour != nowHour) {
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            long lastHourTimestamp = cal.getTimeInMillis();
            aggregateLastHours(lastHourTimestamp, nowHourTimestamp);
            ctx.getEdit().putLong(MyContext.lastHourlyAggregation, lastHr).commit();
            lastHourlyAggregation = lastHr;
        }


        if (lastDayAggregation == null) {
            lastDayAggregation = ctx.getLong(MyContext.lastDayAggregation, 0L);
            if (lastDayAggregation == 0L) {
                lastDayAggregation = lastHr;
                ctx.getEdit().putLong(MyContext.lastDayAggregation, lastHr).commit();
            }
        }

        cal.setTimeInMillis(lastDayAggregation);
        int lastDay = cal.get(Calendar.DAY_OF_MONTH);

        Log.i(TAG, "lastDayAggregation: " + lastDay + ", nowDay: " + nowDay);
        if (lastDay != nowDay) {
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            long lastDayTimestamp = cal.getTimeInMillis();
            aggregateLastDays(lastDayTimestamp, nowDayTimestamp);
            lastDayAggregation = lastHr;
            ctx.getEdit().putLong(MyContext.lastDayAggregation, lastHr).commit();
        }
    }

    private void aggregateLastDays(final long lastDayTimestamp, final long nowDayTimestamp) {
        new MyTimer(new Runnable() {
            public void run() {
                for (long currentEndDay = lastDayTimestamp + DAY; currentEndDay <= nowDayTimestamp; currentEndDay += DAY)
                    aggregateDay(currentEndDay);
            }
        });
    }


    private void aggregateDay(final long endDayTimestamp) {
        new MyTimer(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.i(TAG, "aggregateLastDay from " + (endDayTimestamp - DAY));
                    CloseableIterator<MioFuseHeartRate> iterator = null;
                    try {
                        iterator = ctx.getDb().dao(MioFuseHeartRate.class).queryBuilder().where().gt(Database.TIMESTAMP_MS, endDayTimestamp - DAY).and().lt(Database.TIMESTAMP_MS, endDayTimestamp).iterator();
                        DescriptiveStatistics stats = new DescriptiveStatistics();
                        while (iterator.hasNext())
                            stats.addValue(iterator.next().getHeartRateInt());
                        MioFuseHeartRateDay day = new MioFuseHeartRateDay();
                        day.setUserId(ctx.getUserId());
                        day.setSync(false);
                        day.setTimestampMs(endDayTimestamp);
                        day.setMin((int) stats.getMin());
                        day.setFirstDecile((int) stats.getPercentile(10));
                        day.setMedian((int) stats.getPercentile(50));
                        day.setMean((int) stats.getMean());
                        day.setLastDecile((int) stats.getPercentile(90));
                        day.setMax((int) stats.getMax());
                        day.setStandardDeviation((int) stats.getStandardDeviation());
                        day.setSampleSize(stats.getN());
                        day.setFirstQuartile((int) stats.getPercentile(25));
                        day.setLastQuartile((int) stats.getPercentile(75));
                        ctx.getLogicOutput().mioFuseHeartRateDay(day);
                    } finally {
                        if (iterator != null)
                            iterator.close();
                    }

                    if (DELTA_ENABLED) {
                        Log.i(TAG, "aggregateLastDay MioFuseHeartRateDeltaDay...");
                        CloseableIterator<MioFuseHeartRateDelta> iteratorDelta = null;
                        try {
                            iteratorDelta = ctx.getDb().dao(MioFuseHeartRateDelta.class).queryBuilder().where().gt(Database.TIMESTAMP_MS, endDayTimestamp - DAY).and().lt(Database.TIMESTAMP_MS, endDayTimestamp).iterator();
                            DescriptiveStatistics stats = new DescriptiveStatistics();
                            while (iteratorDelta.hasNext())
                                stats.addValue(iteratorDelta.next().getDelta());
                            Log.i(TAG, "aggregateLastDay done, n: " + stats.getN());
                            MioFuseHeartRateDeltaDay day = new MioFuseHeartRateDeltaDay();
                            day.setUserId(ctx.getUserId());
                            day.setSync(false);
                            day.setTimestampMs(endDayTimestamp);
                            day.setMin((int) stats.getMin());
                            day.setFirstDecile((int) stats.getPercentile(10));
                            day.setMedian((int) stats.getPercentile(50));
                            day.setMean((int) stats.getMean());
                            day.setLastDecile((int) stats.getPercentile(90));
                            day.setMax((int) stats.getMax());
                            day.setStandardDeviation((int) stats.getStandardDeviation());
                            day.setSampleSize(stats.getN());
                            day.setFirstQuartile((int) stats.getPercentile(25));
                            day.setLastQuartile((int) stats.getPercentile(75));
                            Log.i(TAG, "aggregateLastDay " + (day));
                            ctx.getLogicOutput().mioFuseHeartRateDeltaDay(day);
                        } finally {
                            if (iteratorDelta != null)
                                iteratorDelta.close();
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "aggregateLastDay: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private void aggregateLastHours(final long lastHourTimestamp, final long nowHourTimestamp) {
        Log.i(TAG, "aggregateLastHours from " + lastHourTimestamp + " to " + nowHourTimestamp + "...");
        new MyTimer(new Runnable() {
            public void run() {
                Log.i(TAG, "aggregateLastHours from " + lastHourTimestamp + " to " + nowHourTimestamp + " !");
                for (long currentEndHour = lastHourTimestamp + HOUR; currentEndHour <= nowHourTimestamp; currentEndHour += HOUR)
                    aggregateHour(currentEndHour);
            }
        });
    }

    private void aggregateHour(long endHourTimestamp) {
        try {
            Log.i(TAG, "aggregateLastHours from " + (endHourTimestamp - HOUR));
            Log.i(TAG, "aggregateLastHours MioFuseHeartRateHour...");
            CloseableIterator<MioFuseHeartRate> iterator = null;
            try {
                iterator = ctx.getDb().dao(MioFuseHeartRate.class).queryBuilder().where().gt(Database.TIMESTAMP_MS, endHourTimestamp - HOUR).and().lt(Database.TIMESTAMP_MS, endHourTimestamp).iterator();
                DescriptiveStatistics stats = new DescriptiveStatistics();
                while (iterator.hasNext())
                    stats.addValue(iterator.next().getHeartRateInt());
                Log.i(TAG, "aggregateLastHours done, n: " + stats.getN());
                MioFuseHeartRateHour hour = new MioFuseHeartRateHour();
                hour.setUserId(ctx.getUserId());
                hour.setSync(false);
                hour.setTimestampMs(endHourTimestamp);
                hour.setMin((int) stats.getMin());
                hour.setFirstDecile((int) stats.getPercentile(10));
                hour.setMedian((int) stats.getPercentile(50));
                hour.setMean((int) stats.getMean());
                hour.setLastDecile((int) stats.getPercentile(90));
                hour.setMax((int) stats.getMax());
                hour.setStandardDeviation((int) stats.getStandardDeviation());
                hour.setSampleSize(stats.getN());
                hour.setFirstQuartile((int) stats.getPercentile(25));
                hour.setLastQuartile((int) stats.getPercentile(75));
                Log.i(TAG, "aggregateLastHours " + (hour));
                ctx.getLogicOutput().mioFuseHeartRateHour(hour);
            } finally {
                if (iterator != null)
                    iterator.close();
            }
            if (DELTA_ENABLED) {
                Log.i(TAG, "aggregateLastHours MioFuseHeartRateDeltaHour...");
                CloseableIterator<MioFuseHeartRateDelta> iteratorDelta = null;
                try {
                    iteratorDelta = ctx.getDb().dao(MioFuseHeartRateDelta.class).queryBuilder().where().gt(Database.TIMESTAMP_MS, endHourTimestamp - HOUR).and().lt(Database.TIMESTAMP_MS, endHourTimestamp).iterator();
                    DescriptiveStatistics stats = new DescriptiveStatistics();
                    while (iteratorDelta.hasNext())
                        stats.addValue(iteratorDelta.next().getDelta());
                    Log.i(TAG, "aggregateLastHours done, n: " + stats.getN());
                    MioFuseHeartRateDeltaHour hour = new MioFuseHeartRateDeltaHour();
                    hour.setUserId(ctx.getUserId());
                    hour.setSync(false);
                    hour.setTimestampMs(endHourTimestamp);
                    hour.setMin((int) stats.getMin());
                    hour.setFirstDecile((int) stats.getPercentile(10));
                    hour.setMedian((int) stats.getPercentile(50));
                    hour.setMean((int) stats.getMean());
                    hour.setLastDecile((int) stats.getPercentile(90));
                    hour.setMax((int) stats.getMax());
                    hour.setStandardDeviation((int) stats.getStandardDeviation());
                    hour.setSampleSize(stats.getN());
                    hour.setFirstQuartile((int) stats.getPercentile(25));
                    hour.setLastQuartile((int) stats.getPercentile(75));
                    Log.i(TAG, "aggregateLastHours " + (hour));
                    ctx.getLogicOutput().mioFuseHeartRateDeltaHour(hour);
                } finally {
                    if (iteratorDelta != null)
                        iteratorDelta.close();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "aggregateLastDay: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void onCharacteristicRead(BluetoothGattCharacteristic characteristic) {
    }

    public void onDisconnected() {
        if (looper != null)
            looper.cancel();
        ctx.getLogicOutput().mioFuseDisconnected();
    }

    public void onError(String message) {
        onDestroy();
        ctx.getLogicOutput().mioFuseOnError(message);
    }

}
