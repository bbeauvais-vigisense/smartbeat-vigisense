package com.smartbeat.vigisense.android.api;

public class Database {
    public static final String PROVIDER_NAME = "com.smartbeat.provider.Vigisense";
    public static final String DATABASE_NAME = "vigisenseDatabase";
    public static final int DATABASE_VERSION = 1;
    public static final String ID ="_id" ;
    public static final String USER_ID ="userId" ;
    public static final String DEMO_USER_ID_VALUE = "demoUserId";
    public static final String SYNC ="sync" ;
    public static final String TIMESTAMP_MS ="timestampMs" ;

    public static final String BODY_SCALE_TABLE = "BodyScale";
    public static final String BODY_SCALE_BATTERY_PERCENT_INT = "batteryPercentInt";
    public static final String BODY_SCALE_WEIGHT_KG_FLOAT = "weightKgFloat";
    public static final String BODY_SCALE_FAT_PERCENT_FLOAT = "fatPercentFloat";

    public static final String PEDOMETER_TABLE = "Pedometer";
    public static final String PEDOMETER_STEPS_INT = "stepsInt";
    public static final String PEDOMETER_DISTANCE_METER_FLOAT = "distanceMeterFloat";
    public static final String PEDOMETER_CALORIE_FLOAT = "calorieFloat";

    public static final String BP_MONITOR_TABLE = "BpMonitor";
    public static final String BP_MONITOR_SYSTOLE_INT = "systoleInt";
    public static final String BP_MONITOR_DIASTOLE_INT = "diastoleInt";
    public static final String BP_MONITOR_HEART_RATE_INT = "heartRateInt";

    public static final String MIO_FUSE_HEART_RATE_TABLE = "MioFuseHeartRate";
    public static final String MIO_FUSE_HEART_RATE_INT = "heartRateInt";

    public static final String MIO_FUSE_HEART_RATE_HOUR_TABLE = "MioFuseHeartRateHour";
    public static final String MIO_FUSE_HEART_RATE_HOUR_MEDIAN = "MioFuseHeartRateHourMedian";
    public static final String MIO_FUSE_HEART_RATE_HOUR_STANDARD_DEVIATION = "MioFuseHeartRateHourStandardDeviation";
    public static final String MIO_FUSE_HEART_RATE_HOUR_MIN = "MioFuseHeartRateHourMin";
    public static final String MIO_FUSE_HEART_RATE_HOUR_FIRST_DECILE = "MioFuseHeartRateHourFirstDecile";
    public static final String MIO_FUSE_HEART_RATE_HOUR_LAST_DECILE = "MioFuseHeartRateHourLastDecile";
    public static final String MIO_FUSE_HEART_RATE_HOUR_MAX = "MioFuseHeartRateHourMax";

    public static final String MIO_FUSE_HEART_RATE_DAY_TABLE = "MioFuseHeartRateDay";
    public static final String MIO_FUSE_HEART_RATE_DAY_MEDIAN = "MioFuseHeartRateDayMedian";
    public static final String MIO_FUSE_HEART_RATE_DAY_MEAN = "MioFuseHeartRateDayMean";
    public static final String MIO_FUSE_HEART_RATE_DAY_STANDARD_DEVIATION = "MioFuseHeartRateDayStandardDeviation";
    public static final String MIO_FUSE_HEART_RATE_DAY_MIN = "MioFuseHeartRateDayMin";
    public static final String MIO_FUSE_HEART_RATE_DAY_FIRST_DECILE = "MioFuseHeartRateDayFirstDecile";
    public static final String MIO_FUSE_HEART_RATE_DAY_LAST_DECILE = "MioFuseHeartRateDayLastDecile";
    public static final String MIO_FUSE_HEART_RATE_DAY_MAX = "MioFuseHeartRateDayMax";

    public static final String MIO_FUSE_DAILY_TABLE = "MioFuseDailyActivity";
    public static final String MIO_FUSE_DATE_STR = "dateStr";
    public static final String MIO_FUSE_STEPS_INT = "stepsInt";

    public static final String MIO_FUSE_TABLE = "MioFuseActivity";

    public static final String MIO_FUSE_BATTERY_TABLE = "MioFuseBattery";
    public static final String MIO_FUSE_BATTERY_PERCENT_INT = "batteryPercentInt";

    public static final String QUESTIONNAIRE_TABLE = "Questionnaire";
    public static final String QUESTIONNAIRE_JSON = "json";

    public static final String OXIMETER_TABLE = "Oximeter";
    public static final String OXIMETER_PSO2_INT = "spo2";

    public static final String FIBRILLATION_TABLE = "Fibrillation";
    public static final String FIBRILLATION_JSON = "json";

    public static final String MEDICATION_TABLE = "Medication";
    public static final String MEDICATION_JSON = "json";
}
