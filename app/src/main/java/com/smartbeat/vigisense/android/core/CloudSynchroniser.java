package com.smartbeat.vigisense.android.core;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.smartbeat.vigisense.android.R;

public class CloudSynchroniser {
    private static final int HOUR_SEC = 60 * 60;
    private static final String TAG = CloudSynchroniser.class.getName();
    private final MyContext ctx;
    private Account account;

    public CloudSynchroniser(MyContext myContext) {
        ctx = myContext;
        ContentResolver.addPeriodicSync(getAccount(),ctx.getString(R.string.provider), Bundle.EMPTY, 4 * HOUR_SEC);
        myContext.getCtx().getContentResolver().registerContentObserver(Uri.parse("content://" + ctx.getString(R.string.provider)), true, new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                onChange(selfChange, null);
            }

            @Override
            public void onChange(boolean selfChange, Uri uri) {
                Log.i(TAG + "-ContentObserver", "onChange");
                syncManual();
            }
        });

    }

    public Account getAccount() {
//        if (account == null) {
            account = new Account(ctx.getString(R.string.account_name), ctx.getString(R.string.account_type));
            AccountManager accountManager = (AccountManager) ctx.getCtx().getSystemService(Context.ACCOUNT_SERVICE);
            if (accountManager.addAccountExplicitly(account, null, null)) {
                Log.d(TAG, "Account added, account: " + account);
            } else {
                Log.v(TAG, "Account already added, account: " + account);
            }
//        }
        return account;
    }

    public void syncManual() {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(getAccount(), ctx.getString(R.string.provider), settingsBundle);
    }

    public void onDestroy() {

    }


}
